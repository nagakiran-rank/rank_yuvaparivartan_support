module RankYuvaparivartanSupport
  module RankInstantFeesController

    def self.included (base)
      base.instance_eval do
        alias_method_chain :print_reciept, :tmpl
      end
    end

    def print_reciept_with_tmpl
      @instant_fee = InstantFee.find(params[:id])
      @instant_fee_details = @instant_fee.instant_fee_details.all
      @student =Student.find_by_id(@instant_fee.payee_id)

      render :pdf =>'instant_fee_reciept',
             :template => "instant_fees/print_single_receipt_pdf1"
    end

  end
end