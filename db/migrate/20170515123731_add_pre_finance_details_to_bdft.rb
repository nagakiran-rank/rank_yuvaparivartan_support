class AddPreFinanceDetailsToBdft < ActiveRecord::Migration
  def self.up
  	add_column :bank_deposit_finance_transactions, :pre_finance, :boolean, :default => false
  	add_column :bank_deposit_finance_transactions, :pre_finance_transaction_id, :integer
  end

  def self.down
  end
end
