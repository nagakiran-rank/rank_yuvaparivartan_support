namespace :add_additional_details_for_uncreated_data do
  desc "Adding Student Additional Details from pre student additional details for uncreated Data"
  task :install => :environment  do
  	@school = School.all
  	@school.each do |sc|
      MultiSchool.current_school = School.find sc.id
      PreStudentRegistration.find_all_by_school_id(sc.id).each do |psr|
        psr.pre_student_additional_details.each do |pad|
          if psr.student.present?
            student_additional_ids = psr.student.student_additional_details.collect(&:additional_field_id)
            if student_additional_ids.include? pad.additional_field_id
              Rails.logger.info "do nothing"
            else
				      StudentAdditionalDetail.create!(:student_id=>psr.student_id,:additional_field_id=>pad.additional_field_id,:additional_info=>pad.additional_info,:school_id=>pad.school_id)
            end
  			  end
        end
  		end
  	end
  end
end