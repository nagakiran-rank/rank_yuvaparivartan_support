namespace :add_pre_student_admission_links do
  desc "add the link add_pre_student_admission_link student module"
  	task :install => :environment  do
  		academics_category = MenuLinkCategory.find_by_name("academics")
  		puts "Pre student link added for the student Module"
  		MenuLink.create!(:name=>'pre_student_admission',:target_controller=>'pre_student_registrations',:target_action=>'index',:higher_link_id=>MenuLink.find_by_name('students').id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>academics_category.id) unless MenuLink.exists?(:name=>'pre_student_admission')
  	end
end