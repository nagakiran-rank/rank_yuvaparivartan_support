class WriteoffDeposit < ActiveRecord::Base
	belongs_to :bank_deposit_finance_transaction
	belongs_to :writeoff_detail, :foreign_key => 'writeoff_details_id'

end
