class CreateWriteoffDetails < ActiveRecord::Migration
  def self.up
    create_table :writeoff_details do |t|
    	t.integer :student_id
    	t.integer :writeoff_amount
    	t.string :reason_for_writeoff
    	t.binary :document_upload
    	t.integer :vertical_id
      	t.timestamps
    end
  end

  def self.down
    drop_table :writeoff_details
  end
end
