class AddColumnToWriteoffDeposit < ActiveRecord::Migration
  def self.up
  add_column :writeoff_deposits, :student_id, :integer
  add_column :writeoff_deposits, :writeoff_deposit_number, :decimal
  add_column :writeoff_details, :writeoff_date, :date
  end

  def self.down
   remove_column :writeoff_deposits, :student_id
   remove_column :writeoff_deposits, :writeoff_deposit_number
   remove_column :writeoff_details, :writeoff_date
  end
end
