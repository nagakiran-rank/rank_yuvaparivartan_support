class AddFieldsToFinanceTransaction < ActiveRecord::Migration
  def self.up
  	add_column :finance_transactions, :Manual_receipt_no, :string
  	add_column :finance_transactions, :Manual_receipt_date, :date
  end

  def self.down
  	remove_column :finance_transactions, :Manual_receipt_no
  	remove_column :finance_transactions, :Manual_receipt_date
  end
end
