class CreateBatchTransferWithFeePaids < ActiveRecord::Migration
  def self.up
    create_table :batch_transfer_with_fee_paids do |t|
      t.integer :previous_batch_id
      t.integer :current_batch_id
      t.references :student
      t.integer :school_id
      t.decimal :paid_amount,    :precision => 15, :scale => 2
      t.boolean :is_refunded,    :default => false
      t.boolean :is_deducted, :default => false

      t.timestamps
    end
  end

  def self.down
    drop_table :batch_transfer_with_fee_paids
  end
end
