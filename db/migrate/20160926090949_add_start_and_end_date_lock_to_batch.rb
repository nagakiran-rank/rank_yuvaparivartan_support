class AddStartAndEndDateLockToBatch < ActiveRecord::Migration
  def self.up
  	add_column :batches, :start_date_lock, :boolean ,:default => false
  	add_column :batches, :end_date_lock, :boolean ,:default => false
  end

  def self.down
  	add_column :batches, :start_date_lock, :boolean
  	add_column :batches, :end_date_lock, :boolean
  end
end
