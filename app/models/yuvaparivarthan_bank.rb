class YuvaparivarthanBank < ActiveRecord::Base
	has_many :bank_deposit_yuva_banks, :foreign_key => 'bank_id'
	validates_presence_of :bank_name 
	validates_presence_of :account_number
	validates_presence_of :branch_name
	validates_presence_of :state_name
	validates_presence_of :district_name
	validates_uniqueness_of :account_number , :message => "Already present "
end
