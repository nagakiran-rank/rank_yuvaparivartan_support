module RankYuvaparivartanSupport
	module RankFinanceExtensionsController
		def self.included (base)
      base.instance_eval do
        alias_method_chain :pay_all_fees,:tmpl
        alias_method_chain :pay_fees_in_particular_wise,:tmpl
        alias_method_chain :particular_wise_fee_payment,:tmpl
        alias_method_chain :particular_wise_fee_pay_pdf,:tmpl
        alias_method_chain :pay_all_fees_receipt_pdf,:tmpl
        alias_method_chain :delete_multi_fees_transaction,:tmpl
      end
    end
    def pay_all_fees_with_tmpl

      @student=Student.find(params[:id])
      # @student_batch=Student.find(params[:id]).all_batches.reverse
      @finance_fees=FinanceFee.find(:all, :select => "distinct finance_fees.*", :include => [{:finance_fee_collection => [{:fine => :fine_rules}]}], :joins => [{:finance_fee_collection => :fee_collection_batches}], :conditions => "student_id='#{@student.id}' and finance_fee_collections.is_deleted=0")
      # @student_batch=Student.find_by_batch_id(@financefees.batch_id).all_batches.reverse
      @transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
      if request.post?
        status=true
        params[:transactions].each_pair do |f,pairs|
         pairs["balance"] = pairs["amount"]
        end  
         MultiFeesTransaction.transaction do
          multi_fees_transaction= MultiFeesTransaction.create(params[:multi_fees_transaction])
          begin
            finance_transactions=FinanceTransaction.create!(params[:transactions].values)
            # finance_transactions.balance =  params[:transactions][:amount]
          rescue Exception => e
            status=false
          end

          if status and (multi_fees_transaction.valid? && finance_transactions.all?(&:valid?))
            multi_fees_transaction.finance_transactions=finance_transactions
            flash[:notice]="#{t('fees_paid')}"
          else
            flash[:notice]="#{t('fee_payment_failed')}"
            raise ActiveRecord::Rollback
          end
        end
        return redirect_to :controller => 'finance_extensions', :action => 'pay_all_fees'
      end
      @paid_fees=@student.multi_fees_transactions
      @other_transactions=FinanceTransaction.find(:all, :select => "distinct finance_transactions.*", :joins => "LEFT OUTER JOIN `multi_fees_transactions_finance_transactions` ON `multi_fees_transactions_finance_transactions`.finance_transaction_id = `finance_transactions`.id LEFT OUTER JOIN `multi_fees_transactions` ON `multi_fees_transactions`.id = `multi_fees_transactions_finance_transactions`.multi_fees_transaction_id", :conditions => "payee_id='#{@student.id}' and multi_fees_transactions.id is NULL and finance_type='FinanceFee'")
      render :template => 'rank_finance_extensions/pay_all_fees_with_tmpl'
    end
    def pay_fees_in_particular_wise_with_tmpl
      @student = Student.find(params[:id])
      @dates=FinanceFeeCollection.find(:all, :joins => "INNER JOIN fee_collection_batches on fee_collection_batches.finance_fee_collection_id=finance_fee_collections.id INNER JOIN finance_fees on finance_fees.fee_collection_id=finance_fee_collections.id", :conditions => "finance_fees.student_id='#{@student.id}' and finance_fee_collections.is_deleted=#{false}").uniq
      render :template => 'rank_finance_extensions/pay_fees_in_particular_wise_with_tmpl'
    end
    def particular_wise_fee_payment_with_tmpl
      if params[:date].present?
        @student = Student.find(params[:id])
        # @student_batch = Student.find(params[:id]).all_batches.reverse
        # @date = @fee_collection = FinanceFeeCollection.find(params[:date], :include => [:fee_category, {:finance_fee_particulars => :particular_payments}, :fee_discounts])
        @date = @fee_collection = FinanceFeeCollection.find(params[:date])
        @financefee =FinanceFee.find_by_fee_collection_id_and_student_id(@date.id, @student.id, :include => [:finance_transactions, {:particular_payments => [:particular_discounts, :finance_fee_particular]}])
        @student_batch=Student.find_by_batch_id(@financefee.batch_id).all_batches.reverse
        @due_date = @fee_collection.due_date
        @fee_category = @fee_collection.fee_category
        @transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
        @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
        # discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
        # discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
        dis_count=FeeDiscount.find_all_by_batch_id(@student.batch.id)
        dis_count.each do |dis|
          if dis.receiver_type == "Batch" or dis.receiver_type == "StudentCategory"
            @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
          else
            @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
          end
        end
        @total_discount = 0
        @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
        @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?
        bal=(@total_payable-@total_discount).to_f
        days=(Date.today-@date.due_date.to_date).to_i
        auto_fine=@date.fine
        if days > 0 and auto_fine
          @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
          @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule
        end
        @paid_fine=@fine_amount
        @fine_amount=0 if @financefee.is_paid
        if request.post?
          transaction=FinanceTransaction.new(params[:fees])
          transaction.balance =  params[:fees][:amount]
          check_amount=transaction.amount.to_f-transaction.fine_amount.to_f
          unless (@financefee.balance.to_f+@fine_amount.to_f) < check_amount
            if transaction.save
              finance_transaction_hsh={"finance_transaction_id" => transaction.id}
              if params[:particular_payment].present?
                params[:particular_payment][:particular_payments_attributes].values.each { |hsh| hsh.merge!(finance_transaction_hsh) }
                @financefee.update_attributes(params[:particular_payment])
              end
              flash[:notice]="#{t('fee_paid')}"
              @error=false
            else
              flash[:notice]="#{t('fee_payment_failed')}"
            end
          else
            flash[:notice] = "#{t('fee_payment_failed')}"
            @error=false
          end
        end
        @financefee.reload
        @paid_fees=@financefee.finance_transactions.all(:include => [{:particular_payments => [:finance_fee_particular, :particular_discounts]}])
        paid_fine=@paid_fees.select { |fine_transaction| fine_transaction.description=='fine_amount_included' }.sum(&:fine_amount).to_f
        @fine_amount=@fine_amount.to_f-paid_fine
          @fine_amount=0 if @financefee.is_paid
        # @fine_amount=@fine_amount
        # @fine_amount=nil unless @financefee.balance <= @fine_amount
        @applied_discount=ParticularDiscount.find(:all, :joins => [{:particular_payment => :finance_fee}], :conditions => "finance_fees.id=#{@financefee.id}").sum(&:discount).to_f
      else
        @error=true
      end
      render :template => 'rank_finance_extensions/particular_wise_fee_payment_with_tmpl'
    end


    def particular_wise_fee_pay_pdf_with_tmpl
      @fine_amount=params[:fine_amount]
      @paid_fine=@fine_amount
      @student = Student.find(params[:id])
      @date = @fee_collection = FinanceFeeCollection.find(params[:date], :include => [:fee_category, {:finance_fee_particulars => :particular_payments}, :fee_discounts])
      @financefee =FinanceFee.find_by_fee_collection_id_and_student_id(@date.id, @student.id, :include => [:finance_transactions, {:particular_payments => [:particular_discounts, :finance_fee_particular]}])
      @due_date = @fee_collection.due_date
      @fee_category = @fee_collection.fee_category
      @transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
      @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
      # discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
      discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
      @total_discount = 0
      @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
      @total_discount =discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless discounts.nil?
      @paid_fees=@financefee.finance_transactions.all(:include => [{:particular_payments => [:finance_fee_particular, :particular_discounts]}])
      @a = @paid_fees.collect(&:receipt_no).join(', ')
      @pre_receipt = @paid_fees.collect(&:pre_receipt_no).join(', ')
      @applied_discount=ParticularDiscount.find(:all, :joins => [{:particular_payment => :finance_fee}], :conditions => "finance_fees.id=#{@financefee.id}").sum(&:discount).to_f
      @fine_amount=0 if @financefee.is_paid
      render :pdf => 'rank_finance_extensions/new_particular_wise_fee_pay_pdf',
             :template => "rank_finance_extensions/new_particular_wise_fee_pay_pdf"
      # render :pdf => 'rank_finance_extensions/particular_wise_fee_pay_pdf',
      #        :template => "rank_finance_extensions/particular_wise_fee_pay_pdf"
      @paid_fees.each do |p|
        p.update_attributes(:particular_wise_fee => 1)
      end
    end
    def pay_all_fees_receipt_pdf_with_tmpl
      @student=Student.find(params[:id])
      @finance_fees=FinanceFee.find(:all, :select => "distinct finance_fees.*", :include => [{:finance_fee_collection => [{:fine => :fine_rules}]}], :joins => [{:finance_fee_collection => :fee_collection_batches}], :conditions => "student_id='#{@student.id}' and finance_fee_collections.is_deleted=0")
      @paid_fees=@student.multi_fees_transactions
      @a = ""
      @pre_receipt = ""
      @paid_fees.each do |p|
       @a = @a +","+ p.finance_transactions.collect(&:receipt_no).join(', ')
       @pre_receipt = @pre_receipt + ","+ p.finance_transactions.collect(&:pre_receipt_no).join(', ')
      end
      @a.slice!(0)
      @pre_receipt.slice(0)
      @other_transactions=FinanceTransaction.find(:all, :select => "distinct finance_transactions.*", :joins => "LEFT OUTER JOIN `multi_fees_transactions_finance_transactions` ON `multi_fees_transactions_finance_transactions`.finance_transaction_id = `finance_transactions`.id LEFT OUTER JOIN `multi_fees_transactions` ON `multi_fees_transactions`.id = `multi_fees_transactions_finance_transactions`.multi_fees_transaction_id", :conditions => "payee_id='#{@student.id}' and multi_fees_transactions.id is NULL and finance_type='FinanceFee'")
      # render :pdf => 'rank_finance_extensions/pay_all_fees_receipt_pdf',
      #        :template => "rank_finance_extensions/pay_all_fees_receipt_pdf"
      render :pdf => 'rank_finance_extensions/new_pay_all_fees_receipt_pdf',
             :template => "rank_finance_extensions/new_pay_all_fees_receipt_pdf"
      @paid_fees.each do |p|
        p.finance_transactions.each do |q|
          q.update_attributes(:pay_all_fee => 1)
        end
      end
      # @other_transactions.each do |p|
      #    p.update_attributes(:pay_all_fee => 1)
      # end
    end
    
    def delete_multi_fees_transaction_with_tmpl
      @student=Student.find(params[:id])
      @transaction_category_id=FinanceTransactionCategory.find_by_name("Fee").id
      if params[:type]=='multi_fees_transaction'
        mft=MultiFeesTransaction.find(params[:transaction_id])
         mft.destroy
        # bdft=BankDepositFinanceTransaction.find_by_finance_transaction_id(params[:transaction_id])
        #   unless bdft.nil?
        #     flash[:notice] = "#{t('already_this_transaction_present_in_deposit_details')}"
        #   else
        #     mft.destroy 
        #   end
      else
        ActiveRecord::Base.transaction do
          ft= FinanceTransaction.find(params[:transaction_id])
          if FedenaPlugin.can_access_plugin?("fedena_pay")
            payment = ft.payment
            unless payment.nil?
              status = Payment.payment_status_mapping[:reverted]
              payment.update_attributes(:status_description => status)
              payment.save
            end
          end
          raise ActiveRecord::Rollback unless ft.destroy
          # bdft=BankDepositFinanceTransaction.find_by_finance_transaction_id(params[:transaction_id])
          # unless bdft.nil?
          #   flash[:notice] = "#{t('already_this_transaction_present_in_deposit_details')}"
          # else
          #   raise ActiveRecord::Rollback unless ft.destroy 
          # end
        end
      end
      @paid_fees=@student.multi_fees_transactions
      @other_transactions=FinanceTransaction.find(:all, :select => "distinct finance_transactions.*", :joins => "LEFT OUTER JOIN `multi_fees_transactions_finance_transactions` ON `multi_fees_transactions_finance_transactions`.finance_transaction_id = `finance_transactions`.id LEFT OUTER JOIN `multi_fees_transactions` ON `multi_fees_transactions`.id = `multi_fees_transactions_finance_transactions`.multi_fees_transaction_id", :conditions => "payee_id='#{@student.id}' and multi_fees_transactions.id is NULL and finance_type='FinanceFee'")
      @finance_fees=FinanceFee.find(:all, :include => "finance_fee_collection", :conditions => "student_id='#{@student.id}'")
      render :update do |page|
        flash[:notice]="#{t('finance.flash18')}"
        page.replace_html "pay_fees", :partial => 'rank_finance_extensions/pay_fees_form'
      end
    end

    def edit_manual_receipt3
      @transaction_values = FinanceTransaction.find(params[:id])     
      @name = params[:student_name]
      if request.post? 
          values = params[:edit_finance_transaction]
        date = Date.new values["Manual_receipt_date(1i)"].to_i, values["Manual_receipt_date(2i)"].to_i, values["Manual_receipt_date(3i)"].to_i
        if date < Date.today or date == Date.today
            @transaction_values.update_attributes(:Manual_receipt_no => params[:edit_finance_transaction][:Manual_receipt_no] , :Manual_receipt_date => date)
             flash[:notice] = "#{t('Transaction_Details_Updated_Sucessfully')}"
              redirect_to :controller => 'finance_extensions', :action => 'particular_wise_fee_payment',:id => params[:sid],:date=>params[:date]
         else
            render :update do |page|
             page.alert "enter valid date"
            end
         end      
      else  
         render :update do |page|
          page.replace_html 'modal-box', :partial => 'rank_finance_extensions/edit_manual_receipt3'
           page << "Modalbox.show($('modal-box'), {title: '#{t('edit_manual_receipt_and_manual_date')}', width: 650});"
        end
      end
    end
    def edit_manual_receipt4
      @paid_fees_values = MultiFeesTransaction.find(params[:id])
      @name = params[:student_name]     
      if request.post?
        @paid_fees_values.finance_transactions.each do |a|
        @s=FinanceTransaction.find(a.id)
        values = params[:edit_finance_transaction]
        date = Date.new values["Manual_receipt_date(1i)"].to_i, values["Manual_receipt_date(2i)"].to_i, values["Manual_receipt_date(3i)"].to_i
        if date < Date.today or date == Date.today
            @s.update_attributes(:Manual_receipt_no => params[:edit_finance_transaction][:Manual_receipt_no] , :Manual_receipt_date => date)
            render :update do |page|
              page.hide 'modal-box'
              page.reload
            end
        else
            render :update do |page|
             page.alert "enter valid date"
            end
         end       
        end        
        
      else
      render :update do |page|
          page.replace_html 'modal-box', :partial => 'rank_finance_extensions/edit_manual_receipt4'
           page << "Modalbox.show($('modal-box'), {title: '#{t('edit_manual_receipt_and_manual_date')}', width: 650});"
      end
      end
    end
    def edit_manual_receipt5
      @transaction_values =FinanceTransaction.find(params[:id]) 
      if request.post?  
        values = params[:edit_finance_transaction]
        date = Date.new values["Manual_receipt_date(1i)"].to_i, values["Manual_receipt_date(2i)"].to_i, values["Manual_receipt_date(3i)"].to_i
        if date < Date.today or date == Date.today
            @transaction_values.update_attributes(:Manual_receipt_no => params[:edit_finance_transaction][:Manual_receipt_no] , :Manual_receipt_date => date)
             flash[:notice] = "#{t('Transaction_Details_Updated_Sucessfully')}"         
            render :update do |page|
              page.hide 'modal-box'
              page.reload
            end
        else
            render :update do |page|
             page.alert "enter valid date"
            end
         end     
      else
        render :update do |page|
          page.replace_html 'modal-box', :partial => 'rank_finance_extensions/edit_manual_receipt5'
           page << "Modalbox.show($('modal-box'), {title: '#{t('edit_manual_receipt_and_manual_date')}', width: 650});"
        end
      end
      
    end
	end
end