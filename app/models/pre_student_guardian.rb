class PreStudentGuardian < ActiveRecord::Base
	belongs_to :pre_student_registration
	belongs_to :country
	validates_presence_of :first_name,:relation,:last_name
	HUMANIZED_COLUMNS = {:first_name => "#{t('guardian_first_name')}",:relation=>"#{t('guardian_relation')}",:last_name => "#{t('guardian_last_name')}"}
	validates_presence_of :office_phone1
	validates_presence_of :mobile_phone

  def self.human_attribute_name(attribute)
    HUMANIZED_COLUMNS[attribute.to_sym] || super
  end
end
