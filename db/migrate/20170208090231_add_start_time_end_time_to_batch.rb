class AddStartTimeEndTimeToBatch < ActiveRecord::Migration
  def self.up
  	add_column :batches, :start_time, :string
  	add_column :batches, :end_time, :string
  end

  def self.down
  	remove_column :batches, :start_time, :string
  	remove_column :batches, :end_time, :string
  end
end
