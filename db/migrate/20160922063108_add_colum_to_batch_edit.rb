class AddColumToBatchEdit < ActiveRecord::Migration
  def self.up
  	add_column :batches, :audit_rank, :string
  	add_column :batches, :ec_rank, :string
  	add_column :batches, :finance_remark, :string
  	add_column :batches, :other_remark, :string
  end

  def self.down
  	remove_column :batches, :audit_rank
  	remove_column :batches, :ec_rank
  	remove_column :batches, :finance_remark
  	remove_column :batches, :other_remark
  end
end
