class AddFieldsToBankDepositFinanceTransaction < ActiveRecord::Migration
  def self.up
  	add_column :bank_deposit_finance_transactions, :reject_status, :boolean ,:default => false
  	add_column :bank_deposit_finance_transactions, :reject_reason, :string
  end

  def self.down
  	remove_column :bank_deposit_finance_transactions, :reject_status, :boolean ,:default => false
  	remove_column :bank_deposit_finance_transactions, :reject_reason, :string
  end
end
