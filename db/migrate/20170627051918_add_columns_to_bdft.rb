class AddColumnsToBdft < ActiveRecord::Migration
  def self.up
  	add_column :bank_deposit_finance_transactions, :balance, :decimal
  	add_column :bank_deposit_finance_transactions, :status, :boolean, :default => false
  end

  def self.down
  	remove_column :bank_deposit_finance_transactions, :balance
  	remove_column :bank_deposit_finance_transactions, :status
  end
end
