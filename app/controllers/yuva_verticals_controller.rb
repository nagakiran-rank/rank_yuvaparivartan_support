class YuvaVerticalsController < ApplicationController
	before_filter :find_vertical, :only => [:show, :edit, :update, :destroy]
	include AdminControllerActions
	helper_method :admin_user_session
	before_filter :require_admin_session
	skip_before_filter :load_school
	skip_before_filter :message_user
	skip_before_filter :set_user_language
	skip_before_filter :set_variables
	before_filter :get_school_count
	layout "schools"
  
	def index
		@verticals = YuvaVertical.all
	end

	def new
		@vertical = YuvaVertical.new
	end
	 
  def create
    @vertical = YuvaVertical.new params[:yuva_vertical]
    if @vertical.save
      flash[:notice] = "Created vertical successfully"
      redirect_to yuva_verticals_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @vertical.update_attributes(params[:yuva_vertical])
      flash[:notice] = "Updated course details successfully"
      redirect_to yuva_verticals_path
    else
      render 'edit'
    end
  end

  def destroy
    # if @vertical.active_courses.empty?
      @vertical.destroy
      flash[:notice]="Vertical deleted successfully"
      redirect_to yuva_verticals_path
    # else
    #   flash[:notice]="Unable to Delete. The Vertical is already assigned to school."
    #   redirect_to yuva_verticals_path
    # end

  end

  def show
  end

	private

		def find_vertical
	    @vertical = YuvaVertical.find params[:id]
	  end

	  def get_school_count
	    @schools_count = School.count(:conditions=>{:is_deleted=>false})
	  end
end
