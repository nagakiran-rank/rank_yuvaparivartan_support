class PreStudentRegistrationsController < ApplicationController

	def index
		#if current_user.admin?
		courses = []
		master_course = MasterCourse.active.status
		master_course.each do |course|
      courses << Course.find_all_by_master_course_id(course.id,:order => "course_name")
    end
    @courses = courses.reject{|c| c == []}
 		# else
 		# 	flash[:notice] = "#{t('flash_msg4')}"
   #    redirect_to :controller => "user",:action => "dashboard"
   #  end
	end

	def pre_student_sort
		search_by =""
    if params[:search].present?
      search_by=params[:search]
    end
    @pre_student_applicant=PreStudentRegistration.search_by_order(params[:course_id],search_by)
    @registration_course = Course.find(params[:course_id])
    render :template => 'pre_student_registrations/pre_student_sort'
	end

	def pre_student_registration_form
		@course = Course.find(params[:course_id])
		@countries = Country.all
    @pre_student_registration = PreStudentRegistration.new
    @pre_student_guardian = @pre_student_registration.build_pre_student_guardian
    @selected_value = Configuration.default_country
    @pre_student_registration.build_pre_student_guardian
    @pre_student_registration.build_pre_student_previous_data
   	#pre_student_document_upload = @pre_student_registration.build_pre_student_document_upload
   	@pre_student_registration.pre_student_document_uploads.build
    @addl_field_groups = ApplicantAddlFieldGroup.find(:all,:conditions=>{:registration_course_id=>params[:course_id],:is_active=>true})
    additional_mandatory_fields = StudentAdditionalField.active.all(:conditions => {:is_mandatory => true})
    additional_fields = StudentAdditionalField.find_all_by_id(@course.additional_field_ids).compact
    @additional_fields = (additional_mandatory_fields + additional_fields).uniq.compact.flatten
    @pre_student_additional_detail = @pre_student_registration.pre_student_additional_details
    @currency = currency
    @categories = StudentCategory.active
		render :update do |page|
	 		page.replace_html 'calender', :partial => 'pre_student_registrations/pre_student_registration_form' 
		end
	end

	def create_pre_student_registration
		@categories = StudentCategory.active
		@course = Course.find(params[:pre_student_registration][:course_id])
		@countries = Country.all
		@addl_field_groups = ApplicantAddlFieldGroup.find(:all,:conditions=>{:registration_course_id=>params[:pre_student_registration][:course_id],:is_active=>true})
		additional_mandatory_fields = StudentAdditionalField.active.all(:conditions => {:is_mandatory => true})
    additional_fields = StudentAdditionalField.find_all_by_id(@course.additional_field_ids).compact
    @additional_fields = (additional_mandatory_fields + additional_fields).uniq.compact.flatten
		if request.post?
			@course = Course.find(params[:pre_student_registration][:course_id])
	    @pin_code = params[:pre_student_registration][:pin_code]
	    @pre_student_registration = PreStudentRegistration.new(params[:pre_student_registration])
	    #@pre_student_registration = PreStudentRegistration.new
	    @pre_student_registration.pre_std_course_fee = params[:course_fee]
	    @pre_student_guardian = @pre_student_registration.build_pre_student_guardian(params[:applicant_guardian])
	    @pre_student_previous_data = @pre_student_registration.build_pre_student_previous_data(params[:applicant_previous_data])
	    #@pre_student_document_upload = @pre_student_registration.build_pre_student_document_upload(:attachment=>params[:attachment])
	    #@pre_student_registration.build_pre_student_document_uploads(:attachment=>params[:attachment])
	    if params[:attachment].present?
	    	@pre_student_registration.pre_student_document_uploads.build(:attachment=>params[:attachment])
	  	end
	  	if params[:attachment1].present?
	    	@pre_student_registration.pre_student_document_uploads.build(:attachment=>params[:attachment1])
	  	end
	  	if params[:attachment2].present?
	    	@pre_student_registration.pre_student_document_uploads.build(:attachment=>params[:attachment2])
	    end
	    @pre_student_additional_detail = @pre_student_registration.pre_student_additional_details
	    @currency = currency
        if params[:applicant_additional_details].present?
          params[:applicant_additional_details].each_pair do |k, v|
            addl_info = v['additional_info']
            addl_field = StudentAdditionalField.find_by_id(k)
            if addl_field.input_type == "has_many"
              addl_info = addl_info.join(", ")
            end
            addl_detail = @pre_student_registration.pre_student_additional_details.build(:additional_field_id => k,:additional_info => addl_info)
            addl_detail.valid?
            addl_detail.save if addl_detail.valid?
          end
      	end
    		if @pre_student_registration.save
      #     @pre_student_document_upload = PreStudentDocumentUpload.new(:attachment=>params[:attachment], :pre_student_registration_id=>@pre_student_registration.id)
  				# @pre_student_document_upload.save!
          flash[:notice] = "#{t('flash_success')} <a href='/pre_student_registrations/index'>Click for New Admission</a>"
          @enabled_courses = Course.find(:all,:order => "course_name")
          redirect_to :action => "manage_courses"
        else
        	@heading = true
        	@total_add_fields = @additional_fields.count
        	unless params[:applicant_additional_details].nil?
				    @current_add_fields = params[:applicant_additional_details].count
				    if @current_add_fields < @total_add_fields
				    	@pre_student_registration.errors.add_to_base("Select all mandatory Additional Fields")
				    end
				  end
				  @pre_student_registration.pre_student_document_uploads.build
      		render :template => "pre_student_registrations/_pre_student_registration_form",:course_id =>  @course.id,:heading => @heading
        end
	  end
	end

	def view_pre_student
    @pre_student_registration = PreStudentRegistration.find params[:id]
    @course = Course.find_by_id(@pre_student_registration.course_id)
    @additional_details = @pre_student_registration.pre_student_additional_details
	end

	def pre_student_applicant
	    @pre_student_applicant = PreStudentRegistration.find_all_by_course_id(params[:id])
	    @registration_course = Course.find(params[:id])
	end

	def pre_student_allot
  	@pre_std = params[:regid]
  	@batch = Batch.find(params[:pre_student_allot][:batch])
  	#@next_admission_no=User.next_admission_no("student")
  	if @pre_std.nil?
  		flash[:notice] = "#{t('flash7')}"
  		redirect_to :action=>"pre_student_applicant",:id=>params[:course_id]
  	else
	  	@pre_std.each do |p|
	  		@pre_student = PreStudentRegistration.find(p.to_i)
	  		@next_admission_no = User.next_admission_no("student")
	  		if @next_admission_no.nil?
	  			@next_admission_no = "1"
	  		end
	  		@student = Student.new(:first_name=>@pre_student.first_name,:middle_name=>@pre_student.middle_name,:last_name=>@pre_student.last_name,:batch_id=>params[:pre_student_allot][:batch],:date_of_birth=>@pre_student.date_of_birth,:gender=>@pre_student.gender,:nationality_id=>@pre_student.nationality_id,:address_line1=>@pre_student.address_line1,:address_line2=>@pre_student.address_line2,:city=>@pre_student.city,:state=>@pre_student.state,:pin_code=>@pre_student.pin_code,:country_id=>@pre_student.country_id,:phone1=>@pre_student.phone1,:phone2=>@pre_student.phone2,:email=>@pre_student.email,:photo => @pre_student.photo,:is_active=>@pre_student.is_active,:admission_no=>@next_admission_no,:admission_date=>@pre_student.admission_date,:student_category_id=>@pre_student.pre_student_category_id,:religion=>@pre_student.religion)
	    		@finance_fee_collections = @batch.finance_fee_collections
    		if (@finance_fee_collections.empty? == false) && (Batch.find(@batch.id).finance_fee_collections.first.finance_fee_particulars.map{|p| p.amount}.inject(0){|sum,x| sum + x } >= @pre_student.pre_std_course_fee)
	  			if @student.save! 
	  				@pre_student.update_attributes(:student_id=>@student.id,:transferred=>true)
		  			@pre_student_guardian = PreStudentGuardian.find_by_pre_student_registration_id(@pre_student.id)
		  			 @student_gayrdian = Guardian.create!(:ward_id=>@student.id,:first_name=>@pre_student_guardian.first_name,:last_name=>@pre_student_guardian.last_name,:relation=>@pre_student_guardian.relation,:email=>@pre_student_guardian.email,:office_phone1=>@pre_student_guardian.office_phone1,:office_phone2=>@pre_student_guardian.office_phone2,:mobile_phone=>@pre_student_guardian.mobile_phone,:office_address_line1=>@pre_student_guardian.office_address_line1,:office_address_line2=>@pre_student_guardian.office_address_line2,:city=>@pre_student_guardian.city,:state=>@pre_student_guardian.state,:country_id=>@pre_student_guardian.country_id,:dob=>@pre_student_guardian.dob,:occupation=>@pre_student_guardian.occupation,:income=>@pre_student_guardian.income,:education=>@pre_student_guardian.education,:school_id=>@pre_student_guardian.school_id)
		  			 @student.update_attributes(:immediate_contact_id => @student_gayrdian.id)
	  				@pre_std_prevs_data = PreStudentPreviousData.find_by_pre_student_registration_id(@pre_student.id)
	  				unless @pre_std_prevs_data.nil?
	  					StudentPreviousData.create!(:student_id=>@student.id,:institution=>@pre_std_prevs_data.last_attended_school,:year=>@pre_std_prevs_data.qualifying_exam_year,:course=>@pre_std_prevs_data.qualifying_exam,:total_mark=>@pre_std_prevs_data.qualifying_exam_final_score,:school_id=>@pre_std_prevs_data.school_id)
	  				end
	  				@pre_std_add_details = PreStudentAdditionalDetail.find_all_by_pre_student_registration_id(@pre_student.id)
	  				unless @pre_std_add_details.nil?
	  					@pre_std_add_details.each do |abc|
	  					StudentAdditionalDetail.create!(:student_id=>@student.id,:additional_field_id=>abc.additional_field_id,:additional_info=>abc.additional_info,:school_id=>abc.school_id)
	  				end
	  				end
	  				if @pre_student.pre_student_document_uploads.present?
	  					document_uploads = @pre_student.pre_student_document_uploads
		          document_uploads.each do |att|
		            s =  StudentDocument.create(:student_id => @student.id,:document => att.attachment)
		          end
		        end
	  				# @finance_fee = FinanceFee.new_student_fee(Batch.find(@batch.id).finance_fee_collections.first,@student)
	  				# total_fees = FinanceFeeParticular.find_all_by_batch_id(@batch.id).map{|p| p.amount}.sum
	  				@total_fees = FinanceFeeParticular.find_all_by_batch_id(@batch.id)			  				
	  				total_fees = @total_fees.map{|i| i.amount.to_f}.inject{|sum,y| sum + y }
	  				# total_paid = PreFinanceTransaction.find_all_by_pre_student_registration_id(@pre_student.id).map{|p| p.amount}.sum
	  				@total_paid = PreFinanceTransaction.find_all_by_pre_student_registration_id(@pre_student.id)
	  				total_paid = @total_paid.map{|i| i.amount.to_f}.inject{|sum,y| sum + y }
	  				balance1 = (total_fees - total_paid) unless total_paid.nil?
	  				#@finance_fee = FinanceFee.create(:student_id => @student.id, :fee_collection_id => Batch.find(@batch.id).finance_fee_collections.first.id, :balance => balance1, :batch_id => @student.batch_id, :student_category_id => @student.student_category_id)
	  				 FinanceFee.new_student_fee(@batch.finance_fee_collections.first,@student)
	  				 @finance_fee = FinanceFee.find_by_fee_collection_id_and_student_id(@batch.finance_fee_collections.first.id,@student.id)
	  				 @pre_finance_transaction = PreFinanceTransaction.find_all_by_pre_student_registration_id(@pre_student.id)
	  				pre_stu_blance = @pre_student.pre_std_course_fee
	  				pre_count = @pre_finance_transaction.count
	  				@pre_finance_transaction.each do |p|
		  					#@finance_fee1 = FinanceFee.find_by_student_id(@student.id)
		  					#@finance_fee1 = FinanceFee.find_by_fee_collection_id_and_student_id(Batch.find(@batch.id).finance_fee_collections.first.id,@student.id)
		  					#puts "#{@finance_fee1.inspect}-=-=-=-=-=-"
			  				transaction = FinanceTransaction.new
	            	(@finance_fee.balance.to_f > p.amount) ? transaction.title = "#{t('receipt_no')}. (#{t('partial')}) F#{@finance_fee.id}" : transaction.title = "#{t('receipt_no')}. F#{@finance_fee.id}"
			  				transaction.amount = p.amount
			  				transaction.category = FinanceTransactionCategory.find_by_name("Fee")
			  				transaction.transaction_date = p.transaction_date
			  				transaction.finance = @finance_fee
			  				transaction.payee = @student
			  				transaction.payment_mode = p.payment_mode
			  				transaction.payment_note = p.payment_note
			  				transaction.batch_id = @batch.id
			  				transaction.balance = p.amount
			  				transaction.pre_finance_amt = true
			  				transaction.pre_receipt_no = p.receipt_no
			  				transaction.save!
			  				p.update_attributes(:transferred => true)
			  				if pre_count == 1
			  					puts "WHEN ONE TRANSACTION"
			  					@finance_fee.update_attributes(:balance => balance1)
			  					transaction.update_attributes(:finance => @finance_fee)
			  				else
			  					puts "WHEN MORE THAN ONE TRANSACTION"
			  					@finance_fee.update_attributes(:balance => balance1 + PreFinanceTransaction.find_all_by_pre_student_registration_id(@pre_student.id).last.balance)
			  				end
			  			end
			  		# flash[:notice] = "#{t('flash5')}"
	  				# puts "-------return_redirect_to--------"
	  				# return redirect_to :action=>"pre_student_applicant",:id=>@pre_student.course_id
		  		end
    		else
	    		flash[:notice] = "#{t('flash6')}"
		    	return redirect_to :action=>"pre_student_applicant",:id=>@pre_student.course_id
	    	end
	  	end
	  	flash[:notice] = "#{t('flash5')}"
			redirect_to :action=>"pre_student_applicant",:id=>@pre_student.course_id
		end
	end

  def pre_student_edit
  	@pre_student_registration = PreStudentRegistration.find params[:id]
    @selected_value = Configuration.default_country
    @course = Course.find(@pre_student_registration.course_id)
    @categories = StudentCategory.active
    @countries = Country.all
    @currency = currency
    @applicant_guardian = @pre_student_registration.pre_student_guardian
    @applicant_previous_data = @pre_student_registration.pre_student_previous_data
    @addl_field_groups = ApplicantAddlFieldGroup.find(:all,:conditions=>{:registration_course_id=>Course.find(@pre_student_registration.course_id), :is_active => true})
    additional_mandatory_fields = StudentAdditionalField.active.all(:conditions => {:is_mandatory => true})
    additional_fields = StudentAdditionalField.find_all_by_id(@course.additional_field_ids).compact
    @additional_fields = (additional_mandatory_fields + additional_fields).uniq.compact.flatten
    @pre_student_additional_detail = @pre_student_registration.pre_student_additional_details
    #@pre_student_registration.pre_student_document_uploads.build
  end

  def update_pre_student_registration
		@course = Course.find(params[:pre_student_registration][:course_id])
		@selected_value = Configuration.default_country
		@categories = StudentCategory.active
  	@countries = Country.all
  	@currency = currency
    @pin_code = params[:pre_student_registration][:pin_code]
    @pre_student_registration = PreStudentRegistration.find(params[:id])
    @pre_student_registration1 = @pre_student_registration.update_attributes(params[:pre_student_registration])
    #@pre_student_guardian = PreStudentGuardian.find_by_pre_student_registration_id(params[:id])
    #@pre_student_guardian1 = @pre_student_guardian.update_attributes!(params[:applicant_guardian])
    @pre_student_registration.pre_student_guardian.update_attributes(params[:applicant_guardian])
    #@pre_student_guardian.update_attributes(:dob=>params[:pre_student_guardian][:dob])
    #@pre_student_document_upload = PreStudentDocumentUpload.find_by_pre_student_registration_id(params[:id])
    if params[:attachment].present?
    	@pre_student_registration.pre_student_document_uploads[0].update_attributes(:attachment=>params[:attachment], :pre_student_registration_id=>@pre_student_registration.id)
    end
    if params[:attachment1].present?
    	@pre_student_registration.pre_student_document_uploads[1].update_attributes(:attachment=>params[:attachment1], :pre_student_registration_id=>@pre_student_registration.id)
    end
    if params[:attachment2].present?
    	@pre_student_registration.pre_student_document_uploads[2].update_attributes(:attachment=>params[:attachment2], :pre_student_registration_id=>@pre_student_registration.id)
    end
 		#@pre_student_previous_data = PreStudentPreviousData.find_by_pre_student_registration_id(params[:id])
 		@pre_student_registration.pre_student_previous_data.update_attributes(params[:applicant_previous_data])
    if params[:applicant_additional_details].present?
  		params[:applicant_additional_details].each_pair do |k,v|
  			@pre_std_add_details = @pre_student_registration.pre_student_additional_details.find_by_pre_student_registration_id_and_additional_field_id(params[:id],k.to_i)
  			if @pre_std_add_details.nil?
    			@pre_student_additional_detail = PreStudentAdditionalDetail.new(:pre_student_registration_id => params[:id] ,:additional_field_id => k.to_i ,:additional_info => v['additional_info'].to_s)
    			if @pre_student_additional_detail.save
    			else
	    			unless @pre_student_additional_detail.errors.full_messages.empty?
					    @pre_student_additional_detail.errors.full_messages.each do |p|
					    	@pre_student_registration.errors.add_to_base(p)
					    end
					end	    				
    			end
    		else
    			addl_field = StudentAdditionalField.find_by_id(k)
    			additional_info = v['additional_info']
            if addl_field.input_type == "has_many"
              additional_info = additional_info.join(", ")
            end
    			@pre_std_add_details.update_attributes(:additional_info => additional_info)
    		end
  		end 
    end
    unless @pre_student_registration.pre_student_guardian.errors.full_messages.empty?
	    @pre_student_registration.pre_student_guardian.errors.full_messages.each do |p|
	    	@pre_student_registration.errors.add_to_base(p)
	    end
	end
	unless @pre_student_registration.pre_student_previous_data.errors.full_messages.empty?
	    @pre_student_registration.pre_student_previous_data.errors.full_messages.each do |p|
	    	@pre_student_registration.errors.add_to_base(p)
	    end
	end
	# unless @pre_student_registration.pre_student_document_upload.errors.full_messages.empty?
	#     @pre_student_registration.pre_student_document_upload.errors.full_messages.each do |p|
	#     	@pre_student_registration.errors.add_to_base(p)
	#     end
	# end
	@pre_student_registration.pre_student_additional_details.each do|q|
	    unless q.errors.full_messages.empty?
		    q.errors.full_messages.each do |p|
		    	@pre_student_registration.errors.add_to_base(p)
		    end
		end
	end
	@addl_field_groups = ApplicantAddlFieldGroup.find(:all,:conditions=>{:registration_course_id=>Course.find(@pre_student_registration.course_id), :is_active => true})
    additional_mandatory_fields = StudentAdditionalField.active.all(:conditions => {:is_mandatory => true})
    additional_fields = StudentAdditionalField.find_all_by_id(@course.additional_field_ids).compact
    @additional_fields = (additional_mandatory_fields + additional_fields).uniq.compact.flatten
    @total_add_fields = @additional_fields.count
    unless params[:applicant_additional_details].nil?
	    @current_add_fields = params[:applicant_additional_details].count
	    if @current_add_fields < @total_add_fields
	    	@pre_student_registration.errors.add_to_base("Select all mandatory Additional Fields")
	    end
	  end
    if @pre_student_registration.errors.empty?
    	redirect_to :action => "view_pre_student" , :id => @pre_student_registration.id 
    else
    	@selected_value = Configuration.default_country
	    @course = Course.find(@pre_student_registration.course_id)
	    @categories = StudentCategory.active
	    @countries = Country.all
	    @currency = currency
	    @applicant_guardian = @pre_student_registration.pre_student_guardian
	    @applicant_previous_data = @pre_student_registration.pre_student_previous_data
	    @addl_field_groups = ApplicantAddlFieldGroup.find(:all,:conditions=>{:registration_course_id=>Course.find(@pre_student_registration.course_id), :is_active => true})
	    additional_mandatory_fields = StudentAdditionalField.active.all(:conditions => {:is_mandatory => true})
	    additional_fields = StudentAdditionalField.find_all_by_id(@course.additional_field_ids).compact
	    @additional_fields = (additional_mandatory_fields + additional_fields).uniq.compact.flatten
	    @pre_student_additional_detail = @pre_student_registration.pre_student_additional_details
    	render :template => "pre_student_registrations/pre_student_edit",:course_id =>  @course.id
    end
  end

  def manage_courses
  	@enabled_courses = Course.find(:all,:order => "course_name")
  	render :template => "pre_student_registrations/success"
  end

  def find_cities
  	@yuva_state_id = YuvaState.find_by_name(params[:student_state]).id
    @yuva_cities = YuvaCity.find_all_by_state_id(@yuva_state_id)
    render :update do |page|
        page.replace_html 'student_cities', :partial => 'pre_student_registrations/find_cities'
    end
  end

  def download_document
  	 begin
      @document = PreStudentDocumentUpload.find params[:id]
      unless @document.nil?
        send_file  @document.attachment.path , :type=>@document.attachment_content_type
      end
    rescue Exception => e
      Rails.logger.info "Exception from rank_yuvaparivarthan_support pluggin in pre_student_registration_controller, download_document action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong while downloading a file. Please inform administration"
      redirect_to :controller => "pre_student_registrations", :action => :view_pre_student ,:id => @document.pre_student_registration_id
    end
  end

  def change_document_0
  	render :update do |page|
      page.replace_html 'document_replace_0', :partial => 'pre_student_registrations/change_document_0'
      page.hide "document_hide"
    end
  end

  def change_document_1
	render :update do |page|
    page.replace_html 'document_replace_1', :partial => 'pre_student_registrations/change_document_1'
    page.hide "document_hide"
   end
  end

  def change_document_2
		render :update do |page|
	    page.replace_html 'document_replace_2', :partial => 'pre_student_registrations/change_document_2'
	    page.hide "document_hide"
	  end
  end
   	# respond_to do |format|
    #   format.js
    # end
  #end

end

