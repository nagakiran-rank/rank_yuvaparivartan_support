namespace :delete_extra_student_additional_fields do
  desc "Deleting Student additional details"
  task :install => :environment  do
  	@school = School.all
  	@school.each do |sc|
  		MultiSchool.current_school = School.find sc.id
  		Student.find_all_by_school_id(sc.id).each do |st|
        student_additional_ids = []
  			st.student_additional_details.each do |sad|
          if student_additional_ids.include? sad.additional_field_id
            sad.delete
          end
          student_additional_ids << sad.additional_field_id
  			end
  		end
  	end
  end
end