class AddRejectToWriteoffDeposit < ActiveRecord::Migration
  def self.up
  	add_column :writeoff_deposits, :reject, :boolean, :default => false
  	add_column :writeoff_deposits, :reason, :string
  end

  def self.down
  end
end
