module RankYuvaparivartanSupport
  module RankBatchTransfersController

    def self.included (base)
      base.instance_eval do
        alias_method_chain :show, :tmpl
        alias_method_chain :transfer, :tmpl
        alias_method_chain :subject_transfer, :tmpl
      end
    end

    def show_with_tmpl
      flash[:notice] = nil
      @batch = Batch.find params[:id], :include => [:students],:order => "students.first_name ASC"
      @batches = Batch.active - @batch.to_a
      defaulter_students = @batch.students.collect{|student| student.finance_fees}.flatten.collect{|s| s.is_paid}
      if defaulter_students.include? false
        flash[:notice] = "#{t('flash5')}"
      end
      render :template => 'rank_batch_transfers/show_with_tmpl'
    end

    def transfer_with_tmpl
      if request.post?
        @batch = Batch.find params[:id], :include => [:students],:order => "students.first_name ASC"
        if params[:transfer][:to].present?
          unless params[:transfer][:students].nil?
            students = Student.find(params[:transfer][:students])
            students.each do |s|
              finance=s.finance_fees.select{|fee| fee.batch_id == s.batch.id}.map{|f| f.fee_transactions}.flatten.map{|u| u.finance_transaction.amount}
              finance_amount=finance.inject{|sum,x| sum + x }
              #  hostel=HostelFee.find_all_by_student_id(s.id).select{|p| p.hostel_fee_collection.batch_id == @batch.id}.flatten.map{|i| i.finance_transaction}.compact.map {|r| r.amount}
              # hostel_amount=hostel.inject{|sum,y| sum + y }
              #  transport=TransportFee.find_all_by_receiver_id(s.id).select {|t| t.transport_fee_collection.batch_id == @batch.id}.flatten.map{|c| c.finance_transaction}.compact.map{|a| a.amount}
              # transport_amount=transport.inject{|sum,z| sum + z }
              total_amount=finance_amount
              batch_student=s.batch_students.find_by_batch_id(s.batch.id)
              if batch_student.nil?
                s.batch_students.create(:batch_id => s.batch.id)
                # s.batch_students.create(:batch_id => s.batch.id, :roll_number => s.roll_number)
              else
                batch_student.touch
              end
              s.update_attribute(:batch_id, params[:transfer][:to])
              s.reload
              #s.update_attribute(:has_paid_fees_for_batch,0)
              # s.update_attribute(:roll_number,nil)
              if params[:batch_transfer_with_fee].present? and total_amount.present?
                batch_transfer_fee = BatchTransferWithFeePaid.new(:paid_amount => total_amount , :previous_batch_id =>@batch.id,:current_batch_id => params[:transfer][:to], :school_id => MultiSchool.current_school.id, :student_id => s.id, :is_refunded => false, :is_deducted => false)
                batch_transfer_fee.save
              end
            end
          end
          batch = @batch
          @stu = Student.find_all_by_batch_id(batch.id)
          if @stu.empty?
            batch.update_attribute :is_active, false
            Subject.find_all_by_batch_id(batch.id).each do |sub|
              sub.employees_subjects.each do |emp_sub|
                emp_sub.delete
              end
            end
          end
          flash[:notice] = "#{t('flash1')}"
          batch = Batch.find params[:transfer][:to]
          if batch.course.enable_student_elective_selection
            unless batch.elective_groups.active.empty? #&& batch.subjects.select{|es| es.elective_group_id != nil}.blank?
              batch.elective_groups.active.each do |eg|
                if !eg.end_date.nil? && !eg.subjects.active.empty? && eg.end_date >= Date.today
                  end_date = eg.end_date
                  recipients_array = []
                  students.each do |s|
                    recipients_array << s.user.id << (s.immediate_contact.user.id unless s.immediate_contact.nil? )
                  end
                end
                sender = current_user.id
                Reminder.send_message(recipients_array,sender,end_date,eg.name)
              end
            end
          end
          redirect_to :controller => 'batch_transfers'
        else
          @batches = Batch.active - @batch.to_a
          @batch.errors.add_to_base("#{t('select_a_batch_to_continue')}")
          render :template=> "rank_batch_transfers/show_with_tmpl"
        end
      else
        redirect_to :action=>"show_with_tmpl", :id=> params[:id]
      end
    end

    # def fee_check_box
    #   unless params[:to].nil?
    #     @batch = Batch.find(params[:to])
    #     if @batch.finance_fee_categories.present?
    #       render :update do |page|
    #         page.replace_html 'che-box', :partial => 'rank_batch_transfers/batch_transfer_with_fee_paid'
    #       end
    #     else
    #       render :update do |page|
    #         page.replace_html 'che-box', :text => 'No Fee category created for this batch'
    #       end
    #     end
    #   else
    #     render :update do |page|
    #       page.replace_html 'che-box', :text => ''
    #     end
    #   end
    # end
    def refund_details
      student=Student.find(params[:id])
      batch_transfer_fee = BatchTransferWithFeePaid.find_by_student_id_and_current_batch_id(student.id,student.batch.id)
      batch_transfer_fee.update_attribute(:is_refunded, true)
      render :update do |page|
        page.replace_html 'refund', :partial => 'rank_batch_transfers/refund_details'
      end
    end
    def discount_details
      student=Student.find(params[:id])
      batch_transfer_fee = BatchTransferWithFeePaid.find_by_student_id_and_current_batch_id(student.id,student.batch.id)
      paid_amount=batch_transfer_fee.paid_amount
      previous_batch = Batch.find(batch_transfer_fee.previous_batch_id)
      batch_transfer_fee.update_attribute(:is_deducted, true)
      @financefee=FinanceFee.find_all_by_student_id_and_batch_id(student.id,student.batch.id)
      @finance_amount=@financefee.map{|i| i.balance.to_f}.inject{|sum,y| sum + y }
      @financefee.each do |f|
        @finance_fee_category=f.finance_fee_collection.fee_category
        # @fee_discount=FeeDiscount.new(:receiver_type=>"Student", :receiver_id=>student.id, :batch_id=>f.batch_id, :finance_fee_category_id=>@finance_fee_category.id, :is_deleted=>true, :is_instant=>true, :is_amount=>true, :discount=>paid_amount, :master_receiver_type=>"Student", :master_receiver_id=>student.id, :name=>previous_batch.full_name)
        @fee_discount=FeeDiscount.new(:receiver_type=>"Student", :receiver_id=>student.id, :batch_id=>f.batch_id, :finance_fee_category_id=>@finance_fee_category.id, :is_deleted=>true, :is_amount=>true, :discount=>paid_amount, :name=>previous_batch.full_name)
        fee_particulars = f.finance_fee_collection.finance_fee_particulars.all(:conditions => "batch_id=#{f.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==f.student or par.receiver==f.student_category or par.receiver==f.batch) }
        total_payable=fee_particulars.map { |s| s.amount }.sum.to_f
        discount_amount=@fee_discount.is_amount? ? (total_payable*(@fee_discount.discount.to_f)/total_payable) : (total_payable*(@fee_discount.discount.to_f)/100)
        unless paid_amount == 0 
          if f.balance.to_f >= discount_amount.to_f
            @fee_discount.discount = paid_amount
            paid_amount = 0
            @fee_discount.save
            balance=f.balance-@fee_discount.discount
             f.update_attribute(:balance,balance)
            #   CollectionDiscount.create(:fee_discount_id => @fee_discount.id, :finance_fee_collection_id => f.fee_collection_id)
            #   FinanceFeeParticular.add_or_remove_particular_or_discount(@fee_discount, f.finance_fee_collection)
            # end
            # if @fee_discount.save
            #   CollectionDiscount.create(:fee_discount_id => @fee_discount.id, :finance_fee_collection_id => f.fee_collection_id)
            #   FinanceFeeParticular.add_or_remove_particular_or_discount(@fee_discount, f.finance_fee_collection)
            # end
          else
            @fee_discount.discount = f.balance.to_f
            paid_amount = paid_amount.to_f - f.balance.to_f
            @fee_discount.save
            balance=f.balance-@fee_discount.discount
            f.update_attribute(:balance,balance)
            # if @fee_discount.save
            #   CollectionDiscount.create(:fee_discount_id => @fee_discount.id, :finance_fee_collection_id => f.fee_collection_id)
            #   FinanceFeeParticular.add_or_remove_particular_or_discount(@fee_discount, f.finance_fee_collection)
            # end
          end
          # unless  (discount_amount.to_f >= f.balance.to_f)
          #   if @fee_discount.save
          #     CollectionDiscount.create(:fee_discount_id => @fee_discount.id, :finance_fee_collection_id => f.fee_collection_id)
          #     FinanceFeeParticular.add_or_remove_particular_or_discount(@fee_discount, f.finance_fee_collection)
          #     break
          #   end
          # else
          #   @fee_discount.errors.add_to_base(t('discount_cannot_be_greater_than_total_amount'))
          # end
        end
      end
      render :update do |page|
        page.replace_html 'refund1', :partial => 'rank_batch_transfers/discount_details'
      end
    end

    def subject_transfer_with_tmpl
    @batch = Batch.find(params[:id])
    @elective_groups = @batch.elective_groups.all(:conditions => {:is_deleted => false})
    @normal_subjects = @batch.normal_batch_subject
    @elective_subjects = Subject.find_all_by_batch_id(@batch.id,:conditions=>["elective_group_id IS NOT NULL AND is_deleted = false"])
    render :template => 'rank_batch_transfers/subject_transfer_with_tmpl'
  end

  end
end