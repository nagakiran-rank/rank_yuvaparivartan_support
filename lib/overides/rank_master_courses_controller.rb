module RankYuvaparivartanSupport
  module RankMasterCoursesController

    def self.included (base)
      base.instance_eval do
        alias_method_chain :new, :tmpl
        alias_method_chain :create, :tmpl
        alias_method_chain :edit, :tmpl
        alias_method_chain :update, :tmpl
        alias_method_chain :show, :tmpl
      end
    end

    def new_with_tmpl
      @master_course = MasterCourse.new
      @grade_types=MasterCourse.grading_types_as_options
      render :template => 'rank_master_courses/new_with_tmpl'
    end
   
    def create_with_tmpl
      @master_course = MasterCourse.new params[:master_course]
      if @master_course.save
        flash[:notice] = "Created course successfully"
        redirect_to master_courses_path
      else
        @grade_types=MasterCourse.grading_types_as_options
        render 'rank_master_courses/new_with_tmpl'
      end
    end

    def edit_with_tmpl
      @grade_types=MasterCourse.grading_types_as_options
      @grade_types = @grade_types.select{|g| @master_course.all_available_grading_type.map{|gr| gr.to_i}.include?(g[1].to_i)}
      render :template => 'rank_master_courses/edit_with_tmpl'
    end

    def update_with_tmpl
      if @master_course.update_attributes(params[:master_course])
        flash[:notice] = "Updated course details successfully"
        redirect_to master_courses_path
      else
        @grade_types=MasterCourse.grading_types_as_options
        @grade_types = @grade_types.select{|g| @master_course.all_available_grading_type.map{|gr| gr.to_i}.include?(g[1].to_i)}
        render 'rank_master_courses/edit_with_tmpl'
      end
    end

    def show_with_tmpl
      @grade_types=MasterCourse.grading_types_as_options
      courses = @master_course.active_courses
      @schools=School.find_all_by_id(courses.map{|c| c["school_id"]})
      render 'rank_master_courses/show_with_tmpl'
    end   
  end
end