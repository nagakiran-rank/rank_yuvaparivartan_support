class CreatePreStudentDocumentUploads < ActiveRecord::Migration
  def self.up
    create_table :pre_student_document_uploads do |t|
    	t.integer :school_id
      t.references :pre_student_registration
      t.string  :attachment_file_name
      t.string  :attachment_content_type
      t.integer :attachment_file_size
      t.timestamps
    end
  end

  def self.down
    drop_table :pre_student_document_uploads
  end
end
