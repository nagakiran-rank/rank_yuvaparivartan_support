module RankYuvaparivartanSupport
  module RankFinanceDonation

    def self.included (base)
      base.instance_eval do
        alias_method_chain :create_finance_transaction, :tmpl
      end
    end
		def create_finance_transaction_with_tmpl
			transaction = FinanceTransaction.create(
				:title => "#{t('donation_from')}" + donor,
				:description => description,
				:amount => amount,
				:transaction_date => transaction_date,
				:category => FinanceTransactionCategory.find_by_name('Donation'),
				:balance => amount
				)
			self.transaction_id = transaction.id
		end
	end
end