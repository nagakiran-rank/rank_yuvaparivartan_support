module RankYuvaparivartanSupport
  module RankFinanceTransaction

    def self.included (base)
      base.instance_eval do
        alias_method_chain :create_cancelled_transaction, :tmpl
        alias_method_chain :set_fine, :tmpl
      end
    end
    
    def create_cancelled_transaction_with_tmpl
      finance_transaction_attributes=self.attributes
      balance=finance.balance+(amount-fine_amount)
      finance.update_attributes(:is_paid => false, :balance => balance)

      FeeTransaction.destroy_all({:finance_transaction_id => id})
      # bdft=BankDepositFinanceTransaction.find_by_finance_transaction_id(id) 
      # unless bdft.nil?
      # else
        finance_transaction_attributes.delete "id"
        finance_transaction_attributes.delete "created_at"
        finance_transaction_attributes.delete "updated_at"
        finance_transaction_attributes.delete "multi_fees_transaction_id"
        finance_transaction_attributes.delete "finance_transaction_id"
        finance_transaction_attributes.delete "Manual_receipt_no"
        finance_transaction_attributes.delete "Manual_receipt_date"
        finance_transaction_attributes.delete "fee_deposit"
        finance_transaction_attributes.delete "balance"
        finance_transaction_attributes.delete "deposited_date"
        
        finance_transaction_attributes.merge!(:user_id => Fedena.present_user.id, :collection_name => finance.finance_fee_collection.name)
        finance_transaction_attributes.delete "id"
        cancelled_transaction=CancelledFinanceTransaction.new(finance_transaction_attributes)
        cancelled_transaction.save
      # end
      # finance_transaction_attributes=self.attributes
      # if finance_type=='FinanceFee'
      #   balance=finance.balance+(amount-fine_amount)
      #   finance.update_attributes(:is_paid => false, :balance => balance)

      #   FeeTransaction.destroy_all({:finance_transaction_id => id})
      # end
      # if finance.present? and ["FinanceFee","HostelFee","TransportFee","InstantFee"].include? finance_type
      #   collection_name=finance.name
      #   finance_type_name=finance_type
      # else
      #   if category_name=='Refund' and fee_refund.present? and fee_refund.finance_fee.present?
      #     collection_name=fee_refund.finance_fee.name
      #   else
      #     collection_name=nil
      #   end
      #   finance_type_name=category_name
      # end
      # finance_transaction_attributes.merge!(:user_id => Fedena.present_user.id, :finance_type => finance_type_name,:collection_name =>collection_name)
      # finance_transaction_attributes.delete "id"
      # finance_transaction_attributes.delete "created_at"
      # finance_transaction_attributes.delete "updated_at"
      # finance_transaction_attributes.delete "multi_fees_transaction_id"
      # finance_transaction_attributes.delete "finance_transaction_id"
      # finance_transaction_attributes.delete "balance"
      # finance_transaction_attributes.delete "fee_deposit"
      # finance_transaction_attributes.delete "deposited_date"

      # dependend_destroy_models=FinanceTransaction.reflect_on_all_associations.select{|a| a.options[:dependent]==:destroy}.map{|d| d.name}
      # other_details={}
      # dependend_destroy_models.each do |ddm|
      #   if instance_eval(ddm.to_s).respond_to? 'fetch_other_details_for_cancelled_transaction'
      #     other_details=instance_eval(ddm.to_s).fetch_other_details_for_cancelled_transaction
      #   end
      # end
      # finance_transaction_attributes.merge!(:other_details=>other_details,:finance_transaction_id=>id,:lastvchid=>-(lastvchid.to_i.abs))
      # cancelled_transaction=CancelledFinanceTransaction.new(finance_transaction_attributes)
      # cancelled_transaction.save
    end

    def set_fine_with_tmpl
      # balance=finance.balance+fine_amount-(amount)
      if finance_type=="FinanceFee"
        balance=finance.balance
        manual_fine= fine_amount.present? ? fine_amount.to_f : 0
        fee_balance=balance
        actual_amount=balance+finance.finance_transactions.sum(:amount)-finance.finance_transactions.sum(:fine_amount)
        date=finance.finance_fee_collection
        days=(Date.today-date.due_date.to_date).to_i
        auto_fine=date.fine
        fine_amount=0
        if auto_fine.present?
          fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{date.created_at}'"], :order => 'fine_days ASC')
          fine_amount=fine_rule.is_amount ? fine_rule.fine_amount : (actual_amount*fine_rule.fine_amount)/100 if fine_rule
          paid_fine=finance.finance_transactions.find(:all, :conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
          fine_amount=fine_amount-paid_fine
        end
        actual_balance=FedenaPrecision.set_and_modify_precision(finance.balance+fine_amount).to_f
        amount_paying=FedenaPrecision.set_and_modify_precision(amount-manual_fine).to_f
        actual_balance=0 if FinanceFee.find(finance.id).is_paid
        unless self.balance.present? or self.Manual_receipt_no.present? or self.Manual_receipt_date.present?
          if amount_paying > actual_balance and description!='fine_amount_included'
            errors.add_to_base(t('finance.flash19'))
            return false
          end
        end
      end
    end

end
end