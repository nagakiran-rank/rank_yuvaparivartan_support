function add_document_uploads(link, association, content){
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g");
    if ($$(".addl_attachments").first().select("div.fields").size()==4){
        alert("Cant add more than 4 additional attachments.");
    }else{
        $(link).up().insert({
            before: content.replace(regexp, new_id)
        });
    }
}
function remove_fields_pre(link){
    //    console.log($(link).previous("input[type=hidden]"));
    $(link).previous("input[type=hidden]").value='1';
    $(link).up(".fields").hide();
     j(link.up(".fields")).attr('class',"new_class")
}