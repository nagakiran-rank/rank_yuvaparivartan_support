class AddColToPreStudentRegistration < ActiveRecord::Migration
  def self.up
  	add_column :pre_student_registrations, :transferred, :boolean ,:default => false
  end

  def self.down
  end
end
