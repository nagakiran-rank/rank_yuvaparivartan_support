class CreatePreFinanceTransactions < ActiveRecord::Migration
  def self.up
    create_table :pre_finance_transactions do |t|
      t.string :title
      t.string :description
      t.decimal :amount
      t.date :transaction_date
      t.references :pre_student_registration
      t.string :payee_type
      t.string :receipt_no
      t.integer :school_id
      t.string :payment_mode
      t.text :payment_note
      t.integer :user_id
      t.integer :course_id
      t.decimal :balance
      t.date :deposited_date
      t.timestamps
    end
  end

  def self.down
    drop_table :pre_finance_transactions
  end
end
