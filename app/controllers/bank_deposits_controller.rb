class BankDepositsController < ApplicationController
  before_filter :login_required
  filter_access_to :all
  require 'will_paginate/collection'

  def index 
    type_of_deposit =params[:type_of_deposit]
    last_transaction = BankDepositFinanceTransaction.last
    last_bdft_no = last_transaction.bank_deposit_finance_transaction_number unless last_transaction.nil?
    unless last_bdft_no.nil?
     bdft_number = last_bdft_no.next
    else
      bdft_number = "1"
    end
    if type_of_deposit == "Fee Deposit" 
      @bankupload =BankDepositYuvaBank.new(:bank_id=>params[:bank_id],:total_amount =>params[:total_amount],:upload_document => params[:upload_document],:date_of_collection =>params[:date_of_collection],:date_of_deposit => params[:date_of_deposit])
      if params[:pre_student_checkbox] == "true"        
        if request.post? && @bankupload.save
          if params[:transactions].present?
            params[:transactions].each do |f, details|
              @bank_deposits_yuva = BankDepositFinanceTransaction.new(:type_of_deposit=>params[:type_of_deposit], :date_of_collection =>params[:date_of_collection], :date_of_deposit =>params[:date_of_deposit] , :finance_transaction_id=>f, :fee_collected=>details[:fee_collected], :employee_id=>@current_user.id, :fee_to_be_deposited=>details[:fee_to_be_deposited], :notes=>params[:notes], :bank_deposit_yuvaparivarthan_bank_id=>@bankupload.id,:vertical_id=>params[:type_of_vertical])
              if request.post? and @bank_deposits_yuva.fee_to_be_deposited!=nil and @bank_deposits_yuva.fee_to_be_deposited!=0
                bdft_deposit_amount = 0
                bdft_deposit = BankDepositFinanceTransaction.find_all_by_finance_transaction_id(f)
                unless bdft_deposit.empty?
                  bdft_deposit_amount = bdft_deposit.map{|p| p.fee_to_be_deposited}.inject{|sum,y| sum + y}
                end
                @bank_deposits_yuva.save                
                balance = @bank_deposits_yuva.fee_collected - (@bank_deposits_yuva.fee_to_be_deposited + bdft_deposit_amount)
                @bank_deposits_yuva.update_attributes(:bank_deposit_finance_transaction_number => bdft_number, :balance => balance)
                bdft_deposit.each do |deposit|
                  deposit.update_attributes(:balance => balance)
                end
              end
            end
          end
          if params[:pre_transactions].present?
            params[:pre_transactions].each do |p, pdetails|
              @bank_deposits_yuva = BankDepositFinanceTransaction.new(:type_of_deposit=>params[:type_of_deposit], :date_of_collection =>params[:date_of_collection], :date_of_deposit =>params[:date_of_deposit] , :pre_finance_transaction_id=>p, :fee_collected=>pdetails[:fee_collected], :employee_id=>@current_user.id, :fee_to_be_deposited=>pdetails[:fee_to_be_deposited], :notes=>params[:notes], :bank_deposit_yuvaparivarthan_bank_id=>@bankupload.id,:vertical_id=>params[:type_of_vertical])
              if request.post? and @bank_deposits_yuva.fee_to_be_deposited!=nil and @bank_deposits_yuva.fee_to_be_deposited!=0
                bdft_deposit_amount = 0
                bdft_deposit = BankDepositFinanceTransaction.find_all_by_finance_transaction_id(p)
                unless bdft_deposit.empty?
                  bdft_deposit_amount = bdft_deposit.map{|p| p.fee_to_be_deposited}.inject{|sum,y| sum + y}
                end
                @bank_deposits_yuva.save                
                balance = @bank_deposits_yuva.fee_collected - (@bank_deposits_yuva.fee_to_be_deposited + bdft_deposit_amount)
                @bank_deposits_yuva.update_attributes(:bank_deposit_finance_transaction_number => bdft_number, :balance => balance)
                bdft_deposit.each do |deposit|
                  deposit.update_attributes(:balance => balance)
                end
              end
            end
          end
        end
      else
        if request.post? && @bankupload.save
          if params[:transactions].present?
            params[:transactions].each do |f, details|
              @bank_deposits_yuva = BankDepositFinanceTransaction.new(:type_of_deposit=>params[:type_of_deposit], :date_of_collection =>params[:date_of_collection], :date_of_deposit =>params[:date_of_deposit] , :finance_transaction_id=>f, :fee_collected=>details[:fee_collected], :employee_id=>@current_user.id, :fee_to_be_deposited=>details[:fee_to_be_deposited], :notes=>params[:notes], :bank_deposit_yuvaparivarthan_bank_id=>@bankupload.id,:vertical_id=>params[:type_of_vertical])
              if request.post? and @bank_deposits_yuva.fee_to_be_deposited!=nil and @bank_deposits_yuva.fee_to_be_deposited!=0
                bdft_deposit_amount = 0
                bdft_deposit = BankDepositFinanceTransaction.find_all_by_finance_transaction_id(f)
                unless bdft_deposit.empty?
                  bdft_deposit_amount = bdft_deposit.map{|p| p.fee_to_be_deposited}.inject{|sum,y| sum + y}
                end
                @bank_deposits_yuva.save
                balance = @bank_deposits_yuva.fee_collected - (@bank_deposits_yuva.fee_to_be_deposited + bdft_deposit_amount)
                @bank_deposits_yuva.update_attributes(:bank_deposit_finance_transaction_number => bdft_number, :balance => balance)
              end
            end
          end
        end
      end
      flash[:notice] = "#{t('fee_amount_has_been_successfully_added')}"           
    elsif type_of_deposit == "Donation Deposit"
      @bankupload =BankDepositYuvaBank.new(:bank_id=>params[:bank_id],:total_amount =>params[:total_amount],:upload_document => params[:upload_document],:date_of_collection =>params[:date_of_collection],:date_of_deposit => params[:date_of_deposit])
      if request.post? and @bankupload.save
        params[:transactions].each do |f, details|
          @bank_deposits_yuva = BankDepositFinanceTransaction.new(:type_of_deposit=>params[:type_of_deposit], :date_of_collection =>params[:date_of_collection], :date_of_deposit =>params[:date_of_deposit] , :finance_transaction_id=>f, :fee_collected=>details[:fee_collected], :employee_id=>@current_user.id, :fee_to_be_deposited=>details[:fee_to_be_deposited], :notes=>params[:notes], :bank_deposit_yuvaparivarthan_bank_id=>@bankupload.id)
          if request.post? and @bank_deposits_yuva.fee_to_be_deposited!=nil and @bank_deposits_yuva.fee_to_be_deposited!=0
            @bank_deposits_yuva.save
            @bank_deposits_yuva.update_attributes(:bank_deposit_finance_transaction_number => bdft_number)
          end
        end 
        flash[:notice] = "#{t('donation_amount_has_been_added_successfully')}"
      end
    else
      @bankupload = BankDepositYuvaBank.new(:bank_id=>params[:bank_id],:total_amount =>params[:total_amount],:upload_document => params[:upload_document],:date_of_collection =>params[:date_of_collection],:date_of_deposit => params[:date_of_deposit])
      if request.post? and @bankupload.save
        @bank_deposits_yuva = BankDepositFinanceTransaction.new(:type_of_deposit=>params[:type_of_deposit],:date_of_collection =>params[:date_of_collection],:date_of_deposit => params[:date_of_deposit], :bank_deposit_yuvaparivarthan_bank_id=>@bankupload.id, :employee_id=>@current_user.id)
        @bank_deposits_yuva.save
        @bank_deposits_yuva.update_attributes(:bank_deposit_finance_transaction_number => bdft_number)
        flash[:notice] = "#{t('other_amount_has_been_added_successfully')}"
      end           
    end
  end         
  
  def current_calender_date   
    @yuvabank = YuvaparivarthanBank.all
    render :update do |page|
      if params[:type_of_deposit] == "Fee Deposit"
        page.replace_html 'calender', :partial => 'bank_deposits/calender'
      elsif params[:type_of_deposit] == "Donation Deposit" 
        page.replace_html 'calender', :partial => 'bank_deposits/calender'
      elsif params[:type_of_deposit] == "" 
        page.replace_html 'calender', :partial => 'bank_deposits/nothing'       
      else
        page.replace_html 'calender', :partial => 'bank_deposits/current_calender_date' 
      end
    end
  end
  
  def fees_deposit_details
    @yuvabank = YuvaparivarthanBank.all
    end_date=params[:end_date].to_date unless params[:end_date].nil?
    start_date=(params[:start_date]).to_date unless params[:start_date].nil?
    if params[:pre_student] == "true"
      # @obj = BankDepositFinanceTransaction.find_all_by_pre_finance_and_reject_status_and_admin_status(true,false,true)
      @obj = BankDepositFinanceTransaction.find_all_by_admin_status_and_pre_finance(true,false)
      g = @obj.map(&:pre_finance_transaction_id)
      pre_finance_transaction = PreFinanceTransaction.find(:all, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}' and transferred = false"]).reject{ |e| g.include? e.id }
      @appr = BankDepositFinanceTransaction.find_all_by_admin_status(true)
      # wrt = WriteoffDeposit.find_all_by_reject(false).collect(&:student_id)
      wrt = WriteoffDeposit.find(:all, :conditions => ["approve = true or reject = false"]).collect(&:student_id)
      k = @appr.map(&:finance_transaction_id)
      # finance_transactions = FinanceTransaction.find(:all, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and balance !=0"]).reject{ |l| k.include? l.id }
      finance_transactions = FinanceTransaction.find(:all, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'and balance !=0"])
      finance_transaction = finance_transactions.reject{ |r| wrt.include? r.payee_id }
      # finance_transaction = PreFinanceTransaction.find(g)

      @finance_fees = []
      @pre_finance_fees = []
      @pre_finance_fees = pre_finance_transaction
      @finance_fees = finance_transaction
    else
      @apprv = BankDepositFinanceTransaction.find_all_by_admin_status(true)
      # wrt = WriteoffDeposit.find_all_by_reject(false).collect(&:student_id)
      wrt = WriteoffDeposit.find(:all, :conditions => ["approve = true or reject = false"]).collect(&:student_id)
      y = @apprv.map(&:finance_transaction_id)
      # finance_transactions = FinanceTransaction.find_all_by_batch_id(params[:batch_ids].split(",")).reject{ |r| y.include? r.id }
      finance_transactions = FinanceTransaction.find_all_by_batch_id(params[:batch_ids].split(","))
      finance_transaction = finance_transactions.reject{ |r| wrt.include? r.payee_id }
      # finance_transaction = FinanceTransaction.find_all_by_batch_id_and_pre_finance_amt(params[:batch_ids].split(","),false)
      # if !start_date.nil?
      #   finance_transaction=FinanceTransaction.find(:all, :conditions => ["transaction_date >= '#{start_date}' and balance !=0 and pre_finance_amt = false"])
      #   @finance_fees=[]
      #   unless finance_transaction.empty?
      #     finance_transaction.each do |transaction|
      #       unless transaction.finance_id.nil?
      #         @finance_fees<<transaction
      #       end
      #     end
      #   end
      # elsif !end_date.nil?
      #   finance_transaction=FinanceTransaction.find(:all, :conditions => ["transaction_date <= '#{end_date}'  and balance !=0 and pre_finance_amt = false"])
      #   @finance_fees=[]
      #   unless finance_transaction.empty?
      #     finance_transaction.each do |transaction|
      #       unless transaction.finance_id.nil?
      #         @finance_fees<<transaction
      #       end
      #     end
      #   end
      # elsif !start_date.nil? && !end_date.nil?
      #   unless start_date > end_date
      #     finance_transaction=FinanceTransaction.find(:all, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'  and balance !=0 and pre_finance_amt = false"])
      #     @finance_fees=[]
      #     unless finance_transaction.empty?
      #       finance_transaction.each do |transaction|
      #         unless transaction.finance_id.nil?
      #           @finance_fees<<transaction
      #         end
      #       end
      #     end
      #   else
      #     flash[:warn_notice] = "#{t('flash17')}"
      #   end
      # else
        @finance_fees = finance_transaction
        # flash[:warn_notice] = "#{t('flash17')}"
      # end
    end
    # if params[:pre_student] == "true"
    #     render :update do |page|
    #       page.replace_html 'fees_deposit', :partial => 'bank_deposits/pre_finance'
    #     end
    # else
      render :update do |page|
        page.replace_html 'fees_deposit', :partial => 'bank_deposits/fees_deposit_details'
      end
    # end
  end

  def yuva_bank_details
    @yuvabank = YuvaparivarthanBank.find_by_id(params[:bank_id])
    @type_of_deposit=params[:deposit_type]
    if params[:bank_id] == ""
      render :update do |page|
        page.replace_html 'bank_details_yuva', :partial => 'bank_deposits/yuva_bank_details1'
      end
    else
      render :update do |page|
        page.replace_html 'bank_details_yuva', :partial => 'bank_deposits/yuva_bank_details'
      end
    end
  end

  def bank_deposit_details
    if params[:page].present?
        page_deposit = params[:page]
    else
      page_deposit = 1
    end
    if params[:per_page].present?
      per_page_deposit = params[:per_page]
    else
      per_page_deposit = 10
    end
    @write_off_bdft =  WriteoffDeposit.find_all_by_reject(false).map{|p| p.bank_deposit_finance_transaction_id}.uniq
    pre_finance =[]
    bdft_all = BankDepositFinanceTransaction.all
    bdft_pre = bdft_all.collect(&:pre_finance_transaction_id)
    bdft_pre1=bdft_pre.reject{|a| a.nil?}
    unless bdft_pre1.empty?
      pre_finance = PreFinanceTransaction.find(bdft_pre1)
    end
    pre_trans= []
    unless pre_finance.empty?
      pre_finance.each do |pf|
        if pf.transferred
          pre_trans << pf
        end
      end
    end
    unless pre_trans.empty?
      pre_trans.each do |pre|
        finance_tra = FinanceTransaction.find_by_pre_receipt_no(pre.receipt_no)
        puts"--finance_tra------#{finance_tra.inspect}----"
        bdft = BankDepositFinanceTransaction.find_all_by_pre_finance_transaction_id(pre.id)
        puts"<<bdft<<<<#{bdft.inspect}<<<<"
        bdft.each do |b|
          b.update_attributes(:finance_transaction_id => finance_tra.id,:batch_id => finance_tra.batch_id, :pre_finance_transaction_id => nil)
        end
      end
    end
    b=BankDepositFinanceTransaction.all.map {|p| p.bank_deposit_finance_transaction_number}.uniq
    a = b.map(&:to_i).reject{ |e| @write_off_bdft.include? e }.reverse                                               
    @bdf=[]
    unless a.empty?
     a.map{|p| @bdf<<BankDepositFinanceTransaction.find_by_bank_deposit_finance_transaction_number(p)}

      @bdft =  WillPaginate::Collection.create( page_deposit, per_page_deposit, @bdf.length) do |pager|
        pager.replace @bdf[pager.offset, pager.per_page]
      end 
    end
  end

  def edit_bank_deposit_details
    @bankyuva = YuvaparivarthanBank.all
    @all_bdft=BankDepositFinanceTransaction.find_all_by_bank_deposit_finance_transaction_number(params[:id])
    if @all_bdft.first.type_of_deposit=="Fee Deposit" or @all_bdft.first.type_of_deposit=="Donation Deposit"
      if @all_bdft.first.finance_transaction_id.present? && @all_bdft.first.pre_finance_transaction_id.present?
       @finance=FinanceTransaction.find(@all_bdft.first.finance_transaction_id)
       @pre_finance=PreFinanceTransaction.find(@all_bdft.first.pre_finance_transaction_id)
      elsif @all_bdft.first.finance_transaction_id.nil?
       @pre_finance=PreFinanceTransaction.find(@all_bdft.first.pre_finance_transaction_id)
      elsif @all_bdft.first.pre_finance_transaction_id.nil?
       @finance=FinanceTransaction.find(@all_bdft.first.finance_transaction_id)
      end
    end
    unless @all_bdft.empty?
      if @all_bdft.first.admin_status == false
        @bank =BankDepositYuvaBank.find_by_id(@all_bdft.first.bank_deposit_yuvaparivarthan_bank_id)
        @yuvabank =YuvaparivarthanBank.find_by_id(@bank.bank_id)
        @emp=User.find_by_id(@all_bdft.first.employee_id)
        if request.post?
          if @all_bdft.first.type_of_deposit=="Fee Deposit"
            if params[:total_amount].present?
              @bank.update_attributes(:total_amount =>params[:total_amount])
            end
            if params[:upload_document].present? and params[:total_amount].present?
              @bank.update_attributes(:upload_document => params[:upload_document], :total_amount =>params[:total_amount])
            end
            if params[:bank_id].present?
              @bank.update_attributes(:bank_id=>params[:bank_id],:total_amount =>params[:total_amount],:date_of_collection =>params[:date_of_collection],:date_of_deposit => params[:date_of_deposit])
            end
             if params[:date_of_deposit].present?
              @bank.update_attributes(:date_of_deposit=>params[:date_of_deposit])
             end
            params[:transactions].each_pair do |f, details|
              @all_bdft.each do |b|
                if b.finance_transaction_id.nil?  == false
                  if b.finance_transaction_id==f.to_i
                    b.update_attributes(:date_of_deposit =>params[:date_of_deposit] , :employee_id=>@current_user.id,:fee_to_be_deposited=>details[:fee_to_be_deposited],:notes=>details[:notes],:bank_deposit_yuvaparivarthan_bank_id=>@bank.id)
                    bdft_deposit_amount = 0
                    bdft_deposit = BankDepositFinanceTransaction.find_all_by_finance_transaction_id(b.finance_transaction_id)
                    unless bdft_deposit.empty?
                      bdft_deposit_amount = bdft_deposit.map{|p| p.fee_to_be_deposited}.inject{|sum,y| sum + y}
                    end 
                    balance = b.fee_collected - (bdft_deposit_amount)
                    b.update_attributes(:balance => balance)
                    bdft_deposit.each do |deposit|
                      deposit.update_attributes(:balance => balance)
                    end
                    if b.fee_to_be_deposited.nil?
                      b.destroy
                    end
                  end
                else
                  if b.pre_finance_transaction_id==f.to_i
                    b.update_attributes(:date_of_deposit =>params[:date_of_deposit] , :employee_id=>@current_user.id,:fee_to_be_deposited=>details[:fee_to_be_deposited],:notes=>details[:notes],:bank_deposit_yuvaparivarthan_bank_id=>@bank.id)
                    bdft_deposit_amount = 0
                    bdft_deposit = BankDepositFinanceTransaction.find_all_by_pre_finance_transaction_id(b.pre_finance_transaction_id)
                    unless bdft_deposit.empty?
                      bdft_deposit_amount = bdft_deposit.map{|p| p.fee_to_be_deposited}.inject{|sum,y| sum + y}
                    end
                    balance = b.fee_collected - (bdft_deposit_amount)
                    b.update_attributes(:balance => balance)
                    bdft_deposit.each do |deposit|
                      deposit.update_attributes(:balance => balance)
                    end
                    if b.fee_to_be_deposited.nil?
                      b.destroy
                    end
                  end
                end
              end
            end
            flash[:notice] = "#{t('fee_amount_has_been_successfully_updated')}"
          elsif @all_bdft.first.type_of_deposit=="Donation Deposit"
            if params[:total_amount].present?
              @bank.update_attributes(:total_amount =>params[:total_amount])
            end
            if params[:upload_document].present? and params[:total_amount].present?
              @bank.update_attributes(:upload_document => params[:upload_document], :total_amount =>params[:total_amount])
            end
            if params[:bank_id].present?
              @bank.update_attributes(:bank_id=>params[:bank_id],:total_amount =>params[:total_amount], :date_of_collection =>params[:date_of_collection],:date_of_deposit => params[:date_of_deposit])
            end
            # if params[:date_of_collection].present?
            #   @bank.update_attributes(:date_of_collection=>params[:date_of_collection])
            # end
            params[:transactions].each_pair do |f, details|
              @all_bdft.each do |b|
                if b.finance_transaction_id==f.to_i
                  b.update_attributes(:date_of_deposit =>params[:date_of_deposit] , :employee_id=>@current_user.id,:fee_to_be_deposited=>details[:fee_to_be_deposited],:bank_deposit_yuvaparivarthan_bank_id=>@bank.id)
                  if b.fee_to_be_deposited.nil?
                    b.destroy
                  end
                end
              end
            end
            flash[:notice] = "#{t('donation_amount_has_been_updated_successfully')}"
          else
            if params[:total_amount].present? and params[:total_amount].to_i != 0
              @bank.update_attributes(:total_amount =>params[:total_amount])
            else
              @all_bdft.first.destroy
            end
            if params[:bank_id].present?
              @bank.update_attributes(:bank_id=>params[:bank_id],:total_amount =>params[:total_amount], :date_of_collection =>params[:date_of_collection],:date_of_deposit => params[:date_of_deposit])
            end
            if params[:upload_document].present?
              @bank.update_attributes(:upload_document=>params[:upload_document])
            end
            # if params[:date_of_collection].present?
            #   @bank.update_attributes(:date_of_collection=>params[:date_of_collection])
            # end
            # if params[:date_of_deposit].present?
            #   @bank.update_attributes(:date_of_deposit=>params[:date_of_deposit])
            # end
            flash[:notice] = "#{t('other_amount_has_been_updated_successfully')}"
          end
          redirect_to :controller => "bank_deposits",:action => "bank_deposit_details"
        end
      else
        flash[:notice] = "Already Approved"
        redirect_to :controller => "bank_deposits",:action => "bank_deposit_details"
      end
    else
      flash[:notice] = "No Transaction Found"
        redirect_to :controller => "bank_deposits",:action => "bank_deposit_details"
    end
  end

  def approve_bank_deposit_details
    @bank_deposit_details=BankDepositFinanceTransaction.find_all_by_bank_deposit_finance_transaction_number(params[:id])
    bdft_details = BankDepositFinanceTransaction.find_all_by_admin_status(true)
    finance_details = bdft_details.collect(&:finance_transaction_id).compact
    pre_finance_details = bdft_details.collect(&:pre_finance_transaction_id).compact
    @bank_deposit_details.each do |b|
      if b.type_of_deposit=="Fee Deposit" or b.type_of_deposit=="Donation Deposit"
        if b.pre_finance_transaction_id.nil?  == false
          @finance_transaction_details=PreFinanceTransaction.find(b.pre_finance_transaction_id)
          finance_transaction_balance=(@finance_transaction_details.balance-b.fee_to_be_deposited).to_f
          @finance_transaction_details.update_attributes(:balance=>finance_transaction_balance)
          unless pre_finance_details.include? b.pre_finance_transaction_id
            b.update_attribute(:status,true)
          end
        else
          @finance_transaction_details=FinanceTransaction.find(b.finance_transaction_id)
          finance_transaction_balance=(@finance_transaction_details.balance-b.fee_to_be_deposited).to_f
          @finance_transaction_details.update_attributes(:balance=>finance_transaction_balance)
          unless finance_details.include? b.finance_transaction_id
            b.update_attribute(:status,true)
          end
        end
        b.update_attribute(:admin_status,true)
      else
        b.update_attribute(:admin_status,true)
      end
    end
    redirect_to :controller => "bank_deposits",:action => "bank_deposit_details"
  end 

  def show_bank_deposit_details
    @all_bdft=BankDepositFinanceTransaction.find_all_by_bank_deposit_finance_transaction_number(params[:id])
    if @all_bdft.first.type_of_deposit=="Fee Deposit" or @all_bdft.first.type_of_deposit=="Donation Deposit"
      if @all_bdft.first.finance_transaction_id.present? && @all_bdft.first.pre_finance_transaction_id.present?
       @finance=FinanceTransaction.find(@all_bdft.first.finance_transaction_id)
       @pre_finance=PreFinanceTransaction.find(@all_bdft.first.pre_finance_transaction_id)
      elsif @all_bdft.first.finance_transaction_id.nil?
       @pre_finance=PreFinanceTransaction.find(@all_bdft.first.pre_finance_transaction_id)
      elsif @all_bdft.first.pre_finance_transaction_id.nil?
       @finance=FinanceTransaction.find(@all_bdft.first.finance_transaction_id)
      end
    end
      @bank =BankDepositYuvaBank.find_by_id(@all_bdft.first.bank_deposit_yuvaparivarthan_bank_id)
      @yuvabank =YuvaparivarthanBank.find_by_id(@bank.bank_id)
      @emp=User.find_by_id(@all_bdft.first.employee_id)
      unless @all_bdft.empty?
       if @all_bdft.map{|p| p.bank_deposit_finance_transaction_number}.include?("true")
        redirect_to :controller => "bank_deposits",:action => "bank_deposit_details"
       end
     end
  end

  def download_attachment
    #download the  attached file
    begin
      @document = BankDepositYuvaBank.find params[:id]
      unless @document.nil?
        send_file  @document.upload_document.path , :type=>@document.upload_document_content_type
      end
    rescue Exception => e
      Rails.logger.info "Exception from rank_yuvaparivarthan_support pluggin in bank_deposits_controller, download_attachment action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong while downloading a file. Please inform administration"
      redirect_to :controller => "bank_deposits", :action => :bank_deposit_details
    end
  end

  def yuva_bank_details_edit
    @yuvabank = YuvaparivarthanBank.find_by_id(params[:bank_id])
    @bdft=BankDepositFinanceTransaction.find_by_bank_deposit_yuvaparivarthan_bank_id(params[:id])
    @bank =BankDepositYuvaBank.find_by_id(params[:id])
    render :update do |page|
      page.replace_html 'bank_details_yuva', :partial => 'bank_deposits/yuva_bank_details_edit'
    end
  end

  def end_date
    render :update do |page|
      if params[:type_of_deposit] == "Fee Deposit"
        page.replace_html 'end_date', :partial => 'bank_deposits/end_date'
      else
        page.replace_html 'end_date', :partial => 'bank_deposits/end_date1'
      end
    end
  end

  def donation_deposit_details
    @yuvabank = YuvaparivarthanBank.all
    start_date=(params[:date_of_collection]).to_date
    end_date=params[:end_date].to_date
    unless start_date > end_date
      finance_donation=FinanceDonation.find(:all, :conditions => ["transaction_date >= '#{start_date}' and transaction_date <= '#{end_date}'"])
      @finance_donation=[]
      unless finance_donation.empty?
        finance_donation.each do |donation|
          finance_transaction=FinanceTransaction.find(donation.transaction_id)
          # unless finance_transaction.empty?
            @finance_donation<<finance_transaction
          # end
        end
      end
    else
      flash[:warn_notice] = "#{t('flash17')}"
    end
    render :update do |page|
      page.replace_html 'donation_deposit', :partial => 'bank_deposits/donation_deposit_details'
    end
  end

  def reject_bank_deposit_details
    @bank_deposit_details=BankDepositFinanceTransaction.find_all_by_bank_deposit_finance_transaction_number(params[:id])
    if request.post? 
      @bank_deposit_details.each do |s|
        s.update_attributes(:reject_status => true , :reject_reason => params[:reject][:reject_reason])
      end
      render :update do |page|
        page.hide 'modal-box'
        page.reload
      end
    else   
      render :update do |page|
        page.replace_html 'modal-box', :partial => 'bank_deposits/reject_reason'
        page << "Modalbox.show($('modal-box'), {title: '#{t('reason_for_rejection')}', width: 650});"
      end
    end
  end

  def change_scanned_copy
    render :update do |page|
      page.replace_html 'scanned_copy_replace', :partial => 'bank_deposits/change_scanned_copy'
      page.hide "abcd"
    end
  end

  def vertical_index 
    type_of_vertical =YuvaVertical.find(params[:type_of_vertical])
    @batches = Batch.find_all_by_yuva_vertical_id(type_of_vertical.id)
    @bdft = BankDepositFinanceTransaction.find_all_by_vertical_id(params[:type_of_vertical])
    render :update do |page|
      if params[:type_of_vertical].present?
        page.show 'vertical'
      page.replace_html 'vertical', :partial => 'bank_deposits/vertical_index'  
      else
       page.replace_html 'vertical', :partial => 'bank_deposits/calender'
      end     
    end
  end

  def display_vertical
    @yuvabank = YuvaparivarthanBank.all
    render :update do |page|
       if params[:type_of_deposit] == "Fee Deposit"
        page.replace_html 'calender', :partial => 'bank_deposits/display_vertical' 
       elsif params[:type_of_deposit] == "Donation Deposit" 
        page.replace_html 'calender', :partial => 'bank_deposits/calender'
        page.hide 'vertical'
       elsif params[:type_of_deposit] == "" 
        page.replace_html 'calender', :partial => 'bank_deposits/nothing'       
        page.hide 'vertical'
       else
        page.replace_html 'calender', :partial => 'bank_deposits/current_calender_date' 
        page.hide 'vertical'
      end
    end
  end

  def write_off_index
    # if params[:pre_student_checkbox] == "true"
    #   @write_off_bdft =  WriteoffDeposit.find_all_by_reject(false).map{|p| p.bank_deposit_finance_transaction_id}.uniq
    #     b=BankDepositFinanceTransaction.find_all_by_pre_finance_and_reject_status(true,false).map {|p| p.bank_deposit_finance_transaction_number}.uniq
    #     a = b.map(&:to_i).reject{ |e| @write_off_bdft.include? e }.reverse
    #     @bdft=[]
    #     unless a.empty?
    #       a.map{|p| @bdft<<BankDepositFinanceTransaction.find_by_bank_deposit_finance_transaction_number(p.to_i)}
    #       # @bdft =  WillPaginate::Collection.create( page_deposit, per_page_deposit, @bdf.length) do |pager|
    #       #   pager.replace @bdf[pager.offset, pager.per_page]

    #       # end 
    #     end
    #   render :update do |page|
    #       page.replace_html 'listing', :partial => 'bank_deposits/pre_writeoff_index' 
    #   end
    #   else
        # @write_off_bdft =  WriteoffDeposit.find_all_by_reject(false).map{|p| p.bank_deposit_finance_transaction_id}.uniq
        # b= BankDepositFinanceTransaction.find_all_by_vertical_id_and_admin_status(params[:type_of_vertical],true).map{|p| p.bank_deposit_finance_transaction_number}.uniq
        # a = b.map(&:to_i).reject{ |e| @write_off_bdft.include? e }.reverse
        # @bdft=[]
        # unless a.empty?
        #   a.map{|p| @bdft<<BankDepositFinanceTransaction.find_by_bank_deposit_finance_transaction_number_and_admin_status(p.to_i,true)}.uniq
          # @bdft =  WillPaginate::Collection.create( page_deposit, per_page_deposit, @bdf.length) do |pager|
          #   pager.replace @bdf[pager.offset, pager.per_page]

          # end 
        # end
        # render :update do |page|
        #   page.replace_html 'listing', :partial => 'bank_deposits/write_off_index' 
        # end
      # end 
  finance_transaction = FinanceTransaction.find_all_by_batch_id(params[:batch_ids].split(","))
  puts"#{finance_transaction.inspect}.......cccccccccccccccc"
      render :update do |page|
        page.replace_html 'listing', :partial => 'bank_deposits/write_off_index' 
      end
      # end 
  end

  def write_off
  if params[:page].present?
        page_deposit = params[:page]
    else
      page_deposit = 1
    end
    if params[:per_page].present?
      per_page_deposit = params[:per_page]
    else
      per_page_deposit = 10
    end
    b=BankDepositFinanceTransaction.all.map {|p| p.bank_deposit_finance_transaction_number}.uniq
    a = b.reverse
    @bdf=[]
    unless a.empty?
     a.map{|p| @bdf<<BankDepositFinanceTransaction.find_by_bank_deposit_finance_transaction_number_and_admin_status(p,true)}
      @bdft =  WillPaginate::Collection.create( page_deposit, per_page_deposit, @bdf.length) do |pager|
        pager.replace @bdf[pager.offset, pager.per_page]
      end 
    end
  end

  def pre_student_calender
    render :update do |page|
        page.replace_html 'pre_calender', :partial => 'bank_deposits/pre_student_calender' 
    end
  end

  def view_student_finance_details
    @finance_transactions = FinanceTransaction.find_all_by_payee_id params[:id]
    if @finance_transactions.empty?
      redirect_to :controller => "bank_deposits",:action => "write_off"
    end
  end

 def write_off_batch
    @batches = Batch.find_all_by_yuva_vertical_id(params[:type_of_vertical])
    render :update do |page|
        page.replace_html 'write_off_batch', :partial => 'bank_deposits/write_off_batch' 
    end
  end

  def students_list
    @students = []
    @write_off_bdft =  WriteoffDeposit.find_all_by_reject(false).map{|p| p.student_id}.uniq
    students = Student.find_all_by_batch_id(params[:batch_ids].split(",")).map{|a| a.id}
    student_ids = students.map(&:to_i).reject{ |e| @write_off_bdft.include? e }
    former_students = ArchivedStudent.find_all_by_batch_id(params[:batch_ids].split(",")).map{|a| a.former_id}
    former_student_ids = former_students.map(&:to_i).reject{ |e| @write_off_bdft.include? e } unless former_students.blank?
    unless former_student_ids.blank?
      former_student_ids.map{|p| @students<<ArchivedStudent.find_by_former_id(p)}
    end
    student_ids.map{|p| @students<<Student.find_by_id(p)}
    # @students = Student.find student_ids
    render :update do |page|
        page.replace_html 'students_list', :partial => 'bank_deposits/students_list' 
    end
  end
end
