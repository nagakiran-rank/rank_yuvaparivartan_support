module RankYuvaparivartanSupport
  module RankStudentController

    def self.included (base)
      base.instance_eval do
        alias_method_chain :change_to_former, :tmpl
        alias_method_chain :profile, :tmpl
        alias_method_chain :add_additional_details, :tmpl
        alias_method_chain :edit_additional_details, :tmpl        
        alias_method_chain :admission4, :tmpl
        alias_method_chain :fees, :tmpl
        alias_method_chain :admission1, :tmpl
        alias_method_chain :admission2, :tmpl
        alias_method_chain :edit, :tmpl
        alias_method_chain :add_guardian, :tmpl
        alias_method_chain :previous_data_from_profile, :tmpl
        alias_method_chain :edit_guardian, :tmpl
        alias_method_chain :electives, :tmpl
        alias_method_chain :guardians, :tmpl
        alias_method_chain :show_previous_details, :tmpl
        alias_method_chain :search_ajax, :tmpl
        alias_method_chain :list_students_by_course, :tmpl
        # alias_method_chain :remove, :remove_student
        alias_method :remove, :remove_student
        alias_method :index, :index_student
      end
    end

    def change_to_former_with_tmpl
      @dependency = @student.former_dependency
      if request.post?
        unless params[:remove][:status_description].empty?
        @status = false
          @student.archive_student(params[:remove][:status_description],params[:leaving_date])
          render :update do |page|
            page.replace_html 'remove-student', :partial => 'student_tc_generate'
            page.replace_html 'tc-generate', :partial => 'rank_student/reason_flash'
          end
        else
          @status = true
         render :update do |page|
          flash[:reason] = " #{t('add_reason')} "
          page.replace_html 'tc-generate', :partial => 'rank_student/reason_flash'
        end
        end
      else
      render :template => 'rank_student/change_to_former_with_tmpl'
      end
    end

    def profile_with_tmpl
      @student = Student.find(params[:id])
      @current_user = current_user
      @address = @student.address_line1.to_s + ' ' + @student.address_line2.to_s
      @sms_module = Configuration.available_modules
      @biometric_id = BiometricInformation.find_by_user_id(@student.user_id).try(:biometric_id)
      @sms_setting = SmsSetting.new
      @previous_data = @student.student_previous_data
      @immediate_contact = @student.immediate_contact
      @assigned_employees = @student.batch.employees
      @additional_details = @student.student_additional_details.find(:all,:include => [:student_additional_field],:conditions => ["student_additional_fields.status = true"],:order => "student_additional_fields.priority ASC")
      @additional_fields_count = StudentAdditionalField.count(:conditions => "status = true")
      @student_docs = StudentDocument.find_all_by_student_id(@student.id)
      render :template => 'rank_student/profile_with_tmpl'
    end

    def upload_document
      begin
        @document = StudentDocument.new(params[:student_document])
        if  @document.save
          flash[:notice] = "File Uploaded Successfully"
          redirect_to :action => :profile, :id => @document.student_id
        else
          flash[:notice] = "Document Size should be less than 2 MB and Pdf, Jpeg and Doc Types"
          redirect_to :action => :profile, :id => @document.student_id
        end
      rescue Exception => e
        Rails.logger.info "Exception from rank_yuvaparivarthan_support pluggin in rank_student_controller, upload_document action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong while Uploading file check the conditions Or Please inform administration"
        redirect_to :action => :profile, :id => @document.student_id
      end
    end

    def attachment_destroy
      begin
        @document = StudentDocument.find(params[:id])
        if @document.delete
          redirect_to :action => :profile, :id => @document.student_id
        end
      rescue Exception => e
        Rails.logger.info "Exception from rank_yuvaparivarthan_support pluggin in rank_student_controller, attachment_destroy action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong while Deleting a file. Please inform administration"
        redirect_to :action => :profile, :id => @document.student_id
      end
    end

    def download_attachment
      #download the  attached file
      begin
        @document = StudentDocument.find params[:id]
        unless @document.nil?
          send_file  @document.document.path , :type=>@document.document_content_type
        end
      rescue Exception => e
        Rails.logger.info "Exception from rank_yuvaparivarthan_support pluggin in rank_student_controller, download_attachment action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong while downloading a file. Please inform administration"
        redirect_to :action => :profile, :id => @document.student_id
      end
    end

    def add_additional_details_with_tmpl
     @all_details = StudentAdditionalField.find(:all, :order=>"priority ASC")
     @additional_details = StudentAdditionalField.find(:all, :conditions=>{:status=>true},:order=>"priority ASC")
     @inactive_additional_details = StudentAdditionalField.find(:all, :conditions=>{:status=>false},:order=>"priority ASC")
     @additional_field = StudentAdditionalField.new    
     @student_additional_field_option = @additional_field.student_additional_field_options.build
     if request.post?
      priority = 1
      unless @all_details.empty?
        last_priority = @all_details.map{|r| r.priority}.compact.sort.last
        priority = last_priority + 1
      end
      @additional_field = StudentAdditionalField.new(params[:student_additional_field])
      @additional_field.priority = priority
      if @additional_field.save
        flash[:notice] = "#{t('flash1')}"
        redirect_to :controller => "student", :action => "add_additional_details"
      end
     else
      render :template => "rank_student/add_additional_details_with_tmpl"
     end
    end

    def edit_additional_details_with_tmpl
      @additional_details = StudentAdditionalField.find(:all, :conditions=>{:status=>true},:order=>"priority ASC")
      @inactive_additional_details = StudentAdditionalField.find(:all, :conditions=>{:status=>false},:order=>"priority ASC")
      @additional_field = StudentAdditionalField.find(params[:id])
      @student_additional_field_option = @additional_field.student_additional_field_options
      if request.get?
        render "rank_student/add_additional_details_with_tmpl"
      else
        if @additional_field.update_attributes(params[:student_additional_field])
          flash[:notice] = "#{t('flash2')}"
          redirect_to (:controller => "student", :action => "add_additional_details")
        else
          render "rank_student/add_additional_details_with_tmpl"
        end
      end
    end

    def admission4_with_tmpl
      @student = Student.find(params[:id])
      @student_additional_details = StudentAdditionalDetail.find_all_by_student_id(@student.id)
      @additional_fields = StudentAdditionalField.find(:all, :conditions=> "status = true", :order=>"priority ASC")
      if @additional_fields.empty?
        flash[:notice] = "#{t('flash9')} #{@student.first_name} #{@student.last_name}. #{t('new_admission_link')} <a href='/student/admission1'>Click Here</a>"
        redirect_to(:controller => "student", :action => "profile", :id => @student.id ) and return
      end
      if request.post?
        @error=false
        mandatory_fields = StudentAdditionalField.find(:all, :conditions=>{:is_mandatory=>true, :status=>true})
        mandatory_fields.each do|m|
          unless params[:student_additional_details][m.id.to_s.to_sym].present?
            @student.errors.add_to_base("#{m.name} must contain atleast one selected option.")
            @error=true
          else
            if params[:student_additional_details][m.id.to_s.to_sym][:additional_info]==""
              @student.errors.add_to_base("#{m.name} cannot be blank.")
              @error=true
            end
          end
        end

        unless @error==true
          additional_field_ids_posted = []
          additional_field_ids = @additional_fields.map(&:id)
          if params[:student_additional_details].present?
            params[:student_additional_details].each_pair do |k, v|
              addl_info = v['additional_info']
              additional_field_ids_posted << k.to_i
              addl_field = StudentAdditionalField.find_by_id(k)
              if addl_field.input_type == "has_many"
                addl_info = addl_info.join(", ")
              end
              prev_record = StudentAdditionalDetail.find_by_student_id_and_additional_field_id(params[:id], k)
              unless prev_record.nil?
                unless addl_info.present?
                  prev_record.destroy
                else
                  prev_record.update_attributes(:additional_info => addl_info)
                end
              else
                addl_detail = StudentAdditionalDetail.new(:student_id => params[:id],
                  :additional_field_id => k,:additional_info => addl_info)
                addl_detail.save if addl_detail.valid?
              end
            end
          end
          if additional_field_ids.present?
            StudentAdditionalDetail.find_all_by_student_id_and_additional_field_id(params[:id],(additional_field_ids - additional_field_ids_posted)).each do |additional_info|
              additional_info.destroy unless additional_info.student_additional_field.is_mandatory == true
            end
          end
          flash[:notice] = "#{t('flash9')} #{@student.first_name} #{@student.last_name}. #{t('new_admission_link')} <a href='/student/admission1'>Click Here</a>"
          redirect_to :controller => "student", :action => "profile", :id => @student.id
        else
          render :template => "rank_student/admission4_with_tmpl"
        end
      else
        render :template => "rank_student/admission4_with_tmpl"
      end
    end
    def fees_with_tmpl
        @dates=FinanceFeeCollection.find(:all,:joins=>"INNER JOIN fee_collection_batches on fee_collection_batches.finance_fee_collection_id=finance_fee_collections.id INNER JOIN finance_fees on finance_fees.fee_collection_id=finance_fee_collections.id inner join collection_particulars on collection_particulars.finance_fee_collection_id=finance_fee_collections.id inner join finance_fee_particulars on finance_fee_particulars.id=collection_particulars.finance_fee_particular_id and ((finance_fee_particulars.receiver_type='Student' and finance_fee_particulars.receiver_id=finance_fees.student_id) or (finance_fee_particulars.receiver_type='StudentCategory' and finance_fee_particulars.receiver_id=finance_fees.student_category_id) or (finance_fee_particulars.receiver_type='Batch' and finance_fee_particulars.receiver_id=finance_fees.batch_id))",:conditions=>"finance_fees.student_id='#{@student.id}'  and finance_fee_collections.is_deleted=#{false} and ((finance_fees.balance > 0 and finance_fees.batch_id<>#{@student.batch_id}) or (finance_fees.batch_id=#{@student.batch_id}) )").uniq
          if request.post?
            @student.update_attribute(:has_paid_fees,params[:fee][:has_paid_fees]) unless params[:fee].nil?
            flash[:notice] = "#{t('status_updated')}"
          end
        @batches=@student.all_batches.reverse
        # @dates=@student.fees_list
        @dates=[]
        #  if @student.has_paid_fees
        #  flash[:notice]=t('do_not_create_fee_collections_from_now_on')
        # elsif @student.has_paid_fees_for_batch
        #   flash[:notice]=t('do_not_create_fee_collections_for_this_student_in_the_current_batch')
        # else
        #   flash[:notice]=nil
        # end
        render "rank_student/fees_with_tmpl"
    end
    def admission1_with_tmpl
    @student = Student.new(params[:student])
    @selected_value = Configuration.default_country 
    @application_sms_enabled = SmsSetting.find_by_settings_key("ApplicationEnabled")
    @last_admitted_student = User.last(:select=>"username",:conditions=>["student=?",true])
    @next_admission_no=User.next_admission_no("student")
    @config = Configuration.find_by_config_key('AdmissionNumberAutoIncrement')
    @categories = StudentCategory.active
    if request.post?
      # @batch = Batch.find(@student.batch_id)
      # @student_names = @batch.students.map{|p| [p.full_name]}
      # @a = true
      # if @student.full_name.present? && @student.batch_id.present? && @student.phone2.present?
      #   @batch.students.each do |p|
      #     if (p.full_name == @student.full_name) && (p.phone2 == @student.phone2)
      #       @student.errors.add_to_base("Student already present with same name and mobile number in the selected batch")
      #       @error = true
      #       @a = false
      #       break
      #     end
      #   end
      # end
      # if @a == true
        if @config.config_value.to_i == 1
          @exist = Student.first(:conditions => ["admission_no LIKE BINARY(?)",params[:student][:admission_no]])
          if @exist.nil?
            @status = @student.save
          else
            @status = @student.save
          end
        else
          @status = @student.save
        end
      # end
    
      if @status
        if @student.batch.course.enable_student_elective_selection
          unless @student.batch.elective_groups.active.empty? #&& !@student.batch.subjects.select{|es| es.elective_group_id != nil}.blank?
            @student.batch.elective_groups.active.each do |eg|
              if !eg.end_date.nil? && !eg.subjects.active.empty? && eg.end_date >= Date.today
                end_date = eg.end_date
                recipients_array = [@student.user.id]
                sender = current_user.id
                Reminder.send_message(recipients_array,sender,end_date,eg.name)
              end
            end
          end
        end
      end
      if @status
        sms_setting = SmsSetting.new()
        if sms_setting.application_sms_active and @student.is_sms_enabled
          recipients = []
          message = "#{t('student_admission_done_for')} #{@student.first_and_last_name}. #{t('username_is')} #{@student.admission_no}, #{t('guardian_password_is')} #{@student.admission_no}123. #{t('thanks')}"
          if sms_setting.student_admission_sms_active
            recipients.push @student.phone2 unless @student.phone2.blank?
          end
          unless recipients.empty?
            Delayed::Job.enqueue(SmsManager.new(message,recipients))
          end
        end
        if Configuration.find_by_config_key('EnableSibling').present? and Configuration.find_by_config_key('EnableSibling').config_value=="1"
          flash[:notice] = "#{t('flash22')}"
          return redirect_to :controller => "student", :action => "admission1_2", :id => @student.id
        else
          flash[:notice] = "#{t('flash8')}"
          return redirect_to :controller => "student", :action => "admission2", :id => @student.id
        end
      end
    end
    render :template => "rank_student/admission1_with_tmpl"
  end

  def admission2_with_tmpl 
    @student = Student.find params[:id]
    @guardian = Guardian.new(params[:guardian])
    if request.post? and @guardian.save
      return redirect_to :controller => "student", :action => "admission2", :id => @student.id
    end
    render :template => "rank_student/admission2_with_tmpl"
  end

  def edit_with_tmpl
    @student = Student.find(params[:id])
    if @student.student_lock == true 
      flash[:notice] = "#{t('flash_msg')}"
      redirect_to :controller => "user",:action => "dashboard"
    else
      @state = YuvaState.find_by_name(@student.state)
      @city = YuvaCity.find_by_name(@student.city)
      @student.gender=@student.gender.downcase
      @student_user = @student.user
      @student_categories = StudentCategory.active
      unless @student.student_category.present? and @student_categories.collect(&:name).include?(@student.student_category.name)
        current_student_category=@student.student_category
        @student_categories << current_student_category if current_student_category.present?
      end 
      @batches = Batch.active
      @student.biometric_id = BiometricInformation.find_by_user_id(@student.user_id).try(:biometric_id)
      @application_sms_enabled = SmsSetting.find_by_settings_key("ApplicationEnabled")

      if request.put?
        unless params[:student][:image_file].blank?
          unless params[:student][:image_file].size.to_f > 280000
            if @student.update_attributes(params[:student])
              flash[:notice] = "#{t('flash3')}"
              redirect_to :controller => "student", :action => "profile", :id => @student.id
            end
          else
            flash[:notice] = "#{t('flash_msg11')}"
            redirect_to :controller => "student", :action => "edit", :id => @student.id
          end
        else
          if @student.update_attributes(params[:student])
            flash[:notice] = "#{t('flash3')}"
            redirect_to :controller => "student", :action => "profile", :id => @student.id
          end
        end
      else
      render :template => "rank_student/edit_with_tmpl"
      end
    end
  end

  def add_guardian_with_tmpl
    @parent_info = @student.guardians.build(params[:parent_info])
    @countries = Country.all
    if request.post? and @parent_info.save
      #       @parent_info.update_attribute(:ward_id,@student.guardians.first.ward_id) if @student.guardians.present?
      flash[:notice] = "#{t('flash5')} #{@student.full_name}"
      redirect_to :controller => "student" , :action => "admission3_1", :id => @student.id
    end
    render :template => "rank_student/add_guardian_with_tmpl"
  end

  def previous_data_from_profile_with_tmpl
    @student = Student.find(params[:id])
    @previous_data = StudentPreviousData.new(params[:student_previous_details])
    @previous_subject = StudentPreviousSubjectMark.find_all_by_student_id(@student)
    if request.post?
      @previous_data.save
      redirect_to :action => "profile", :id => @student.id
    else
      return
    end
    render :template => "rank_student/previous_data_from_profile_with_tmpl"
  end

  def edit_guardian_with_tmpl
    @parent = Guardian.find(params[:id])
    params[:student_id].present? ? @student = Student.find(params[:student_id]): @student = Student.find(params[:parent_detail][:ward_id])
    @countries = Country.all
    params[:parent_detail].delete "ward_id" if  params[:parent_detail]
    if request.post? and @parent.update_attributes(params[:parent_detail])    
      #      if @parent.email.blank?
      #        @parent.email= "noreplyp#{@parent.ward.admission_no}@fedena.com"
      #        @parent.save
      #      end
      if @parent.id  == @student.immediate_contact_id
        unless @parent.user.nil?
          User.update(@parent.user.id, :first_name=> @parent.first_name, :last_name=> @parent.last_name, :email=> @parent.email, :role =>"Parent")
        else
          @parent.create_guardian_user(@student)
        end
      end
      flash[:notice] = "#{t('student.flash4')}"
      redirect_to :controller => "student", :action => "guardians", :id => @student.id
    end
     render :template => "rank_student/edit_guardian_with_tmpl"
  end

  def electives_with_tmpl
    @batch = Batch.find(params[:id])
    @elective_subject = Subject.active.find(params[:id2])
    @students = @batch.students.by_first_name(:include => {:exam_scores => :exam})
    @elective_group = ElectiveGroup.find(@elective_subject.elective_group_id)
    flash[:notice] = nil
    if @elective_subject.nil?
      redirect_to :controller => "user", :action => "dashboard"
    end
    render :template => "rank_student/electives_with_tmpl"
  end

  def guardians_with_tmpl
    @parents = @student.guardians
    render :template => "rank_student/guardians_with_tmpl"
  end

  def show_previous_details_with_tmpl
    @previous_data = StudentPreviousData.find_by_student_id(@student.id)
    @previous_subjects = StudentPreviousSubjectMark.find_all_by_student_id(@student.id)
    render :template => "rank_student/show_previous_details_with_tmpl"
  end
  def remove_student
    @student = Student.find(params[:id])
    render :template => "rank_student/remove_student"
  end
  def find_cities
    @yuva_state_id = YuvaState.find_by_name(params[:student_state]).id
    @yuva_cities = YuvaCity.find_all_by_state_id(@yuva_state_id)
    render :update do |page|
        page.replace_html 'student_cities', :partial => 'rank_student/find_cities'
    end
  end
  def edit_states
    @yuva_country = Country.find(params[:country_id])
    @yuva_states = YuvaState.find_all_by_country_id(@yuva_country.id)
    render :update do |page|
        page.replace_html 'student_states', :partial => 'rank_student/edit_states'
    end
  end
  def index_student
    # @student = Student.find(params[:id])
    render :template => "rank_student/index_student"
  end

  def search_ajax_with_tmpl
    if params[:option] == "active" or params[:option]=="sibling"
      if params[:query].length>= 3
        @students = Student.find(:all,
          :conditions => ["first_name LIKE ? OR middle_name LIKE ? OR last_name LIKE ?
                            OR admission_no = ? OR (concat(first_name, \" \", last_name) LIKE ? ) ",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}" ],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      else
        @students = Student.find(:all,
          :conditions => ["admission_no = ? " , params[:query]],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      end   
      @students.reject!{|r| r.immediate_contact_id.nil?} if @students.present? and params[:option]=="sibling"
      render :partial => "rank_student/search_ajax_student_with_tmpl" , :layout => false
    else
      if params[:query].length>= 3
        @archived_students = ArchivedStudent.find(:all,
          :conditions => ["first_name LIKE ? OR middle_name LIKE ? OR last_name LIKE ?
                            OR admission_no = ? OR (concat(first_name, \" \", last_name) LIKE ? ) ",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}" ],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      else
        @archived_students = ArchivedStudent.find(:all,
          :conditions => ["admission_no = ? " , params[:query]],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      end
      render :partial => "rank_student/search_ajax_with_tmpl"
    end
  end

  def list_students_by_course_with_tmpl
    @students = Student.find_all_by_batch_id(params[:batch_id], :order => 'first_name ASC')
    render(:update) { |page| page.replace_html 'students', :partial => 'rank_student/students_by_course' }
  end

  def send_notifiation
    @student = Student.find(params[:id])
    Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
      :recipient_ids => User.find_by_username("admin").id ,
      :subject=>"Please Unlock the student #{@student.admission_no}",
      :body=>"Please Unlock the student with #{@student.admission_no} in the batch #{@student.batch.full_name} having courses #{@student.batch.course.course_name}.
      " ))
    redirect_to :controller => 'batches',:action => 'show_locked_students',:id => @student.batch.id, :course_id => @student.batch.course.id
  end

  def unlock_student
    @student = Student.find(params[:id])
    @student.update_attribute(:student_lock,false)
    @recipient_ids = User.find_all_by_employee(true).map{|p| p.id}
    Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => User.find_by_username("admin").id,
      :recipient_ids => @recipient_ids,
      :subject=>"Student #{@student.admission_no} has been unlocked for 48 hours.",
      :body=>"The Student #{@student.admission_no} has unlocked for 48 hours. Kindly do necessary changes before he/she gets locked again." ))
    @delayed_student_lock = Delayed::Job.enqueue(AdminStudentUnlock.new(@student.id,@student.school_id) , 0, Time.now + 2.days)
    redirect_to :controller => 'batches',:action => 'locked_students',:id => @student.batch.id, :course_id => @student.batch.course.id
  end

  end
end