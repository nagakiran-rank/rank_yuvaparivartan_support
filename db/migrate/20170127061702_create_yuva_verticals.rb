class CreateYuvaVerticals < ActiveRecord::Migration
  def self.up
    create_table :yuva_verticals do |t|
    	t.string  :name
      t.timestamps
    end
  end

  def self.down
    drop_table :yuva_verticals
  end
end