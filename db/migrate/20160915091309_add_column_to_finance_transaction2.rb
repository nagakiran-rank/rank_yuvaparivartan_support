class AddColumnToFinanceTransaction2 < ActiveRecord::Migration
  def self.up
  	add_column :finance_transactions, :student_fee_receipt, :integer, :default => 0
  	add_column :finance_transactions, :pay_all_fee, :integer, :default => 0
  	add_column :finance_transactions, :particular_wise_fee, :integer, :default => 0
  end

  def self.down
  	remove_column :finance_transactions, :student_fee_receipt
  	remove_column :finance_transactions, :pay_all_fee
  	remove_column :finance_transactions, :particular_wise_fee
  end
end
