module RankYuvaparivartanSupport
  module RankMultiFeesTransactions

    def self.included (base)
      base.instance_eval do
        alias_method_chain :delete_finance_transactions, :tmpl
      end
    end
    def delete_finance_transactions_with_tmpl
	  	bdft=BankDepositFinanceTransaction.find_by_finance_transaction_id(finance_transactions.id) 
      unless bdft.nil?
      else
	    	finance_transactions.destroy_all
	  	end
	  end
  end
end