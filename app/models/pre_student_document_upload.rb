class PreStudentDocumentUpload < ActiveRecord::Base
	# belongs_to :pre_student_registration
	has_attached_file :attachment,
    :url => "/uploads/:class/:id/:attachment/:attachment_fullname?:timestamp",
    :path => "uploads/:class/:attachment/:id_partition/:style/:basename.:extension",
    :max_file_size => 5242880,
    :reject_if => proc { |attributes| attributes.present? },
    :permitted_file_types =>[]

 validates_attachment_size :attachment, :less_than => 5242880,\
    :message=>'must be less than 1 MB.',:if=> Proc.new { |p| p.attachment_file_name_changed? }
 #  validates_attachment_presence :attachment, :message => "can't be blank"
   # has_attached_file :attachment, :styles => { :small => "150x150>" },
   #                  :url  => "/uploads/:class/:id/:attachment/:attachment_fullname?:timestamp",
   #                  :path => "uploads/:class/:attachment/:id_partition/:style/:basename.:extension"

  # validates_attachment_presence :attachment, :message => "can't be blank"
  #validates_attachment_size :attachment, :less_than => 5.megabytes, :message => "should be less than 5MB."
  validates_attachment_content_type :attachment, :content_type => ['image/jpeg', 'image/png', 'image/gif', 'application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ]
  
end
