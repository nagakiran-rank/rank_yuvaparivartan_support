module RankYuvaparivartanSupport
  module RankArchivedStudentController

def self.included (base)
      base.instance_eval do
        alias_method_chain :profile, :tmpl
      end
    end

    def profile_with_tmpl
    @current_user = current_user
    @archived_student = ArchivedStudent.find(params[:id])
    @additional_fields = StudentAdditionalField.all(:all, :conditions=> "status = true", :order=>"priority ASC")
    render :template => 'rank_archived_student/profile_with_tmpl'
  end

  end
end