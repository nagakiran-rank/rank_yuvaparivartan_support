class AddColumnToFinanaceTransaction < ActiveRecord::Migration
  def self.up
  	add_column :finance_transactions, :duplicate, :integer, :default => 0
  end

  def self.down
  	remove_column :finance_transactions, :duplicate
  end
end
