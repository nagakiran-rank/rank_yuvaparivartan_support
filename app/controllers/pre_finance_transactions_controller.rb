class PreFinanceTransactionsController < ApplicationController

	def advance_fee_collection
    #if current_user.admin?
      @courses = Course.active.all(:order => "course_name")
      @batches = Batch.find(:all, :conditions => {:is_deleted => false, :is_active => true}, :joins => :course, :select => "`batches`.*,CONCAT(courses.code,'-',batches.name) as course_full_name", :order => "course_full_name")
      @inactive_batches = Batch.find(:all, :conditions => {:is_deleted => false, :is_active => false}, :joins => :course, :select => "`batches`.*,CONCAT(courses.code,'-',batches.name) as course_full_name", :order => "course_full_name")
      @dates = []
    # else
    #   flash[:notice] = "#{t('flash_msg4')}"
    #   redirect_to :controller => "user",:action => "dashboard"
    # end
	end

	def advance_fees_collection_dates
		@batch = Batch.find(params[:batch_id])
    @dates = @batch.finance_fee_collections
    render :update do |page|
      page.replace_html "fees_collection_dates", :partial => "advance_fees_collection_dates"
    end
	end

	def load_advance_fee_collection
		@pre_student_registration = PreStudentRegistration.find( :all,:conditions => ["course_id = #{params[:course_id]}"], :order => "id ASC" ).first
		@course_name = Course.find(params[:course_id]).course_name
    if params[:pre_student]
      @course_fee = PreStudentRegistration.find(params[:pre_student]).pre_std_course_fee
    else
      @course_fee = @pre_student_registration.pre_std_course_fee unless @pre_student_registration.nil?
    end
		@course = Course.find(params[:course_id])
    if params[:pre_student]
      @pre_student_registration = PreStudentRegistration.find(params[:pre_student])
      @prev_pre_student = @pre_student_registration.previous_student
      @next_pre_student = @pre_student_registration.next_student
    else
    	@prev_pre_student = PreStudentRegistration.find( :all,:conditions => ["course_id = #{params[:course_id]}"], :order => "id ASC" ).last
      unless @pre_student_registration.nil?
        @next_pre_student = @pre_student_registration.next_student
      end
    end
      @pre_finance = PreFinanceTransaction.find_all_by_pre_student_registration_id(@pre_student_registration.id)
      @amount = @pre_finance.map{|a| a.amount}
  			render :update do |page|
  	      page.replace_html "student", :partial => "advance_fees_submission"
        end
  end

  def pre_finance_update_ajax
		@pre_student_registration = PreStudentRegistration.find(params[:pre_student_registration])
    # issue solved
    @prev_pre_student = @pre_student_registration.previous_student
    @next_pre_student = @pre_student_registration.next_student
		@course_name = Course.find_by_id(@pre_student_registration.course_id).course_name
		#@course_fee = Course.find_by_id(@pre_student_registration.course_id).fee_structure
    @course_fee = @pre_student_registration.pre_std_course_fee
		@course = Course.find_by_id(@pre_student_registration.course_id)
    @pre_finance = PreFinanceTransaction.find_all_by_pre_student_registration_id(params[:pre_student_registration])
		transaction = PreFinanceTransaction.new
		transaction.amount = params[:pre_finance_transaction][:fees_paid]
    transaction.payment_note = params[:pre_finance_transaction][:payment_note]
		transaction.transaction_date = params[:transaction_date]
		transaction.payment_mode = params[:fees][:payment_mode]
		transaction.course_id = params[:course_id]
		@course_amt =  @pre_student_registration.pre_std_course_fee
		@fee_paid = params[:pre_finance_transaction][:fees_paid]
		transaction.deposited_date = params[:transaction_date]
		transaction.pre_student_registration_id = @pre_student_registration.id
    # unless @pre_finance.empty?
    #   amount1 = @pre_finance.map{|a| a.amount}
    #   total_amt = amount1.inject(0){|sum,x| sum + x }
    #   @balance = @course_amt.to_i - (total_amt + @fee_paid.to_i)
    #   transaction.balance = @fee_paid.to_i
    #   transaction.save
    #   @pre_finance = PreFinanceTransaction.find_all_by_pre_student_registration_id(params[:pre_student_registration])
    #   @amount = @pre_finance.map{|a| a.amount}
    #   render :update do |page|
    #     page.replace_html "student", :partial => "advance_fees_submission"
    #   end
    # else
  		# @balance = @course_amt.to_i - @fee_paid.to_i
      transaction.balance = @fee_paid.to_i
      transaction.save
      @pre_finance = PreFinanceTransaction.find_all_by_pre_student_registration_id(params[:pre_student_registration])
      @amount = @pre_finance.map{|a| a.amount}
      render :update do |page|
        page.replace_html "student", :partial => "advance_fees_submission"
      end  		
    #end	
  end

  def pre_student_fee_receipt_pdf
  	@pre_student_registration = PreStudentRegistration.find(params[:pre_student_registration])
  	@pre_finance = PreFinanceTransaction.find_all_by_pre_student_registration_id(@pre_student_registration.id)
  	@amount = @pre_finance.map{|a| a.amount}
  	@paid = @amount.inject(0){|sum,x| sum + x }
  	render :pdf => 'pre_finance_transactions/pre_student_fee_receipt_pdf',
             :template => "pre_finance_transactions/pre_student_fee_receipt_pdf"
  end
  
  def print_std_single_receipt_pdf
    @pre_finance_transactions = PreFinanceTransaction.find(params[:id])
    @pre_student_registration = PreStudentRegistration.find(@pre_finance_transactions.pre_student_registration_id)
    unless @pre_student_registration.student_id.nil?
    @student = Student.find(@pre_student_registration.student_id)
    @batch = @student.batch
    @finance_transaction = FinanceTransaction.find_by_payee_id(@student.id)
    end
    render :pdf => 'pre_finance_transactions/print_std_single_receipt_pdf',
    :template => "pre_finance_transactions/print_std_single_receipt_pdf"
    # @finance_transaction.update_attributes(:duplicate => 1)
  end

end
