require 'translator'
require File.join(File.dirname(__FILE__), "lib", "rank_yuvaparivartan_support")
require File.join(File.dirname(__FILE__), "config", "breadcrumbs")


FedenaPlugin.register = {
 :name=>"rank_yuvaparivartan_support",
 :description=>"Rank Yuvaparivartan Support ",
 :multischool_models=>%w{StudentDocument BatchTransferWithFeePaid BankDepositFinanceTransaction PreFinanceTransaction PreStudentRegistration PreStudentAdditionalDetail PreStudentDocumentUpload PreStudentGuardian PreStudentPreviousData WriteoffDeposit WriteoffDetail},
 :auth_file=>"config/rank_yuvaparivartan_support_auth.rb",
 :generic_hook =>[
 	{ :title=> "bank",
     :source=>{:controller=>"finance", :action=>"index" },
     :destination=>{:controller=>"yuva_parivarthan_banks",:action=>"index"},
     :description=>"manage_bank_details"},
  { :title=> "bank_deposit",
     :source=>{:controller=>"finance", :action=>"index" },
     :destination=>{:controller=>"bank_deposits",:action=>"index"},
     :description=>"manage_bank_deposits"},
  { :title=> "deposit_details",
     :source=>{:controller=>"finance", :action=>"index" },
     :destination=>{:controller=>"bank_deposits",:action=>"bank_deposit_details"},
     :description=>"manage_bank_dposits_details"},
  { :title=> "finance_details",
     :source=>{:controller=>"finance", :action=>"index" },
     :destination=>{:controller=>"pre_finance_transactions",:action=>"advance_fee_collection"},
     :description=>"manage_pre_student_fee"},
  { :title=> "writeoff_report",
     :source=>{:controller=>"report", :action=>"index" },
     :destination=>{:controller=>"writeoff_deposits",:action=>"writeoff_reports_details"},
     :description=>"generates_writeoff_report"}
   ]
}

Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
 I18n.load_path.unshift(locale)
end

RankYuvaparivartanSupport.attach_overrides

if RAILS_ENV == 'development'
 ActiveSupport::Dependencies.load_once_paths.\
 reject!{|x| x =~ /^#{Regexp.escape(File.dirname(__FILE__))}/}
end
# Dispatcher.to_prepare :rank_yuvaparivartan_support do
# 	FeeDiscount.send :include, FeeDiscountAfterDestroy
#   # FeeDiscount.send :include, RankFeeDiscountModel
#   # FeeDiscount.send :after_destroy, :fee_discount_after_destroy
# end