module RankYuvaparivartanSupport
	module RankStudentModel

		def self.included (base)
			base.instance_eval do
        # validates_uniqueness_of :first_name, :scope => [:batch_id, :phone2, :middle_name, :last_name], :message => "of student with Mobile number is already Present in the Batch " 
			end
		end

    def validate_on_create
       @batch = Batch.find(self.batch_id)
        @batch.students.each do |s|
          if ((s.full_name==self.full_name)&&(s.phone2==self.phone2))
            errors.add_to_base("Student with same Name and Mobile number is already Present in the Batch" )
            break
          end
        end
    end
    def validate_on_update     
      @students = Student.all(:conditions => ["batch_id = ? AND id != ?",self.batch_id,self.id])
        @students.each do |s|
          if ((s.full_name==self.full_name)&&(s.phone2==self.phone2))
            errors.add_to_base("Student with same Name and Mobile number is already Present in the Batch" )
            break
          end
        end
    end

		def fees_list_by_batch(batch_id)
          batch=Batch.find(batch_id)
          FinanceFeeCollection.find(:all,
            :joins=>"INNER JOIN fee_collection_batches on fee_collection_batches.finance_fee_collection_id = finance_fee_collections.id
                    INNER JOIN finance_fees on finance_fees.fee_collection_id = finance_fee_collections.id
                    LEFT JOIN finance_transactions on finance_transactions.finance_id = finance_fees.id and finance_transactions.finance_type ='FinanceFee'
                    INNER JOIN batches on  batches.id = #{batch_id}
                    INNER JOIN collection_particulars on collection_particulars.finance_fee_collection_id=finance_fee_collections.id
                    INNER JOIN finance_fee_particulars on finance_fee_particulars.id=collection_particulars.finance_fee_particular_id and ((finance_fee_particulars.receiver_type='Student' and finance_fee_particulars.receiver_id=finance_fees.student_id) or (finance_fee_particulars.receiver_type='StudentCategory' and finance_fee_particulars.receiver_id=finance_fees.student_category_id) or (finance_fee_particulars.receiver_type='Batch' and finance_fee_particulars.receiver_id=finance_fees.batch_id))",
            :conditions=>"finance_fees.student_id='#{self.id}'  and
                          finance_fee_collections.is_deleted=#{false} and
                          finance_fees.batch_id=#{batch_id}",
            :select=>"finance_fee_collections.*,
                      batches.name as batch_name,
                      finance_fees.is_paid,
                      finance_fees.balance,
                      max(finance_transactions.transaction_date) as last_transaction_date,
                      sum(DISTINCT(finance_transactions.amount)) as paid_amount",
            :group=>"finance_fees.id",
            :order=>"finance_fees.balance DESC"
          )
    end
# private
#     def is_valid_true
#       @batch = Batch.find(self.batch_id)
#       @batch.students.each do |s|
#         if ((s.full_name==self.full_name)&&(s.phone2==self.phone2))
#           errors.add_to_base("Student with same Name and Mobile number is already Present in the Batch" )
#           break
#         end
#       end
#     end
    
	end
end
  