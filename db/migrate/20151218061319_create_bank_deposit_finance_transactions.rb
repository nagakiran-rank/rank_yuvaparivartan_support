class CreateBankDepositFinanceTransactions < ActiveRecord::Migration
  def self.up
    create_table :bank_deposit_finance_transactions do |t|
      t.string  :type_of_deposit
      t.date    :date_of_collection
      t.date    :date_of_deposit
      t.integer :finance_transaction_id
      t.decimal :fee_collected
      t.integer :admin_id
      t.decimal :fee_to_be_deposited
      t.text    :notes
      t.integer :bank_deposit_yuvaparivarthan_bank_id
      t.integer :employee_id
      t.boolean :admin_status, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :bank_deposit_finance_transactions
  end
end
