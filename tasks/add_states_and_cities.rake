namespace :add_states_and_cities do
	desc "add states from sql file to table"
	task :add => :environment do
		# unless Rails.env.production?
	     connection = ActiveRecord::Base.connection
	     # connection.tables.each do |table|
	     # connection.execute("TRUNCATE #{table}") unless table == "schema_migrations"
	     # end
	    # end
	    puts "#{Rails.root.to_s.inspect}"
		states_sql = File.read(Rails.root.to_s+"/public/sql_files/states.sql")
		states_statements = states_sql.split(/;$/)
		cities_sql = File.read(Rails.root.to_s+"/public/sql_files/cities.sql")
		cities_statements = cities_sql.split(/;$/)
		states_statements.pop # remote empty line states
		cities_statements.pop # remote empty line cities
		ActiveRecord::Base.transaction do
		  states_statements.each do |statement|
		  	puts "#{statement.inspect}"
		  	puts "**************************************************************************"
		    connection.execute(statement)
		  end
		  puts "==========================completed states================================"
		  cities_statements.each do |statement|
		    connection.execute(statement)
		  end
		  puts "=============================completed cities============================="
		end
		change_76_to_last  = YuvaState.find_all_by_country_id(76)
		change_76_to_last.each do |b|
			b.update_attributes(:country_id => 196)
		end
		change_101_to_india  = YuvaState.find_all_by_country_id(101)
		change_101_to_india.each do |b|
			b.update_attributes(:country_id => 76)
		end
	end

	# desc "TODO"
	# task :my_task2 => :environment do
	# end
end