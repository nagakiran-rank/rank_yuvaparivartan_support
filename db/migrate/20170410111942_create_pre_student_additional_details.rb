class CreatePreStudentAdditionalDetails < ActiveRecord::Migration
  def self.up
    create_table :pre_student_additional_details do |t|
    	t.references :pre_student_registration
    	t.integer :additional_field_id
    	t.string :additional_info
    	t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :pre_student_additional_details
  end
end
