module RankYuvaparivartanSupport
	module RankBatchModel

		def self.included (base)
			base.instance_eval do
				validates_presence_of :yuva_vertical_id
			end
		end    
	end
end
  