 module RankYuvaparivartanSupport
  module RankElectiveGroupsController

  	def self.included (base)
      base.instance_eval do
      	alias_method_chain :new, :tmpl
      	alias_method_chain :edit, :tmpl
      	alias_method_chain :index, :tmpl
      	alias_method_chain :show, :tmpl
      end
    end

	def new_with_tmpl
	  @elective_group = @batch.elective_groups.build
	  render :template => 'rank_elective_groups/new_with_tmpl'
	end
	def edit_with_tmpl
	  @elective_group = ElectiveGroup.find(params[:id])
	  # render 'edit'
	  render :template => 'rank_elective_groups/edit_with_tmpl'
	end

	def index_with_tmpl
	  @elective_groups = ElectiveGroup.for_batch(@batch.id, :include => :subjects)
	  render :template => 'rank_elective_groups/index_with_tmpl'
	end

	def show_with_tmpl
	  @electives = Subject.find_all_by_batch_id_and_elective_group_id(@batch.id,@elective_group.id, :conditions=>["is_deleted = false"])
	  render :template => 'rank_elective_groups/show_with_tmpl'
	end

  end
end