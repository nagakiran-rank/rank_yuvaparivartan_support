module RankYuvaparivartanSupport
  module RankCoursesController

    def self.included (base)
      base.instance_eval do
        alias_method_chain :show, :tmpl
      end
    end

    def show_with_tmpl
      @batches = @course.batches.active
      render :template => 'rank_courses/show_with_tmpl'
    end

    def locked_batches
      @course = Course.find(params[:id])
      @locked_batches = Course.find(params[:id]).batches.find_all_by_end_date_lock(true)
      render :template => 'rank_courses/locked_batches'
    end

    def unlock_locked_batch
      @batch = Batch.find(params[:batch_id])
      @batch.update_attributes(:end_date_lock => false , :start_date_lock => false)
      if @batch.save(false)
        flash[:notice] = "Batch Sucessfully unlocked"
        # render :template => 'rank_courses/locked_batches'
        redirect_to :action=>"locked_batches", :id=> params[:id]
      else
        flash[:notice] = "Try again"
        # render :template => 'rank_courses/locked_batches'
        redirect_to :action=>"locked_batches", :id=> params[:id]
      end
    end

  end
end