class AdminStudentUnlock  < Struct.new(:student_id,:school_id)
	def perform
		MultiSchool.current_school = School.find(school_id)
		@student = Student.find(student_id)
		@student.update_attribute(:student_lock, true)
		puts "=-==-=-=STUDENT LOCK-=-=-=-ENDED=-=-=-=-=-=-=-"
	end
end