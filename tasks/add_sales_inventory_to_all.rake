namespace :add_sales_inventory_to_all do
  desc "Automatically populate sales inventory in the finance transaction category"
  task :install => :environment  do
  	@school = School.all
  	@school.each do |s|
	  	MultiSchool.current_school = School.find s.id
	  	FinanceTransactionCategory.find_or_create_by_name(:name=>"SalesInventory",:is_income=>true,:description=>"Sales Module for Fedena")
	  	puts "added sales inventory to #{s.name}"
	end
  end
end