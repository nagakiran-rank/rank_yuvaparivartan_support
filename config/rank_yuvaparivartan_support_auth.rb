authorization do

  role :yuvaparivartan_table_manager do
    has_permission_on [:student],
      :to => [:upload_document,:find_cities,:edit_states, :attachment_destroy, :download_attachment,:remove_student,:index_student,:send_notifiation,:unlock_student]
  end

  role :yuva_admin_only do
    has_permission_on [:yuva_parivarthan_banks],
      :to=>[:index,:destroy_bank,:edit_bank,:show,:new_bank]
    has_permission_on [:bank_deposits],
      :to => [:approve_bank_deposit_details, :reject_bank_deposit_details]
    has_permission_on [:courses],
      :to => [:locked_batches,:unlock_locked_batch]
    has_permission_on [:batches],
      :to => [:locked_students]
  end

  role :bank_module do
    has_permission_on [:bank_deposits],
       :to=>[:index, :current_calender_date, :fees_deposit_details, :yuva_bank_details, :bank_deposit_details, :edit_bank_deposit_details, :approve_bank_deposit_details, :show_bank_deposit_details, :download_attachment, :yuva_bank_details_edit,:end_date, :donation_deposit_details,:change_scanned_copy,:display_vertical,:vertical_index, :write_off, :write_off_index, :pre_student_calender, :view_student_finance_details, :write_off_batch, :students_list]
    has_permission_on [:batch_transfers],
       :to=>[:refund_details, :discount_details]
    has_permission_on [:finance],
      :to => [:edit_manual_receipt, :update_manual_receipt,:edit_manual_receipt2 ,:print_single_receipt_pdf,:print_single_receipt_pdf1]
    has_permission_on [:finance_extensions],
      :to => [:edit_manual_receipt3,:edit_manual_receipt4,:edit_manual_receipt5]
    has_permission_on [:courses],
      :to => [:locked_batches,:unlock_locked_batch]
    has_permission_on [:writeoff_deposits],
      :to => [:writeoff_index,:writeoff_reports_details,:writeoff_report_details_yuva,:approve_writeoff_deposit_details,:reject_writeoff_deposit_details,:deposits,:change_document_upload]
  end
  role :finance_yuva do
    has_permission_on [:finance],
      :to => [:edit_manual_receipt, :update_manual_receipt,:edit_manual_receipt2 ,:print_single_receipt_pdf,:print_single_receipt_pdf1]
    has_permission_on [:finance_extensions],
      :to => [:edit_manual_receipt3,:edit_manual_receipt4,:edit_manual_receipt5]
    has_permission_on [:courses],
      :to => [:locked_batches,:unlock_locked_batch]
    has_permission_on [:batches],
      :to => [:show_locked_students,:locked_students]
  end

  # role :verticals_yuva do
  #   has_permission_on [:verticals],
  #     :to => [:index]
  # end
  role :pre_student do
    has_permission_on [:pre_student_registrations],
      :to=>[:index,:pre_student_registration_form,:create_pre_student_registration,:pre_student_applicant,:pre_student_allot,:view_pre_student,:pre_student_edit,:update_pre_student_registration,:show_form_pre_student_registration,:pre_student_sort,:manage_courses,:find_cities]
  end

  role :advance_fee_collection do
    has_permission_on [:pre_finance_transactions],
      :to=>[:advance_fee_collection,:advance_fees_collection_dates,:load_advance_fee_collection,:pre_finance_update_ajax,:pre_student_fee_receipt_pdf,:print_std_single_receipt_pdf]
  end

  role :admin do
    includes :yuvaparivartan_table_manager
    includes :yuva_admin_only
    includes :bank_module
    includes :pre_student
    includes :advance_fee_collection
    # includes :verticals_yuva
  end

  role :employee do
    includes :yuvaparivartan_table_manager
    includes :finance_yuva
  end


  # role :parent do
  #   includes :yuvaparivartan_table_manager
  # end
  
  # role :student do
  #   includes :yuvaparivartan_table_manager
  # end

end