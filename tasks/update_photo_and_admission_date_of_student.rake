namespace :update_photo_and_admission_date_of_student do
  desc "update photo_url and admission_date for students through pre_student_registration"
  task :install => :environment  do
  	@school = School.all
  	@school.each do |s|
	  	MultiSchool.current_school = School.find s.id
  		@pre_students = PreStudentRegistration.find_all_by_transferred_and_school_id(true,s.id)
  		@pre_students.each do |pre_stu|  		
		  	@student = Student.find_by_id_and_school_id(pre_stu.student_id,s.id)
	  		@student.update_attributes(:photo=>pre_stu.photo, :admission_date => pre_stu.admission_date)
			end
		end
		puts"---task updated successfully----"
  end  
end