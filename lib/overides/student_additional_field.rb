module RankYuvaparivartanSupport
	module StudentAdditionalFieldAux

		def self.included (base)
			base.instance_eval do
				alias_method_chain :options_check, :tmpl
			end
		end

		def options_check_with_tmpl
			unless self.input_type=="text" or self.input_type=="text_area" or self.input_type=="date" or self.input_type.nil?
				all_valid_options=self.student_additional_field_options.reject{|o| (o._destroy==true if o._destroy)}
				unless all_valid_options.present?
					errors.add_to_base(:create_atleast_one_option)
				end
				if all_valid_options.map{|o| o.field_option.strip.blank?}.include?(true)
					errors.add_to_base(:option_name_cant_be_blank)
				end
			end
		end

	end
end
