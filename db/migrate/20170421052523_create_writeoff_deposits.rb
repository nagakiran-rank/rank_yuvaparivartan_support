class CreateWriteoffDeposits < ActiveRecord::Migration
  def self.up
    create_table :writeoff_deposits do |t|
    	t.integer :bank_deposit_finance_transaction_id
      t.references :writeoff_details
      t.boolean :approve 
      t.timestamps
    end
  end

  def self.down
    drop_table :writeoff_deposits
  end
end
