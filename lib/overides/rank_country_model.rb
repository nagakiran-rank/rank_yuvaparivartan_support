module RankYuvaparivartanSupport
	module RankCountryModel
		def self.included (base)
			base.instance_eval do
				has_many :yuva_states, :foreign_key => 'country_id'
			end
		end    
	end
end
  