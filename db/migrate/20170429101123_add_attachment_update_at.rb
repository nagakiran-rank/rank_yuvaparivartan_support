class AddAttachmentUpdateAt < ActiveRecord::Migration
  def self.up
  	add_column :pre_student_document_uploads, :attachment_updated_at, :datetime
  end

  def self.down
  end
end
