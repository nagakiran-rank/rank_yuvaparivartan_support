class CreateYuvaparivarthanBanks < ActiveRecord::Migration
  def self.up
    create_table :yuvaparivarthan_banks do |t|
    	t.string :bank_name
      t.string :account_number
      t.string :branch_name
      t.string :branch_code
      t.string :state_name
      t.string :district_name
      t.string :ifsc_code
      t.string :micr_code

      t.timestamps
    end
  end

  def self.down
    drop_table :yuvaparivarthan_banks
  end
end
