class WriteoffDetail < ActiveRecord::Base
	has_many :writeoff_deposits , :foreign_key => 'writeoff_details_id'
	has_attached_file :document_upload,
	:url => "/system/:class/:attachment/:id/:style/:basename.:extension",
	:path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
	validates_attachment_presence :document_upload

	# VALID_TYPES = ['application/pdf', 'image/jpeg', 'image/jpg', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ]
	
	validates_attachment_content_type :document_upload, :content_type => ['image/jpeg','image/jpg', 'image/png']
	# :message=>'Image can only be pdf, jpeg, mp4',:if=> Proc.new { |d| !d.upload_document_file_name.blank? }
	
	validates_attachment_size :document_upload, :less_than => 2097152
end
