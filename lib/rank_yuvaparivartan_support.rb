require 'dispatcher'
require 'overides/rank_student_controller'
require 'overides/student_additional_field'
require 'overides/rank_archived_student_controller'
require 'overides/rank_student'
require 'overides/rank_batch_transfers_controller'
require 'overides/rank_hostel_fee_controller'
require 'overides/rank_transport_fee_controller'
require 'overides/rank_finance_transaction'
require 'overides/rank_finance_controller'
require 'overides/rank_finance_extensions_controller'
# require 'overides/rank_multi_fees_transaction'
# require 'overides/rank_fee_discount'
require 'overides/rank_finance_donation'
require 'overides/rank_batches_controller'
require 'overides/rank_instant_fees_controller'
require 'overides/rank_subjects_controller'
require 'overides/rank_elective_groups_controller'
require 'overides/rank_batch'
require 'overides/rank_attendances'
require 'overides/rank_applicants_admins_controller'
require 'overides/rank_courses_controller'
require 'overides/rank_country_model'
require 'overides/rank_master_courses_controller'
require 'overides/rank_master_course'
module RankYuvaparivartanSupport
  def self.attach_overrides
    Dispatcher.to_prepare  do
     ::Hostel.instance_eval { include FedenaHostel }
     ::Transport.instance_eval { include FedenaTransport }
     ::Student.instance_eval { include RankStudentModel }
     ::BatchesController.instance_eval { include RankBatchesController }
     ::StudentController.instance_eval { include RankStudentController }
     ::StudentAdditionalField.instance_eval { include StudentAdditionalFieldAux }
     ::ArchivedStudentController.instance_eval { include RankArchivedStudentController }
     ::Student.instance_eval {include StudentExtension}
     ::BatchTransfersController.instance_eval { include RankBatchTransfersController }
     ::Student.send :validates_length_of, :phone2, :is => 10, :message => 'must be equal to 10 digits'
     ::Student.send :validates_numericality_of, :phone2, :message => 'must be a Numeric'
     ::HostelFeeController.instance_eval { include RankHostelFeeController}
     ::TransportFeeController.instance_eval { include RankTransportFeeController}
     ::FinanceTransaction.instance_eval { include RankFinanceTransaction}
     ::FinanceController.instance_eval { include RankFinanceController }
     ::FinanceExtensionsController.instance_eval { include RankFinanceExtensionsController }
     # ::MultiFeesTransaction.instance_eval { include RankMultiFeesTransactions}
     # ::FeeDiscount.instance_eval { include RankFeeDiscountModel}
     ::FinanceDonation.instance_eval { include RankFinanceDonation}
     ::InstantFeesController.instance_eval { include RankInstantFeesController }
     ::SubjectsController.instance_eval { include RankSubjectsController }
     ::ElectiveGroupsController.instance_eval { include RankElectiveGroupsController }
     # ::Batch.send :include , StartDateBatchLock
     ::Batch.instance_eval { include RankBatchModel }
     ::AttendancesController.instance_eval { include RankAttendancesController }
     ::ApplicantsAdminsController.instance_eval { include RankApplicantsAdminsController }
     ::CoursesController.instance_eval { include RankCoursesController }
     # ::Country.send :has_many, :states
     ::Country.instance_eval { include RankCountryModel }
     ::MasterCoursesController.instance_eval { include RankMasterCoursesController}
     ::MasterCourse.instance_eval { include RankMasterCourse}
    end
  end
end
module FedenaHostel
    def self.included (base)
            base.instance_eval do
        end
    end
    def self.student_profile_fees_by_batch_hook
        
        "rank_hostel/student_profile_fees"
    end
  
end
module FedenaTransport
    def self.included (base)
            base.instance_eval do
        end
    end
    def self.student_profile_fees_by_batch_hook
        
        "rank_transport_fee/student_profile_fees"
    end
end

module StudentExtension
    def self.included (base)
            base.instance_eval do
        end
    end
   
    def hostel_fees_by_batch(batch_id)
          
          HostelFee.find_all_by_student_id(self.id).select{|p| p.hostel_fee_collection.batch_id == batch_id}
    end
    def transport_fees_by_batch(batch_id)
      
       TransportFee.find_all_by_receiver_id(self.id).select {|p| p.transport_fee_collection.batch_id == batch_id}
       
    end
   
end