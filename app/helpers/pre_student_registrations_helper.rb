module PreStudentRegistrationsHelper
	def link_to_add_document_uploads(name, f, association,addl_options={})
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, h("add_document_uploads(this, \"#{association}\", \"#{escape_javascript(fields)}\")"),{:class=>"add_button_img"}.merge(addl_options))
  end
  def attr_pair(label,value)
    content_tag(:div,:class => :attr_pair) do
      content_tag(:div,label,:class => :attr_label) + content_tag(:div,value,:class => :attr_value)
    end
  end
  def link_to_remove_fields_pre(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields_pre(this)", {:class=>"delete_button_img"})
  end
end
