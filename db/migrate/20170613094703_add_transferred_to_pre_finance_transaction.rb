class AddTransferredToPreFinanceTransaction < ActiveRecord::Migration
  def self.up
  	add_column :pre_finance_transactions, :transferred, :boolean, :default => false
  end

  def self.down
  end
end
