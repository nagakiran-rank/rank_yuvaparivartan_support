namespace :rank_yuvaparivartan_support do
  desc "Install Rank Yuvaparivarivartan Support"
  task :install do
    system "rsync --exclude=.svn -ruv vendor/plugins/rank_yuvaparivartan_support/public ."
  end

  desc "Migrate all migrations in rank_yuvaparivartan_support plugin"
	task :migrate => :environment do
    	ActiveRecord::Migrator.migrate("vendor/plugins/rank_yuvaparivartan_support/db/migrate/", ENV["VERSION"] ? ENV["VERSION"].to_i : nil)
	end
end