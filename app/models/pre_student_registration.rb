class PreStudentRegistration < ActiveRecord::Base
	has_one :pre_student_guardian,:autosave=>true
  # has_one :pre_student_document_upload,:autosave=>true
	has_one :pre_student_previous_data,:autosave=>true
	has_one :pre_finance_transaction
	has_many :pre_student_additional_details, :dependent => :destroy

  has_many :pre_student_document_uploads
  accepts_nested_attributes_for :pre_student_document_uploads, :allow_destroy => true

  belongs_to :student
  belongs_to :nationality,:class_name=>"Country"
  belongs_to :country
  validates_presence_of :first_name
  validates_presence_of :date_of_birth
  validates_presence_of :gender
  validates_presence_of :last_name
  validates_presence_of :religion
  validates_presence_of :pre_student_category_id
  validates_presence_of :address_line1,:city,:state,:pin_code
  #validates_presence_of :phone1, :message => "Phone No. can't be empty",numericality: { only_integer: true }
  validates_presence_of :phone2, :message => " can't be empty"
  validates_length_of :phone2, :is => 10, :message => 'must be equal to 10 digits'
  validates_numericality_of :phone2, :message => 'must be a Numeric'
  #validates_attachment_presence :pre_student_document_upload, :message => "can't be blank"
  VALID_IMAGE_TYPES = ['image/gif', 'image/png','image/jpeg', 'image/jpg']
  has_attached_file :photo,
    :styles => {:original=> "125x125#"},
    :url => "/uploads/:class/:id/:attachment/:attachment_fullname?:timestamp",
    :path => "uploads/:class/:attachment/:id_partition/:style/:basename.:extension",
    :default_url => "master_student/profile/default_student.png",
    :reject_if => proc { |attributes| attributes.present? },
    :max_file_size => 512000,
    :permitted_file_types =>VALID_IMAGE_TYPES
  
  validates_attachment_content_type :photo, :content_type =>VALID_IMAGE_TYPES,
    :message=>'Image can only be GIF, PNG, JPG',:if=> Proc.new { |p| !p.photo_file_name.blank? }
  validates_attachment_size :photo, :less_than => 512000,\
    :message=>'must be less than 500 KB.',:if=> Proc.new { |p| p.photo_file_name_changed? }

	def previous_student
    prev_st = PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id DESC" )[ PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id DESC" ).index( PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id} "], :order => "id DESC" ).find_by_id(self.id)).next]

     if prev_st.nil?
      # prev_st = PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id ASC" )[ PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id ASC" ).index(PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id} "], :order => "id ASC" ).find_by_id(self.id)).next]
      prev_st = PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id ASC" ).last
    end
    return prev_st
  end

	def next_student
    next_st = PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id ASC" )[PreStudentRegistration.order(:id).find_all_by_course_id(self.course_id).index(PreStudentRegistration.order(:id).find_all_by_course_id(self.course_id).find_by_id(self.id)).next]

    # PreStudentRegistration.order(:id).find( :all,:conditions => ["course_id = 5"], :order => "id DESC" )
    if next_st.nil?
    	#next_st = PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id DESC" )[ PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id DESC" ).index( PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id} "], :order => "id DESC" ).find_by_id(self.id)).next]
      next_st = PreStudentRegistration.find( :all,:conditions => ["course_id = #{self.course_id}"], :order => "id DESC" ).last
    end
    return next_st
  end

  def full_name
    "#{first_name} #{middle_name} #{last_name}"
  end

  def self.search_by_order(course_id,search_by)
    condition_keys = "course_id = ?"
    condition_values = []
    condition_values << course_id
    all_conditions=[]
    if search_by[:status].present?
      if search_by[:status]=="false"
        condition_keys+=" and transferred = ?"
        condition_values << false
      elsif search_by[:status]=="true"
        condition_keys+=" and transferred = ?"
        condition_values << true
      end
    end
    if search_by[:created_at_gte].present?
      condition_keys+=" and created_at >= ?"
      condition_values << search_by[:created_at_gte].to_time.beginning_of_day
    end
    if search_by[:created_at_lte].present?
      condition_keys+=" and created_at <= ?"
      condition_values << search_by[:created_at_lte].to_time.end_of_day
    end
    all_conditions << condition_keys
    all_conditions += condition_values
    # case sorted_order
    # when "reg_no-descend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "reg_no desc")
    # when "reg_no-ascend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "reg_no asc")
    # when "name-descend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "first_name desc")
    # when "name-ascend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "first_name asc")
    # when "da_te-descend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "created_at desc")
    # when "da_te-ascend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "created_at asc")
    # when "status-descend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "status desc")
    # when "status-ascend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "status asc")
    # when "paid-descend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "has_paid desc")
    # when "paid-ascend"
    #   pre_student_registration=self.find(:all, :conditions=>all_conditions, :order => "has_paid asc")
    # else
      pre_student_registration=self.find(:all, :conditions=>all_conditions)
    # end
    return pre_student_registration
  end
end
