class AddColumnsToWriteoff < ActiveRecord::Migration
  def self.up
  		add_column :writeoff_deposits, :school_id, :integer
  		add_column :writeoff_details, :school_id, :integer
  end

  def self.down
  end
end
