namespace :prestudent_additional_add_to_student_additional do
  desc "Updating Student Additional Details from pre student additional details"
  task :install => :environment  do
  	@school = School.all
  	@school.each do |sc|
  		MultiSchool.current_school = School.find sc.id
  		PreStudentRegistration.find_all_by_school_id(sc.id).each do |psr|
  			psr.pre_student_additional_details.each do |pad|
  				StudentAdditionalDetail.create!(:student_id=>psr.student_id,:additional_field_id=>pad.additional_field_id,:additional_info=>pad.additional_info,:school_id=>pad.school_id)
  			end
  		end
  	end
  end
end