class AddColumnsToCourseAndMasterCourse < ActiveRecord::Migration
  def self.up
  	add_column :courses, :status, :boolean ,:default => false
    add_column :courses, :fee_structure , :decimal ,:precision => 15, :scale => 2
    add_column :master_courses, :status, :boolean ,:default => false
    add_column :master_courses, :fee_structure , :decimal ,:precision => 15, :scale => 2
   end

  def self.down
  	remove_column :courses, :status
  	remove_column :courses, :fee_structure
  	remove_column :master_courses, :status
  	remove_column :master_courses, :fee_structure
  end
end
