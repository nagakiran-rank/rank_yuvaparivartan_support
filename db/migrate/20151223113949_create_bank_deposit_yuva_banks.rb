class CreateBankDepositYuvaBanks < ActiveRecord::Migration
  def self.up
    create_table :bank_deposit_yuva_banks do |t|
    	t.integer :bank_id
    	t.decimal :total_amount
    	t.string :upload_document_file_name
        t.string :upload_document_content_type
        t.binary :upload_document_data, :limit => 500.kilobytes
        t.integer :upload_document_file_size
        t.date :date_of_deposit
        t.date :date_of_collection    


      t.timestamps
    end
  end

  def self.down
    drop_table :bank_deposit_yuva_banks
  end
end
