class AddAmountToWriteoffDeposit < ActiveRecord::Migration
  def self.up
  	add_column :writeoff_deposits, :amount, :decimal
  end

  def self.down
  	remove_column :writeoff_deposits, :amount
  end
end
