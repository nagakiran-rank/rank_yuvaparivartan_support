class AddColumneToPreStudentRegistration < ActiveRecord::Migration
  def self.up
  	add_column :pre_student_registrations, :pre_student_category_id, :integer
  	add_column :pre_student_registrations, :religion, :string
  end

  def self.down
  end
end
