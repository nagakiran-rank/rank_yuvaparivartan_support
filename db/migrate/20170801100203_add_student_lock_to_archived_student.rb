class AddStudentLockToArchivedStudent < ActiveRecord::Migration
  def self.up
  	add_column :archived_students, :student_lock, :boolean ,:default => false
  end

  def self.down
  end
end
