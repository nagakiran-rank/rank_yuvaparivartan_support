class PreFinanceTransaction < ActiveRecord::Base
	belongs_to :pre_student_registration
	after_create :add_pre_finance_receipt_number

	def add_pre_finance_receipt_number
		pre_receipt_numbers = PreFinanceTransaction.search(:receipt_no_not_like => "refund").map { |f| f.receipt_no }
      #pre_last_no = pre_receipt_numbers.map { |k| k.scan(/\d+$/i).last.to_i }.max
       pre_last_no = pre_receipt_numbers.map { |k| k.scan(/\d+$/i).last}.max
      pre_last_transaction = PreFinanceTransaction.last(:conditions => ["receipt_no NOT LIKE '%refund%' and receipt_no LIKE ?", "%#{pre_last_no}"])
      pre_last_receipt_no = pre_last_transaction.receipt_no unless pre_last_transaction.nil?
      unless pre_last_receipt_no.nil?
        pre_receipt_split = /(.*?)(\d+)$/.match(pre_last_receipt_no)
        if pre_receipt_split[1].blank?
          pre_receipt_number = pre_receipt_split[2].next
        else
          pre_receipt_number = pre_receipt_split[1]+pre_receipt_split[2].next
        end
      else
        config_receipt_no = Configuration.get_config_value('FeeReceiptNo')
        pre_receipt_number = config_receipt_no.present? ? config_receipt_no : "PRE0001"
      end
      puts "#{pre_receipt_number.inspect}*******************"
      self.update_attributes(:receipt_no => pre_receipt_number)
	end
end
