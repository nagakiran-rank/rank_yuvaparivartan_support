class AddColumnToBdft < ActiveRecord::Migration
  def self.up
  	add_column :pre_student_registrations, :school_id, :integer
  	add_column :bank_deposit_finance_transactions, :vertical_id, :integer
  	add_column :bank_deposit_finance_transactions, :batch_id, :integer
  	add_column :bank_deposit_finance_transactions, :writeoff_amount, :decimal
  	add_column :bank_deposit_finance_transactions, :pre_student_registration_id, :integer
  end

  def self.down
  end
end
