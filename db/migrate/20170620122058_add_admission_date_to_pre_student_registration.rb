class AddAdmissionDateToPreStudentRegistration < ActiveRecord::Migration
  def self.up
  	add_column :pre_student_registrations, :admission_date, :date
  end

  def self.down
  	remove_column :pre_student_registrations, :admission_date
  end
end
