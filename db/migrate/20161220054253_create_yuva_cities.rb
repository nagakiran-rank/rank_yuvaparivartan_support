class CreateYuvaCities < ActiveRecord::Migration
  def self.up
    create_table :yuva_cities do |t|
    	t.string :name
    	t.integer :state_id
      # t.timestamps
    end
  end

  def self.down
    drop_table :yuva_cities
  end
end
