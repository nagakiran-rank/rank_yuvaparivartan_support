namespace :add_value_attendance_weekday_sets do
  desc "Automatically create/update columns for old batches in the attendance weekday sets"
  task :install => :environment  do
  	@school = School.all
  	@school.each do |s|
	  	MultiSchool.current_school = School.find s.id
	    @batch = Batch.all
	    @batch.each do |t|
	    	if AttendanceWeekdaySet.exists?(:batch_id => t.id) == false
		    	AttendanceWeekdaySet.create(:batch_id=>t.id,:weekday_set_id=>t.weekday_set_id,:start_date=>t.start_date,:end_date=>t.end_date,:created_at=>t.created_at,:updated_at=>t.updated_at)
		    	# recup = ActiveRecord::Base.connection.insert("INSERT INTO attendance_weekday_sets (batch_id, weekday_set_id, start_date, end_date, created_at, updated_at) VALUES (#{t.id},#{t.weekday_set_id},'#{t.start_date}','#{t.end_date}','#{t.created_at}','#{t.updated_at}'); ")
		     #                if recup == 1
		     #                  p "successfully updated column image_updated_at in redactor_upload record"
		     #                  puts      #blank line
		     #                end
		     puts "the values have been added for batch #{t.name}"
		 	end
	    end	   
	    puts "the values have been added for #{s.name}"
	    puts "************************************************************************************************"
	end
  end
end