Gretel::Crumbs.layout do
  crumb :yuva_parivarthan_banks_index do
    link I18n.t('bank_details'), {:controller=>"yuva_parivarthan_banks", :action=>"index"}
    parent :finance_index
  end
  crumb :yuva_parivarthan_banks_new_bank do
    link I18n.t('add_new_bank'), {:controller=>"yuva_parivarthan_banks", :action=>"new_bank"}
    parent :yuva_parivarthan_banks_index
  end
  crumb :yuva_parivarthan_banks_show do |bank|
    link  I18n.t('show'),{:controller=>"yuva_parivarthan_banks", :action=>"show",:id=>bank.id}
    parent :yuva_parivarthan_banks_index
  end
  crumb :yuva_parivarthan_banks_edit_bank do |bank|
    link I18n.t('edit'), {:controller=>"yuva_parivarthan_banks", :action=>"edit_bank", :id => bank.id}
    parent :yuva_parivarthan_banks_index
  end
  crumb :bank_deposits_index do
    link I18n.t('bank_deposit_text'), {:controller=>"bank_deposits",:action=> "index"}
    parent :finance_index
  end
  crumb :bank_deposits_bank_deposit_details do
    link I18n.t('bank_deposit_details'), {:controller=>"bank_deposits",:action=> "bank_deposit_details"}
    parent :finance_index
  end
  crumb :bank_deposits_edit_bank_deposit_details do |d|
    link I18n.t('edit'), {:controller=>"bank_deposits",:action=> "edit_bank_deposit_details", :id=> d.id}
    parent :bank_deposits_bank_deposit_details
  end
  crumb :bank_deposits_show_bank_deposit_details do |d|
    link I18n.t('show'), {:controller=>"bank_deposits",:action=> "show_bank_deposit_details", :id=> d.id}
    parent :bank_deposits_bank_deposit_details
  end
  crumb :finance_edit_finance_transaction do |d|
    link I18n.t('edit'), {:controller=>"finance",:action=> "edit_finance_transaction", :id=> d.id}
    parent :finance_fees_submission_batch
  end
  crumb :courses_locked_batches do |course|
    link I18n.t('locked_batch'), {:controller=>"courses",:action=> "locked_batches"}
    parent :courses_show, course
  end
  crumb :pre_student_registrations_index do
    link I18n.t('pre_student_registrations'), {:controller=>"pre_student_registrations", :action=>"index"}
    # parent :finance_index
  end
  crumb :pre_student_registrations_manage_courses do
    link I18n.t('manage_course'), {:controller=>"pre_student_registrations", :action=>"manage_courses"}
     parent :pre_student_registrations_index
  end
  crumb :pre_student_registrations_pre_student_applicant do |d|
    link I18n.t('pre_student_applicant'), {:controller=>"pre_student_registrations", :action=>"pre_student_applicant" , :id=> d}
     parent :pre_student_registrations_manage_courses
  end
  crumb :pre_finance_transactions_advance_fee_collection do 
    link I18n.t('advance_fee_collection'), {:controller=>"pre_finance_transactions", :action=>"advance_fee_collection"}
    parent :finance_index
  end
  crumb :pre_student_registrations_view_pre_student do |d|
    link I18n.t('pre_student_show'), {:controller=>"pre_student_registrations", :action=>"view_pre_student",:id=> d}
     parent :pre_student_registrations_pre_student_applicant,d
  end
  crumb :pre_student_registrations_pre_student_edit do |d|
    link I18n.t('pre_student_edit'), {:controller=>"pre_student_registrations", :action=>"pre_student_edit" ,:id=> d}
     parent :pre_student_registrations_view_pre_student, d
  end
  crumb :writeoff_deposits_writeoff_reports_details do
    link I18n.t('writeoff_reports_details'), {:controller=>"writeoff_deposits", :action=>"writeoff_reports_details"}
    parent :report_index
  end
  crumb :writeoff_deposits_deposits do 
    link I18n.t('writeoff_deposit'), {:controller=>"writeoff_deposits",:action=> "deposits"}
    parent :bank_deposits_bank_deposit_details
  end
  crumb :bank_deposits_write_off do 
    link I18n.t('write_off'), {:controller=>"bank_deposits",:action=> "write_off"}
    parent :bank_deposits_bank_deposit_details
  end
  crumb :writeoff_deposits_show_writeoff_deposit_details do 
    link I18n.t('show_writeoff_deposit_details'), {:controller=>"writeoff_deposits",:action=> "show_writeoff_deposit_details"}
    parent :writeoff_deposits_deposits
  end
  crumb :writeoff_deposits_edit_writeoff_deposit_details do 
    link I18n.t('edit_writeoff_deposit_details'), {:controller=>"writeoff_deposits",:action=> "edit_writeoff_deposit_details"}
    parent :writeoff_deposits_deposits
  end
  crumb :bank_deposits_view_bank_deposit_details do |d|
    link I18n.t('view_bank_deposit_details'), {:controller=>"bank_deposits",:action=> "view_bank_deposit_details", :id=> d.id}
    parent :bank_deposits_bank_deposit_details
  end
end
