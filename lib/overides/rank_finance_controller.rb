module RankYuvaparivartanSupport
  module RankFinanceController
    def self.included (base)
      base.instance_eval do
        alias_method_chain :update_ajax ,:tmpl
        alias_method_chain :load_fees_submission_batch, :tmpl
        alias_method_chain :fees_submission_batch, :tmpl
        alias_method_chain :fees_submission_student, :tmpl
        alias_method_chain :fees_submission_save, :tmpl 
        alias_method_chain :fee_collection_batch_update, :tmpl
        # alias_method_chain :delete_transaction_by_batch, :tmpl
        # alias_method_chain :transaction_deletion, :tmpl
        alias_method_chain :student_fee_receipt_pdf, :tmpl
        alias_method_chain :donors, :tmpl
        alias_method_chain :donation_edit, :tmpl
        alias_method_chain :donation_receipt, :tmpl
        alias_method_chain :donation_receipt_pdf, :tmpl
        alias_method_chain :fees_particulars_new, :tmpl
        alias_method_chain :fees_particulars_create,:tmpl
        alias_method_chain :fee_collection_new,:tmpl
        alias_method_chain :fee_collection_create,:tmpl
        alias_method_chain :fee_collection_batch_update,:tmpl
        alias_method_chain :pay_fees_defaulters,:tmpl
        alias_method_chain :fees_particulars_create2 , :tmpl
        alias_method_chain :master_category_new , :tmpl
        alias_method_chain :show_master_categories_list , :tmpl
        alias_method_chain :master_category_particulars , :tmpl
        alias_method_chain :list_category_batch , :tmpl
        alias_method_chain :show_fee_discounts , :tmpl
        alias_method_chain :fee_collection_dates_batch , :tmpl

      end
    end
    def fees_submission_batch_with_tmpl
      @batches = Batch.find(:all, :conditions => {:is_deleted => false, :is_active => true}, :joins => :course, :select => "`batches`.*,CONCAT(courses.code,'-',batches.name) as course_full_name", :order => "course_full_name")
      @inactive_batches = Batch.find(:all, :conditions => {:is_deleted => false, :is_active => false}, :joins => :course, :select => "`batches`.*,CONCAT(courses.code,'-',batches.name) as course_full_name", :order => "course_full_name")
      @dates = []
      render :template => 'rank_finance/fees_submission_batch_with_tmpl'
    end

    def update_ajax_with_tmpl
      @batch = Batch.find(params[:batch_id])
      @date = @fee_collection = FinanceFeeCollection.find(params[:date])
      student_ids=@date.finance_fees.find(:all, :conditions => "batch_id='#{@batch.id}'").collect(&:student_id).join(',')
      @dates = @batch.finance_fee_collections
      @student = Student.find(params[:student]) if params[:student]
      @student ||= FinanceFee.first(:conditions => "fee_collection_id = #{@date.id}", :joins => 'INNER JOIN students ON finance_fees.student_id = students.id').student
      @prev_student = @student.previous_fee_student(@date.id, student_ids)
      @next_student = @student.next_fee_student(@date.id, student_ids)
      @due_date = @fee_collection.due_date
      total_fees =0

      @financefee = @student.finance_fee_by_date @date
      @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id, :conditions => ["is_deleted IS NOT NULL"])
      @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
      # @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
      # @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@batch.id,@student.id,@date.fee_category_id)
      dis_count=FeeDiscount.find_all_by_batch_id(@batch.id)
      dis_count.each do |dis|
        if dis.receiver_type == "Batch" or dis.receiver_type == "StudentCategory"
          @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
        else
          @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@batch.id,@student.id,@date.fee_category_id)
        end
      end
      @total_discount = 0
      @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
      @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?


      bal=(@total_payable-@total_discount).to_f
      days=(Date.today-@date.due_date.to_date).to_i
      auto_fine=@date.fine
      if days > 0 and auto_fine
        @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
        @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule

      end


      total_fees =@financefee.balance.to_f+params[:special_fine].to_f

      unless params[:fine].nil?
        unless @financefee.is_paid == true
          total_fees += params[:fine].to_f
        else
          total_fees = params[:fine].to_f
        end
      end
      unless params[:fees][:fees_paid].to_f <= 0
        unless params[:fees][:payment_mode].blank?
          unless FedenaPrecision.set_and_modify_precision(params[:fees][:fees_paid]).to_f > FedenaPrecision.set_and_modify_precision(total_fees).to_f
            transaction = FinanceTransaction.new
            (@financefee.balance.to_f > params[:fees][:fees_paid].to_f) ? transaction.title = "#{t('receipt_no')}. (#{t('partial')}) F#{@financefee.id}" : transaction.title = "#{t('receipt_no')}. F#{@financefee.id}"
            transaction.category = FinanceTransactionCategory.find_by_name("Fee")
            transaction.payee = @student
            transaction.amount = params[:fees][:fees_paid].to_f
            transaction.fine_amount = params[:fine].to_f
            transaction.fine_included = true unless params[:fine].nil?
            if params[:special_fine] and FedenaPrecision.set_and_modify_precision(total_fees)==params[:fees][:fees_paid]
              # transaction.fine_amount = params[:fine].to_f
              # transaction.fine_included = true
              @fine_amount=0
            end
            transaction.finance = @financefee
            transaction.transaction_date=params[:transaction_date]
            transaction.payment_mode = params[:fees][:payment_mode]
            transaction.payment_note = params[:fees][:payment_note]
            transaction.balance=transaction.amount
            transaction.save

            # is_paid =@financefee.balance==0 ? true : false
            # @financefee.update_attributes(:is_paid => is_paid)

            @paid_fees = @financefee.finance_transactions
          else
            @paid_fees = @financefee.finance_transactions
            @financefee.errors.add_to_base("#{t('flash19')}")
          end
        else
          @paid_fees = @financefee.finance_transactions
          @financefee.errors.add_to_base("#{t('select_one_payment_mode')}")
        end
      else
        @paid_fees = @financefee.finance_transactions
        @financefee.errors.add_to_base("#{t('flash23')}")
      end
      if @fine_rule and @financefee.balance==0
        @fine_amount=@fine_amount-@paid_fees.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
      end
      @financefee.reload
      render :update do |page|
        page.replace_html "student", :partial => "rank_finance/student_fees_submission"
      end
    end
    def load_fees_submission_batch_with_tmpl
      @batch = Batch.find(params[:batch_id])
      @date = @fee_collection = FinanceFeeCollection.find(params[:date])
      student_ids=@date.finance_fees.find(:all, :conditions => "batch_id='#{@batch.id}'").collect(&:student_id).join(',')
      @dates = @batch.finance_fee_collections

      if params[:student]
        @student = Student.find(params[:student])
        @student_batch = Student.find(params[:student]).all_batches.reverse
        @fee = FinanceFee.first(:conditions => "fee_collection_id = #{@date.id}", :joins => "INNER JOIN students ON finance_fees.student_id = '#{@student.id}'")
      else
        archieved_student=ArchivedStudent.find_by_former_id(student_ids) if student_ids.nil?
       unless archieved_student.nil?
          @student = Student.find(student_ids) if student_ids.nil?
          @student_batch = Student.find(@student.id).all_batches.reverse if student_ids.nil?
          @fee = FinanceFee.first(:conditions => "fee_collection_id = #{@date.id} and FIND_IN_SET(students.id,'#{ student_ids}')", :joins => 'INNER JOIN students ON finance_fees.student_id = students.id')
        else
          
          @fee = FinanceFee.first(:conditions => "fee_collection_id = #{@date.id} and FIND_IN_SET(students.id,'#{ student_ids}')", :joins => 'INNER JOIN students ON finance_fees.student_id = students.id')
        end
      end
      unless @fee.nil?

        @student ||= @fee.student
        @prev_student = @student.previous_fee_student(@date.id, student_ids)
        @next_student = @student.next_fee_student(@date.id, student_ids)
        @financefee = @student.finance_fee_by_date @date
        @due_date = @fee_collection.due_date
        @paid_fees = @fee.finance_transactions
        
        @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id, :conditions => ["is_deleted = false"])

        @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
        # @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
        # @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@batch.id,@student.id,@date.fee_category_id)
        dis_count=FeeDiscount.find_all_by_batch_id(@batch.id)
        dis_count.each do |dis|
          if dis.receiver_type == "Batch" or dis.receiver_type == "StudentCategory"
            @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
          else
            @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@batch.id,@student.id,@date.fee_category_id)
          end
        end
        @total_discount = 0
        @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
        @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?
        bal=(@total_payable-@total_discount).to_f
        days=(Date.today-@date.due_date.to_date).to_i
        auto_fine=@date.fine
        if days > 0 and auto_fine
          @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
          @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule

          if @fine_rule and @financefee.balance==0
            @fine_amount=@fine_amount-@paid_fees.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
          end
        end
        @fine_amount=0 if @financefee.is_paid
        render :update do |page|
          page.replace_html "student", :partial => "rank_finance/student_fees_submission"
           page << "Modalbox.hide();"
        end
      else
        render :update do |page|
          page.replace_html "student", :text => '<p class="flash-msg">No students have been assigned this fee.</p>'
          page << "Modalbox.hide();"
        end
      end
    end
    def fees_submission_student_with_tmpl

      if params[:date].present?       
        @student = Student.find(params[:id])
        # @student_batch = Student.find(params[:id]).all_batches.reverse
        @date = @fee_collection = FinanceFeeCollection.find(params[:date])
        @financefee = @student.finance_fee_by_date(@date)
        @student_batch=Student.find_by_batch_id(@financefee.batch_id).all_batches.reverse

        @due_date = @fee_collection.due_date
        @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id, :conditions => ["is_deleted IS NOT NULL"])
        flash[:warning]=nil
        flash[:notice]=nil
        @paid_fees = @financefee.finance_transactions
        @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
        # @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
        # @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
        dis_count=FeeDiscount.find_all_by_batch_id(@student.batch.id)
        dis_count.each do |dis|
          if dis.receiver_type == "Batch" or dis.receiver_type == "StudentCategory"
             @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
          else
            @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
          end
        end
        @total_discount = 0
        @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
        @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?
        bal=(@total_payable-@total_discount).to_f
        days=(Date.today-@date.due_date.to_date).to_i
        auto_fine=@date.fine
        if days > 0 and auto_fine
          @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
          @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule
          if @fine_rule and @financefee.balance==0
            @fine_amount=@fine_amount-@paid_fees.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
          end
        end
        @fine_amount=0 if @financefee.is_paid
        render :update do |page|
          page.replace_html "fee_submission", :partial => "rank_finance/fees_submission_form"
          page << "Modalbox.hide();"
        end
      else
        render :update do |page|
          page.replace_html "fee_submission", :text => ""
        end
      end
    end

    def fees_submission_save_with_tmpl
      @student = Student.find(params[:student])
      @date = @fee_collection = FinanceFeeCollection.find(params[:date])
      @financefee = @date.fee_transactions(@student.id)

      @due_date = @fee_collection.due_date
      @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id, :conditions => ["is_deleted IS NOT NULL"])
      @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@student.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@student.batch) }
      # @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@student.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@student.batch) }
      # @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
      dis_count=FeeDiscount.find_all_by_batch_id(@student.batch.id)
      dis_count.each do |dis|
        if dis.receiver_type == "Batch" or dis.receiver_type == "StudentCategory"
          @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
        else
          @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
        end
      end
      @total_discount = 0
      @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
      @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?
      total_fees = @financefee.balance.to_f+FedenaPrecision.set_and_modify_precision(params[:special_fine]).to_f
      unless params[:fine].nil?
        total_fees += FedenaPrecision.set_and_modify_precision(params[:fine]).to_f
      end
      @paid_fees = @financefee.finance_transactions


      if request.post?

        unless params[:fees][:fees_paid].to_f <= 0
          unless params[:fees][:payment_mode].blank?
            unless FedenaPrecision.set_and_modify_precision(params[:fees][:fees_paid]).to_f > FedenaPrecision.set_and_modify_precision(total_fees).to_f
              transaction = FinanceTransaction.new
              (@financefee.balance.to_f > params[:fees][:fees_paid].to_f) ? transaction.title = "#{t('receipt_no')}. (#{t('partial')}) F#{@financefee.id}" : transaction.title = "#{t('receipt_no')}. F#{@financefee.id}"
              transaction.category = FinanceTransactionCategory.find_by_name("Fee")
              transaction.payee = @student
              transaction.finance = @financefee
              transaction.fine_included = true unless params[:fine].nil?
              transaction.amount = params[:fees][:fees_paid].to_f
              transaction.fine_amount = params[:fine].to_f
              if params[:special_fine] and total_fees==params[:fees][:fees_paid].to_f
                # transaction.fine_amount = params[:fine].to_f+params[:special_fine].to_f
                # transaction.fine_included = true
                @fine_amount=0
              end
              transaction.transaction_date = params[:transaction_date]
              transaction.payment_mode = params[:fees][:payment_mode]
              transaction.payment_note = params[:fees][:payment_note]
              transaction.balance = transaction.amount
              transaction.save
              # is_paid = @financefee.balance==0 ? true : false
              # @financefee.update_attributes(:is_paid => is_paid)
              flash[:warning] = "#{t('flash14')}"
              flash[:notice]=nil
            else
              flash[:warning]=nil
              flash[:notice] = "#{t('flash19')}"
            end
          else
            flash[:warning]=nil
            flash[:notice] = "#{t('select_one_payment_mode')}"
          end
        else
          flash[:warning]=nil
          flash[:notice] = "#{t('flash23')}"
        end
      end

      bal=(@total_payable-@total_discount).to_f
      days=(Date.today-@date.due_date.to_date).to_i
      auto_fine=@date.fine
      if days > 0 and auto_fine
        @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
        @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule and @financefee.is_paid==false
        if @fine_rule and @financefee.balance==0
          @fine_amount=@fine_amount-@paid_fees.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
        end
      end
      @financefee.reload
      @fine_amount=0 if @financefee.is_paid
      render :update do |page|
        page.replace_html "fee_submission", :partial => "rank_finance/fees_submission_form"
      end
    end    
    def edit_finance_transaction
      @transaction_values = FinanceTransaction.find(params[:id])
      @name = params[:student_name]
      if request.post? and @transaction_values.update_attributes(:Manual_receipt_no => params[:edit_finance_transaction][:Manual_receipt_no] , :Manual_receipt_date => params[:edit_finance_transaction][:Manual_receipt_date])
        flash[:notice] = "#{t('Transaction_Details_Updated_Sucessfully')}"
      end
      render :template => 'rank_finance/edit_finance_transaction'
    end
    def fee_collection_batch_update_with_tmpl
      if params[:id].present?
        @fee_category=FinanceFeeCategory.find(params[:id])
        if @current_user.admin?
        #@batches= Batch.find(:all,:joins=>"INNER JOIN `finance_fee_particulars` ON `batches`.id = `finance_fee_particulars`.batch_id INNER JOIN finance_fee_categories on finance_fee_categories.id=finance_fee_particulars.finance_fee_category_id INNER JOIN courses on courses.id=batches.course_id",:conditions=>"finance_fee_categories.name = '#{@fee_category.name}' and finance_fee_categories.description = '#{@fee_category.description}' and finance_fee_particulars.is_deleted=#{false}",:order=>"courses.code ASC").uniq
        @batches=Batch.active.find(:all, :joins => [{:finance_fee_particulars => :finance_fee_category}, :course], :conditions => "finance_fee_categories.id =#{@fee_category.id} and finance_fee_particulars.is_deleted=#{false}", :order => "courses.code ASC").uniq
        else
          @batches1=Batch.active.find(:all, :joins => [{:finance_fee_particulars => :finance_fee_category}, :course], :conditions => "finance_fee_categories.id =#{@fee_category.id} and finance_fee_particulars.is_deleted=#{false}", :order => "courses.code ASC").uniq
          @batches=@batches1.find_all_by_end_date_lock(false)
        end
      end
      render :update do |page|
        page.replace_html "batchs", :partial => "rank_finance/fee_collection_batchs"
      end
    end
    # def delete_transaction_by_batch_with_tmpl
    #   transaction_deletion
    #   @batch = Batch.find(params[:batch_id])
    #   student_ids=@date.finance_fees.find(:all, :conditions => "batch_id='#{@batch.id}'").collect(&:student_id).join(',')
    #   @student = Student.find(student_ids) if student_ids.nil?
    #   @student_batch = Student.find(@student.id).all_batches.reverse if student_ids.nil?
    #   @dates = FinanceFeeCollection.find(:all)
    #   @fee = FinanceFee.first(:conditions => "fee_collection_id = #{@date.id}", :joins => 'INNER JOIN students ON finance_fees.student_id = students.id')
    #   @student ||= @fee.student
    #   @prev_student = @student.previous_fee_student(@date.id, student_ids)
    #   @next_student = @student.next_fee_student(@date.id, student_ids)

    #   render :update do |page|
    #     page.replace_html "student", :partial => "rank_finance/student_fees_submission"
    #   end
    # end
    # def transaction_deletion_with_tmpl
    #   @student = Student.find(params[:id])
    #   @date = @fee_collection = FinanceFeeCollection.find(params[:date])
    #   @financetransaction=FinanceTransaction.find(params[:transaction_id])
    #   ActiveRecord::Base.transaction do
    #     if FedenaPlugin.can_access_plugin?("fedena_pay")
    #       payment = @financetransaction.payment
    #       unless payment.nil?
    #         status = Payment.payment_status_mapping[:reverted]
    #         payment.update_attributes(:status_description => status)
    #         payment.save
    #       end
    #     end
    #     if @financetransaction
    #       mft=@financetransaction.multi_fees_transactions
    #       if mft.present?
    #         mft_amount=mft.first.amount.to_f-@financetransaction.amount.to_f
    #         if mft_amount==0.0
    #           mft.first.send(:destroy_without_callbacks)
    #         else
    #           mft.first.update_attributes(:amount => mft_amount)
    #         end
    #       end
    #       # raise ActiveRecord::Rollback unless @financetransaction.destroy 
    #       # raise ActiveRecord::Rollback
    #       bdft=BankDepositFinanceTransaction.find_by_finance_transaction_id(@financetransaction.id)
    #       unless bdft.nil?
    #         flash[:notice] = "#{t('already_this_transaction_present_in_deposit_details')}"
    #       else
    #         raise ActiveRecord::Rollback unless @financetransaction.destroy 
    #       end
    #     end
    #   end
    #   @financefee = @student.finance_fee_by_date(@date)
    #   @student_batch=Student.find_by_batch_id(@financefee.batch_id).all_batches.reverse
    #   @due_date = @fee_collection.due_date
    #   @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id, :conditions => ["is_deleted IS NOT NULL"])

    #   flash[:warning]=nil
    #   flash[:notice]=nil

    #   @paid_fees = @financefee.finance_transactions

    #   @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
    #   @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@financefee.batch_id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@financefee.batch) }
    #   @total_discount = 0
    #   @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
    #   @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?
    #   bal=(@total_payable-@total_discount).to_f
    #   days=(Date.today-@date.due_date.to_date).to_i
    #   auto_fine=@date.fine
    #   @fine_amount=0
    #   @paid_fine=0
    #   if days > 0 and auto_fine
    #     @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}'"], :order => 'fine_days ASC')
    #     @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule
    #     @paid_fine=@fine_amount
    #     if @fine_rule.present?
    #       @fine_amount=@fine_amount-@paid_fees.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
    #     end
    #   end
    # end
    # def delete_transaction_for_student
    #   transaction_deletion
    #   render :update do |page|
    #     page.replace_html "fee_submission", :partial => "rank_finance/fees_submission_form"
    #   end
    # end
    def student_fee_receipt_pdf_with_tmpl
      @batch=Batch.find(params[:batch_id])
      @date = @fee_collection = FinanceFeeCollection.find(params[:id2])
      @student = Student.find(params[:id])
      @financefee = @student.finance_fee_by_date @date
      @due_date = @fee_collection.due_date

      @paid_fees = @financefee.finance_transactions
      @a =@paid_fees.collect(&:receipt_no).join(', ')
      @pre_receipt = @paid_fees.collect(&:pre_receipt_no).join(', ')
      @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id, :conditions => ["is_deleted = false"])
      @currency_type = currency

      @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
      # @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
      dis_count=FeeDiscount.find_all_by_batch_id(@student.batch.id)
      dis_count.each do |dis|
        if dis.receiver_type == "Batch" or dis.receiver_type == "StudentCategory"
          @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
        else
          @discounts = FeeDiscount.find_all_by_batch_id_and_receiver_id_and_finance_fee_category_id(@student.batch.id,@student.id,@date.fee_category_id)
        end
      end
      @total_discount = 0
      @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
      @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?
      bal=(@total_payable-@total_discount).to_f
      days=(Date.today-@date.due_date.to_date).to_i
      auto_fine=@date.fine
      if days > 0 and auto_fine
        @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
        @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule

        if @fine_rule and @financefee.balance==0
          @fine_amount=@fine_amount-@paid_fees.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
        end
      end
      @fine_amount=0 if @financefee.is_paid
      render :pdf => 'rank_finance/rank_student_fee_receipt_pdf',
             :template => "rank_finance/rank_student_fee_receipt_pdf" 

      @paid_fees.each do |p|
        p.update_attributes(:student_fee_receipt => 1)
      end
    end

    def edit_manual_receipt
      @transaction_values = FinanceTransaction.find(params[:id])
      @name = params[:student_name]
      if request.post? 
        values = params[:edit_finance_transaction]
        date = Date.new values["Manual_receipt_date(1i)"].to_i, values["Manual_receipt_date(2i)"].to_i, values["Manual_receipt_date(3i)"].to_i
        if date < Date.today or date == Date.today

           @transaction_values.update_attributes(:Manual_receipt_no => params[:edit_finance_transaction][:Manual_receipt_no] , :Manual_receipt_date => date)
             flash[:notice] = "#{t('Transaction_Details_Updated_Sucessfully')}"
              redirect_to :controller => 'finance', :action => 'load_fees_submission_batch', :batch_id => params[:batch_id],:date=>params[:date] , :student=>params[:stu_id]
        else
            render :update do |page|
             page.alert "enter valid date"
            end
         end     
      else  
         render :update do |page|
          page.replace_html 'modal-box', :partial => 'rank_finance/edit_manual_receipt'
           page << "Modalbox.show($('modal-box'), {title: '#{t('edit_manual_receipt_and_manual_date')}', width: 650});"
        end
      end
    end
    def edit_manual_receipt2
      @transaction_values = FinanceTransaction.find(params[:id])
      @name = params[:student_name]
      if request.post? 
        values = params[:edit_finance_transaction]
        date = Date.new values["Manual_receipt_date(1i)"].to_i, values["Manual_receipt_date(2i)"].to_i, values["Manual_receipt_date(3i)"].to_i
        if date < Date.today or date == Date.today
            @transaction_values.update_attributes(:Manual_receipt_no => params[:edit_finance_transaction][:Manual_receipt_no] , :Manual_receipt_date => date)
             flash[:notice] = "#{t('Transaction_Details_Updated_Sucessfully')}"
              redirect_to :controller => 'finance', :action => 'fees_submission_student',:id => params[:sid],:date=>params[:date]
        else
            render :update do |page|
             page.alert "enter valid date"
            end
         end       
      else  
         render :update do |page|
          page.replace_html 'modal-box', :partial => 'rank_finance/edit_manual_receipt2'
           page << "Modalbox.show($('modal-box'), {title: '#{t('edit_manual_receipt_and_manual_date')}', width: 650});"
        end
      end
    end
    def donation_receipt_pdf_with_tmpl
      @donation = FinanceDonation.find(params[:id])
      @finance_donation=FinanceTransaction.find(@donation.transaction_id)
      @currency_type = currency
      render :pdf => 'rank_finance/donation_receipt_pdf',
             :template => "rank_finance/donation_receipt_pdf"
    end

    def donors_with_tmpl
      @donations = FinanceDonation.find(:all, :order => 'transaction_date desc')
      render :template => 'rank_finance/donors_with_tmpl'
    end
    
    def donation_receipt_with_tmpl
      @donation = FinanceDonation.find(params[:id])
      @finance_donation=FinanceTransaction.find(@donation.transaction_id)
      render :template => 'rank_finance/donation_receipt_with_tmpl'
    end

    def donation_edit_with_tmpl
      @donation = FinanceDonation.find(params[:id])
      @transaction = FinanceTransaction.find(@donation.transaction_id)
      if request.post? and @donation.update_attributes(params[:donation])
        donor = "#{t('flash15')} #{params[:donation][:donor]}"
        FinanceTransaction.update(@transaction.id, :description => params[:donation][:description], :title => donor, :amount => params[:donation][:amount], :transaction_date => @donation.transaction_date, :Manual_receipt_no=>params[:manual_receipt_no], :Manual_receipt_date =>params[:manual_receipt_date])
        return redirect_to :controller => 'finance', :action => 'donors'
        flash[:notice] = "#{t('flash16')}"
      end
      render :template => 'rank_finance/donation_edit_with_tmpl'
    end
    def print_single_receipt_pdf
      @finance_transaction = FinanceTransaction.find(params[:id])
      @student = Student.find(params[:stu_id])
      @batch = Batch.find(params[:batch_id])
      @date = params[:date]
      render :pdf => 'rank_finance/print_single_receipt_pdf',
             :template => "rank_finance/print_single_receipt_pdf"
      @finance_transaction.update_attributes(:duplicate => 1)
    end

    def fees_particulars_new_with_tmpl
      @finance_fee_particular =FinanceFeeParticular.new()
      @fees_categories =FinanceFeeCategory.all(:select => "DISTINCT finance_fee_categories.*", :joins => [:category_batches], :conditions => "finance_fee_categories.is_deleted=0 AND finance_fee_categories.is_master=1", :order => "name ASC")
      #@fees_categories = FinanceFeeCategory.find(:all,:group=>'concat(name,description)',:conditions=> "is_deleted = 0 and is_master = 1")
      #@fees_categories.reject!{|f|f.batch.is_deleted or !f.batch.is_active }
      @student_categories = StudentCategory.active
      @all=true
      @student=false
      @category=false
      render :template => 'rank_finance/fees_particulars_new_with_tmpl'
    end
    def fees_particulars_create_with_tmpl
      if request.get?
        redirect_to :action => "fees_particulars_new"
      else
        @finance_category=FinanceFeeCategory.find_by_id(params[:finance_fee_particular][:finance_fee_category_id])
        @batches= Batch.find(:all, :joins => "INNER JOIN `category_batches` ON `batches`.id = `category_batches`.batch_id INNER JOIN finance_fee_categories on finance_fee_categories.id=category_batches.finance_fee_category_id INNER JOIN courses on courses.id=batches.course_id", :conditions => ["finance_fee_categories.name = ? and finance_fee_categories.description = ?", "#{@finance_category.name}", "#{@finance_category.description}"], :order => "courses.code ASC") if  @finance_category
        if params[:particular] and params[:particular][:batch_ids]
          batches=Batch.find(params[:particular][:batch_ids])
          @cat_ids=params[:particular][:batch_ids]
          if params[:particular][:receiver_id].present?
            all_admission_no = admission_no=params[:particular][:receiver_id].split(',')
            all_students = batches.map { |b| b.students.map { |stu| stu.admission_no } }.flatten
            rejected_admission_no = admission_no.select { |adm| !all_students.include? adm }
            unless (rejected_admission_no.empty?)
              @error = true
              @finance_fee_particular = FinanceFeeParticular.new(params[:finance_fee_particular])
              @finance_fee_particular.batch_id=1
              @finance_fee_particular.save
              @finance_fee_particular.errors.add_to_base("#{rejected_admission_no.join(',')} #{t('does_not_belong_to_batch')} #{batches.map { |batch| batch.full_name }.join(',')}")
            end

            selected_admission_no = all_admission_no.select { |adm| all_students.include? adm }
            selected_admission_no.each do |a|
              s = Student.first(:conditions => ["admission_no LIKE BINARY(?)", a])
              if s.nil?
                @error = true
                @finance_fee_particular = FinanceFeeParticular.new(params[:finance_fee_particular])
                @finance_fee_particular.save
                @finance_fee_particular.errors.add_to_base("#{a} #{t('does_not_exist')}")
              end
            end
            unless @error
              selected_admission_no.each do |a|
                s = Student.first(:conditions => ["admission_no LIKE BINARY(?)", a])
                batch=s.batch
                @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
                @finance_fee_particular.receiver_id=s.id
                @error = true unless @finance_fee_particular.save
              end
            end
          else
            @error_batch = ""
            batches.each do |batch|
              @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
              if (FinanceFeeParticular.find_by_finance_fee_category_id_and_amount_and_batch_id(params[:finance_fee_particular][:finance_fee_category_id],params[:finance_fee_particular][:amount],batch.id).present?) && (FinanceFeeParticular.find_all_by_finance_fee_category_id_and_amount_and_batch_id(params[:finance_fee_particular][:finance_fee_category_id],params[:finance_fee_particular][:amount],batch.id).map {|p| p.is_deleted}.include?(false))
                    @error = true
                    @error_batch = @error_batch + batch.name + ", "
              end
            end
            if @error_batch == ""
              @a = 1
            else
             @error = true
             @finance_fee_particular.errors.add_to_base("the fee particular based on the selected category and amount is present for batch #{@error_batch.chop}")
            end
            if @a == 1
              batches.each do |batch|
                if params[:finance_fee_particular][:receiver_type]=="Batch"
                  @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
                    @finance_fee_particular.receiver_id=batch.id
                    @error = true unless @finance_fee_particular.save
                elsif params[:finance_fee_particular][:receiver_type]=="StudentCategory"
                  @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
                  @error = true unless @finance_fee_particular.save
                  @finance_fee_particular.errors.add_to_base("#{t('category_cant_be_blank')}") if params[:finance_fee_particular][:receiver_id]==""
                else

                  @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
                  @error = true unless @finance_fee_particular.save
                  @finance_fee_particular.errors.add_to_base("#{t('admission_no_cant_be_blank')}")
                end
              end
            end
          end
        else
          @error=true
          @finance_fee_particular =FinanceFeeParticular.new(params[:finance_fee_particular])
          @finance_fee_particular.save
        end

        if @error
          @fees_categories = FinanceFeeCategory.find(:all, :group => :name, :conditions => "is_deleted = 0 and is_master = 1")
          @student_categories = StudentCategory.active

          @render=true
          if params[:finance_fee_particular][:receiver_type]=="Student"
            @student=true
          elsif params[:finance_fee_particular][:receiver_type]=="StudentCategory"
            @category=true
          else
            @all=true
          end

          render :action => 'fees_particulars_new'
        else
          flash[:notice]="#{t('particulars_created_successfully')}"
          redirect_to :action => "fees_particulars_new"
        end
      end
    end
    def fee_collection_new_with_tmpl
      @fines=Fine.active
      @fee_categories=FinanceFeeCategory.all(:select => "DISTINCT finance_fee_categories.*", :joins => [{:category_batches => :batch}, :fee_particulars], :conditions => "batches.is_active = 1 AND batches.is_deleted = 0 AND finance_fee_categories.is_deleted=0 AND finance_fee_particulars.is_deleted = 0")
      #@fee_categories=FinanceFeeCategory.find(:all,:joins=>"INNER JOIN finance_fee_particulars on finance_fee_particulars.finance_fee_category_id=finance_fee_categories.id AND finance_fee_particulars.is_deleted = 0 INNER JOIN batches on batches.id=finance_fee_particulars.batch_id AND batches.is_active = 1 AND batches.is_deleted = 0 AND finance_fee_categories.is_deleted=0",:group=>'concat(finance_fee_categories.name,finance_fee_categories.description)')
      @finance_fee_collection = FinanceFeeCollection.new
      render :template => 'rank_finance/fee_collection_new_with_tmpl'
    end
    def fee_collection_create_with_tmpl
      @user = current_user
      @fee_categories=FinanceFeeCategory.all(:select => "DISTINCT finance_fee_categories.*", :joins => [{:category_batches => :batch}, :fee_particulars], :conditions => "batches.is_active = 1 AND batches.is_deleted = 0 AND finance_fee_categories.is_deleted=0 AND finance_fee_particulars.is_deleted = 0")
      unless params[:finance_fee_collection].nil?
        fee_category_name = params[:finance_fee_collection][:fee_category_id]
        @fee_category = FinanceFeeCategory.find_all_by_id(fee_category_name, :conditions => ['is_deleted is false'])
      end
      category =[]
      @finance_fee_collection = FinanceFeeCollection.new
      if request.post?
        @fee_collection = params[:fee_collection]
        @error_batch = ""
        if @fee_collection.nil?
          flash[:notice] = "Fee Already Scheduled for the Batch"
        else
          @fee_collection["category_ids"].each do |p|
            if FinanceFeeCollection.find_by_fee_category_id_and_batch_id(params[:finance_fee_collection][:fee_category_id],p).present? && (FinanceFeeCollection.find_all_by_fee_category_id_and_batch_id(params[:finance_fee_collection][:fee_category_id],p).map {|p| p.is_deleted}.include?(false))
              @error_batch = @error_batch + ""
            end
          end
          if @error_batch = ""
            Delayed::Job.enqueue(DelayedFeeCollectionJob.new(@user, params[:finance_fee_collection], params[:fee_collection]))
            flash[:notice]="Collection is in queue. <a href='/scheduled_jobs/FinanceFeeCollection/1'>Click Here</a> to view the scheduled job."
          else      
            #flash[:notice] = t('flash_msg33')
          end
        end
      end
      redirect_to :action => 'fee_collection_new'
    end
    def fee_collection_batch_update_with_tmpl
      if params[:id].present?
        @fee_category=FinanceFeeCategory.find(params[:id])
        #@batches= Batch.find(:all,:joins=>"INNER JOIN `finance_fee_particulars` ON `batches`.id = `finance_fee_particulars`.batch_id INNER JOIN finance_fee_categories on finance_fee_categories.id=finance_fee_particulars.finance_fee_category_id INNER JOIN courses on courses.id=batches.course_id",:conditions=>"finance_fee_categories.name = '#{@fee_category.name}' and finance_fee_categories.description = '#{@fee_category.description}' and finance_fee_particulars.is_deleted=#{false}",:order=>"courses.code ASC").uniq
        @batches=Batch.active.find(:all, :joins => [{:finance_fee_particulars => :finance_fee_category}, :course], :conditions => "finance_fee_categories.id =#{@fee_category.id} and finance_fee_particulars.is_deleted=#{false}", :order => "courses.code ASC").uniq
      end
      render :update do |page|
        page.replace_html "batchs", :partial => "rank_finance/fee_collection_batchs"
      end
    end
    def print_single_receipt_pdf1
      @multi_fees_transactions = MultiFeesTransaction.find(params[:id])
      @a = @multi_fees_transactions.finance_transactions.collect(&:receipt_no).join(', ')
      @pre_receipt = @multi_fees_transactions.finance_transactions.collect(&:pre_receipt_no).join(', ')
      @student = Student.find(params[:stu_id])
      @batch = Batch.find(params[:batch_id])
      @date = params[:date]
      render :pdf => 'rank_finance/print_single_receipt_pdf1',
             :template => "rank_finance/print_single_receipt_pdf1"
      @multi_fees_transactions.finance_transactions.each do|p|
        p.update_attributes(:duplicate => 1)
      end
    end

    def pay_fees_defaulters_with_tmpl
      @batch=Batch.find(params[:batch_id])
      @fine = params[:fine].to_f unless params[:fine].nil?
      @student = Student.find(params[:id])
      @date = @fee_collection = FinanceFeeCollection.find(params[:date])
      @financefee = @date.fee_transactions(@student.id)
      @due_date = @fee_collection.due_date

      @fee_category = FinanceFeeCategory.find(@fee_collection.fee_category_id, :conditions => ["is_deleted IS NOT NULL"])
      @fee_particulars = @date.finance_fee_particulars.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
      @discounts=@date.fee_discounts.all(:conditions => "batch_id=#{@batch.id}").select { |par| (par.receiver.present?) and (par.receiver==@student or par.receiver==@financefee.student_category or par.receiver==@batch) }
      @total_discount = 0
      @total_payable=@fee_particulars.map { |s| s.amount }.sum.to_f
      @total_discount =@discounts.map { |d| @total_payable * d.discount.to_f/(d.is_amount? ? @total_payable : 100) }.sum.to_f unless @discounts.nil?

      bal=(@total_payable-@total_discount).to_f
      days=(Date.today-@date.due_date.to_date).to_i
      auto_fine=@date.fine
      @paid_fees = @financefee.finance_transactions
      if days > 0 and auto_fine
        @fine_rule=auto_fine.fine_rules.find(:last, :conditions => ["fine_days <= '#{days}' and created_at <= '#{@date.created_at}'"], :order => 'fine_days ASC')
        @fine_amount=@fine_rule.is_amount ? @fine_rule.fine_amount : (bal*@fine_rule.fine_amount)/100 if @fine_rule

    end

    total_fees = @financefee.balance.to_f+FedenaPrecision.set_and_modify_precision(@fine_amount).to_f

    total_fees += @fine unless @fine.nil?

    if request.post?

      unless params[:fees][:fees_paid].to_f <= 0
        unless params[:fees][:payment_mode].blank?
          #unless params[:fees][:fees_paid].to_f> @total_fees
          unless FedenaPrecision.set_and_modify_precision(params[:fees][:fees_paid]).to_f > FedenaPrecision.set_and_modify_precision(total_fees).to_f
            transaction = FinanceTransaction.new
            (@financefee.balance.to_f > params[:fees][:fees_paid].to_f) ? transaction.title = "#{t('receipt_no')}. (#{t('partial')}) F#{@financefee.id}" : transaction.title = "#{t('receipt_no')}. F#{@financefee.id}"
            transaction.category = FinanceTransactionCategory.find_by_name("Fee")
            transaction.payee = @student
            transaction.finance = @financefee
            transaction.amount = params[:fees][:fees_paid].to_f
            transaction.fine_included = true unless @fine.nil?
            transaction.fine_amount = params[:fine].to_f

            if params[:special_fine] and total_fees==params[:fees][:fees_paid].to_f
              # transaction.fine_amount = params[:fine].to_f+FedenaPrecision.set_and_modify_precision(params[:special_fine]).to_f
              # transaction.fine_included = true
              @fine_amount=0
            end
            transaction.transaction_date = params[:transaction_date]
            transaction.payment_mode = params[:fees][:payment_mode]
            transaction.payment_note = params[:fees][:payment_note]
            transaction.save

            # is_paid =@financefee.balance==0 ? true : false
            # @financefee.update_attributes(:is_paid => is_paid)

            @paid_fees = @financefee.finance_transactions
            flash[:notice] = "#{t('flash14')}"
            redirect_to :action => "pay_fees_defaulters", :id => @student, :date => @date, :batch_id => @batch.id
          else
            flash[:notice] = "#{t('flash19')}"
          end
        else
          flash[:warn_notice] = "#{t('select_one_payment_mode')}"
        end
      else
        flash[:warn_notice] = "#{t('flash23')}"
      end
    else
      render :template => 'rank_finance/pay_fees_defaulters_with_tmpl'
    end
    if @fine_rule and @financefee.balance==0
      @fine_amount=@fine_amount-@paid_fees.all(:conditions => ["description=?", 'fine_amount_included']).sum(&:fine_amount)
      @fine_amount= 0 if @fine_amount < 0
    end
    if @financefee.is_paid
      @fine=nil
      @fine_amount=0
    end
    # 
  end

  def fees_particulars_create2_with_tmpl
    batch=Batch.find(params[:finance_fee_particular][:batch_id])
    if params[:particular] and params[:particular][:receiver_id]
      all_admission_no = admission_no=params[:particular][:receiver_id].split(',')
      all_students = batch.students.map { |stu| stu.admission_no }.flatten
      rejected_admission_no = admission_no.select { |adm| !all_students.include? adm }
      unless (rejected_admission_no.empty?)
        @error = true
        @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
        @finance_fee_particular.save
        @finance_fee_particular.errors.add_to_base("#{rejected_admission_no.join(',')} #{t('does_not_belong_to_batch')} #{batch.full_name}")
      end

      selected_admission_no = all_admission_no.select { |adm| all_students.include? adm }
      selected_admission_no.each do |a|
        s = Student.first(:conditions => ["admission_no LIKE BINARY(?)", a])
        if s.nil?
          @error = true
          @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
          @finance_fee_particular.save
          @finance_fee_particular.errors.add_to_base("#{a} #{t('does_not_exist')}")
        end
      end
      unless @error
        unless selected_admission_no.present?
          @finance_fee_particular=batch.finance_fee_particulars.new(params[:finance_fee_particular])
          @finance_fee_particular.save
          @finance_fee_particular.errors.add_to_base("#{t('admission_no_cant_be_blank')}")
          @error = true
        else
          selected_admission_no.each do |a|
            s = Student.first(:conditions => ["admission_no LIKE BINARY(?)", a])
            @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
            @finance_fee_particular.receiver_id=s.id
            @error = true unless @finance_fee_particular.save
          end
        end
      end
    elsif params[:finance_fee_particular][:receiver_type]=="Batch"
      if (FinanceFeeParticular.find_by_finance_fee_category_id_and_amount_and_batch_id(params[:finance_fee_particular][:finance_fee_category_id],params[:finance_fee_particular][:amount],batch.id).present?) && (FinanceFeeParticular.find_all_by_finance_fee_category_id_and_amount_and_batch_id(params[:finance_fee_particular][:finance_fee_category_id],params[:finance_fee_particular][:amount],batch.id).map {|p| p.is_deleted}.include?(false))
                    @error = true
                    @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
                    @finance_fee_particular.errors.add_to_base("the fee particular based on the selected category and amount is present for batch #{batch.name}")
      else
        @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
        @finance_fee_particular.receiver_id=batch.id
        @error = true unless @finance_fee_particular.save
      end
    else
      @finance_fee_particular = batch.finance_fee_particulars.new(params[:finance_fee_particular])
      @error = true unless @finance_fee_particular.save
      @finance_fee_particular.errors.add_to_base("#{t('category_cant_be_blank')}") if params[:finance_fee_particular][:receiver_id]==""
    end
    @batch=batch
    @finance_fee_category = FinanceFeeCategory.find(params[:finance_fee_particular][:finance_fee_category_id])
    @particulars = FinanceFeeParticular.paginate(:page => params[:page], :conditions => ["is_deleted = '#{false}' and finance_fee_category_id = '#{@finance_fee_category.id}' and batch_id='#{@batch.id}' "])

  end

  def master_category_new_with_tmpl
    @finance_fee_category = FinanceFeeCategory.new
    if @current_user.admin? 
      @batches = Batch.active
    else
      @batches1 =Batch.active
      @batches = @batches1.find_all_by_end_date_lock(false)
    end
    respond_to do |format|
      format.js { render :template => 'rank_finance/master_category_new_with_tmpl' }
    end
  end

  def show_master_categories_list_with_tmpl
    unless params[:id].empty?
      @finance_fee_category = FinanceFeeCategory.new
      @finance_fee_particular = FinanceFeeParticular.new
      @batches = Batch.find params[:id] unless params[:id] == ""
      @master_categories =@batches.finance_fee_categories.find(:all, :conditions => ["is_deleted = '#{false}' and is_master = 1 "])
      #@master_categories = FinanceFeeCategory.find(:all,:conditions=> ["is_deleted = '#{false}' and is_master = 1 and batch_id=?",params[:id]])
      @student_categories = StudentCategory.active

      render :update do |page|
        page.replace_html 'categories', :partial => 'rank_finance/master_category_list'
      end
    else
      render :update do |page|
        page.replace_html 'categories', :text => ""
      end
    end
  end

  def master_category_particulars_with_tmpl
    @batch=Batch.find(params[:batch_id])
    @finance_fee_category = FinanceFeeCategory.find(params[:id])
    #categories=FinanceFeeCategory.find(:all,:include=>:category_batches,:conditions=>"name=@finance_fee_category.name and description=@finance_fee_category.description and is_deleted=#{false}").map{|d| d if d.category_batches.empty?}.compact
    #    categories=FinanceFeeCategory.find(:all,:include=>:category_batches,:conditions=>"name='#{@finance_fee_category.name}' and description='#{@finance_fee_category.description}' and is_deleted=#{false}").uniq.map{|d| d if d.batch_id==@batch.id}.compact
    #    if categories.present?
    #      @finance_fee_category = FinanceFeeCategory.find_by_name_and_batch_id_and_is_deleted(@finance_fee_category.name,@batch.id,false)
    #    end
    #@particulars = FinanceFeeParticular.paginate(:page => params[:page],:joins=>"INNER JOIN finance_fee_categories on finance_fee_categories.id=finance_fee_particulars.finance_fee_category_id",:conditions => ["finance_fee_particulars.is_deleted = '#{false}' and finance_fee_categories.name = '#{@finance_fee_category.name}' and finance_fee_categories.description = '#{@finance_fee_category.description}' and finance_fee_particulars.batch_id='#{@batch.id}' "])
    @particulars = FinanceFeeParticular.paginate(:page => params[:page], :conditions => ["is_deleted = '#{false}' and finance_fee_category_id = '#{@finance_fee_category.id}' and batch_id='#{@batch.id}' "])
    render :template => 'rank_finance/master_category_particulars_with_tmpl' 
  end

  def list_category_batch_with_tmpl
    fee_category=FinanceFeeCategory.find(params[:category_id])
    if @current_user.admin?
    #@batches= Batch.find(:all,:joins=>"INNER JOIN `category_batches` ON `batches`.id = `category_batches`.batch_id INNER JOIN finance_fee_categories on finance_fee_categories.id=category_batches.finance_fee_category_id INNER JOIN courses on courses.id=batches.course_id",:conditions=>"finance_fee_categories.name = '#{fee_category.name}' and finance_fee_categories.description = '#{fee_category.description}'",:order=>"courses.code ASC")
    @batches=Batch.active.find(:all, :joins => [{:category_batches => :finance_fee_category}, :course], :conditions => "finance_fee_categories.id =#{fee_category.id}", :order => "courses.code ASC").uniq
    else
      @batches1=Batch.active.find(:all, :joins => [{:category_batches => :finance_fee_category}, :course], :conditions => "finance_fee_categories.id =#{fee_category.id}", :order => "courses.code ASC").uniq
      @batches=@batches1.find_all_by_end_date_lock(false)
    end
    #@batches=fee_category.batches.all(:order=>"name ASC")
    render :update do |page|
      page.replace_html 'list-category-batch', :partial => 'rank_finance/list_category_batch'
    end
  end

  def show_fee_discounts_with_tmpl
    @batch=Batch.find(params[:b_id])
    if params[:id]==""
      render :update do |page|
        page.replace_html "discount-box", :text => ""
      end
    else

      @fee_category = FinanceFeeCategory.find(params[:id])
      @discounts = @fee_category.fee_discounts.all(:joins => "LEFT OUTER JOIN students ON students.id = fee_discounts.receiver_id AND fee_discounts.receiver_type = 'Student' LEFT OUTER JOIN batches ON batches.id = fee_discounts.receiver_id AND fee_discounts.receiver_type = 'Batch' LEFT OUTER JOIN student_categories ON student_categories.id = fee_discounts.receiver_id AND fee_discounts.receiver_type = 'StudentCategory'", :conditions => ["(students.id IS NOT NULL OR batches.id IS NOT NULL OR student_categories.id IS NOT NULL) AND fee_discounts.batch_id='#{@batch.id}' AND fee_discounts.is_deleted= 0"])

      render :update do |page|
        page.replace_html "discount-box", :partial => "rank_finance/show_fee_discounts"
      end
    end
  end

  def fee_collection_dates_batch_with_tmpl
    if params[:id].present?
      @batch= Batch.find(params[:id])
      @finance_fee_collections = @batch.finance_fee_collections
      render :update do |page|
        page.replace_html 'fee_collection_dates', :partial => 'rank_finance/fee_collection_dates_batch'
      end
    else
      render :update do |page|
        page.replace_html 'fee_collection_dates', :text => ''
      end
    end
  end

  end
end