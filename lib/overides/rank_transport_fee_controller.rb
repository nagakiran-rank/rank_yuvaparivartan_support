module RankYuvaparivartanSupport
  module RankTransportFeeController

  	def self.included (base)
      base.instance_eval do
        alias_method_chain :transport_defaulters_fee_pay, :tmpl
        alias_method_chain :transport_fee_collection_pay, :tmpl
        alias_method_chain :transport_fee_pay, :tmpl
        alias_method_chain :student_profile_fee_details, :tmpl
      end
    end
    def transport_defaulters_fee_pay_with_tmpl
    @transport_fee_defaulters_details = TransportFee.find params[:id]
    category_id = FinanceTransactionCategory.find_by_name("Transport").id
    transaction = FinanceTransaction.new
    transaction.title = @transport_fee_defaulters_details.transport_fee_collection.name
    transaction.category_id = category_id
    transaction.transaction_date = Date.today
    transaction.amount = @transport_fee_defaulters_details.bus_fare
    transaction.amount += params[:fine].to_f unless params[:fine].nil?
    transaction.fine_included = true unless params[:fine].nil?
    transaction.payee = @transport_fee_defaulters_details.receiver
    transaction.finance = @transport_fee_defaulters_details
    transaction.balance = transaction.amount
#    if transaction.save
#      @transport_fee_defaulters_details.update_attribute(:transaction_id, transaction.id)
#    end
    @transport_defaulters = TransportFee.find_all_by_transport_fee_collection_id(@transport_fee_defaulters_details.transport_fee_collection_id)
    @transport_defaulters = @transport_defaulters.reject { |u| !u.transaction_id.nil? }
    @collection_id = params[:collection_id]
    @transport_fee_collection= TransportFeeCollection.find_by_id(params[:collection_id])
    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(params[:collection_id])
    #@transport_fee = @transport_fee.reject{|u| !u.transaction_id.nil? }
    @user = TransportFee.find_by_transport_fee_collection_id_and_id(params[:collection_id], params[:id]) unless params[:id].nil?
    @user ||= @transport_fee_collection.transport_fees.first(:conditions => ["transaction_id is null"])
    @next_user = @user.next_default_user unless @user.nil?
    @prev_user = @user.previous_default_user unless @user.nil?
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id) unless @user.nil?
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id) unless @user.nil?
    render :update do |page|
      page.replace_html 'defaulters_transport_fee_collection_details', :partial => 'defaulters_transport_fee_collection_details'
    end
  end

  def transport_fee_collection_pay_with_tmpl
    @transport_fee = TransportFee.find(params[:fees][:transport_fee_id])
    @date = @transport_fee.transport_fee_collection
    if params[:student].present?
    @student = Student.find(params[:student])
    @batch=@student.batch
    @students=Student.find(:all,:joins=>"inner join transport_fees tf on tf.receiver_id=students.id and tf.receiver_type='Student'", :conditions => "tf.transport_fee_collection_id='#{@date.id}' and tf.is_active=1 and students.batch_id='#{@batch.id}'",:order=>"id ASC")
    @prev_student=@students.select{|student| student.id<@student.id}.last||@students.last
    @next_student=@students.select{|student| student.id>@student.id}.first||@students.first
    end
    category_id = FinanceTransactionCategory.find_by_name("Transport").id
    @transaction = FinanceTransaction.new
    unless params[:fees][:payment_mode].blank?
      @transaction.title = @transport_fee.transport_fee_collection.name
      @transaction.category_id = category_id
      unless params[:fees][:fine].blank?
        @transaction.amount = @transport_fee.bus_fare + params[:fees][:fine].to_f
        @transaction.fine_included = true
        @transaction.fine_amount = params[:fees][:fine]
      else
        @transaction.amount = @transport_fee.bus_fare
      end
      @transaction.payee = @transport_fee.receiver
      @transaction.finance = @transport_fee
      @transaction.transaction_date = Date.today
      @transaction.payment_mode = params[:fees][:payment_mode]
      @transaction.payment_note = params[:fees][:payment_note]
      if @transaction.save
      	@transaction.update_attributes(:balance => @transaction.amount)
        user_event = UserEvent.first(:conditions => ["user_id = ? AND event_id = ?",@transport_fee.receiver.user_id,@date.event.id])
        user_event.destroy if user_event.present?
#        @transport_fee.update_attributes(:transaction_id => @transaction.id)
        flash[:notice]="#{t('fee_paid')}"
        flash[:warn_notice]=nil
      end
    else
      flash[:notice]=nil
      flash[:warn_notice]="#{t('select_one_payment_mode')}"
    end

    render :update do |page|
      page.replace_html 'fees_details', :partial => 'fees_details'
    end
  end

  def transport_fee_pay_with_tmpl

    @transport_fee_collection_details = TransportFee.find params[:id]
    category_id = FinanceTransactionCategory.find_by_name("Transport").id
    transaction = FinanceTransaction.new
    transaction.title = @transport_fee_collection_details.transport_fee_collection.name
    transaction.category_id = category_id
    transaction.amount = @transport_fee_collection_details.bus_fare
    transaction.amount += params[:fine].to_f unless params[:fine].nil?
    transaction.fine_included = true unless params[:fine].nil?
    transaction.transaction_date = Date.today
    transaction.payee = @transport_fee_collection_details.receiver
    transaction.finance = @transport_fee_collection_details
    transaction.balance = transaction.amount
    unless transaction.save
#      @transport_fee_collection_details.update_attribute(:transaction_id, transaction.id)
      render :text => transaction.errors.full_messages and return
    end
    @collection_id = params[:collection_id]
    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(params[:collection_id])
    #    @transport_fee = TransportFee.find_all_by_transport_fee_collection_id(@transport_fee_collection_details.transport_fee_collection_id)
    @user = TransportFee.find_by_transport_fee_collection_id_and_id(params[:collection_id], params[:id]) unless params[:id].nil?
    @user ||= @transport_fee.first
    @next_user = @user.next_user
    @prev_user = @user.previous_user
    @transport_fee_collection= TransportFeeCollection.find_by_id(@user.transport_fee_collection_id)
    @transaction = FinanceTransaction.find_by_id(@user.transaction_id)
    render :update do |page|
      page.replace_html 'transport_fee_collection_details', :partial => 'transport_fee_collection_details'
    end
  end

  def student_profile_fee_details_with_tmpl
    if FedenaPlugin.can_access_plugin?("fedena_pay")
      if ((PaymentConfiguration.config_value("enabled_fees").present? and PaymentConfiguration.config_value("enabled_fees").include? "Transport Fee"))
        @active_gateway = PaymentConfiguration.config_value("fedena_gateway")
        #        if @active_gateway == "Paypal"
        #          @merchant_id = PaymentConfiguration.config_value("paypal_id")
        #          @merchant ||= String.new
        #          @certificate = PaymentConfiguration.config_value("paypal_certificate")
        #          @certificate ||= String.new
        #        elsif @active_gateway == "Authorize.net"
        #          @merchant_id = PaymentConfiguration.config_value("authorize_net_merchant_id")
        #          @merchant_id ||= String.new
        #          @certificate = PaymentConfiguration.config_value("authorize_net_transaction_password")
        #          @certificate ||= String.new
        #        elsif @active_gateway == "Webpay"
        #          @merchant_id = PaymentConfiguration.config_value("webpay_merchant_id")
        #          @merchant_id ||= String.new
        #          @product_id = PaymentConfiguration.config_value("webpay_product_id")
        #          @product_id ||= String.new
        #          @item_id = PaymentConfiguration.config_value("webpay_item_id")
        #          @item_id ||= String.new
        #        end
        @custom_gateway = CustomGateway.find(@active_gateway)
#        @custom_gateway.gateway_parameters[:config_fields].each_pair do|k,v|
#          instance_variable_set("@"+k,v)
#        end
      end
    end

    hostname = "#{request.protocol}#{request.host_with_port}"

    @student=Student.find(params[:id])
    @fee= TransportFee.find_by_transport_fee_collection_id_and_receiver_id(params[:id2], params[:id])
    @fee_collection = TransportFeeCollection.find(params[:id2])
    @amount = @fee.bus_fare
    @paid_fees = @fee.finance_transaction unless @fee.transaction_id.blank?

    if params[:create_transaction].present?
      gateway_response = Hash.new
      #      if @active_gateway == "Paypal"
      #        gateway_response = {
      #          :amount => params[:amt],
      #          :status => params[:st],
      #          :transaction_id => params[:tx]
      #        }
      #      elsif @active_gateway == "Authorize.net"
      #        gateway_response = {
      #          :x_response_code => params[:x_response_code],
      #          :x_response_reason_code => params[:x_response_reason_code],
      #          :x_response_reason_text => params[:x_response_reason_text],
      #          :x_avs_code => params[:x_avs_code],
      #          :x_auth_code => params[:x_auth_code],
      #          :x_trans_id => params[:x_trans_id],
      #          :x_method => params[:x_method],
      #          :x_card_type => params[:x_card_type],
      #          :x_account_number => params[:x_account_number],
      #          :x_first_name => params[:x_first_name],
      #          :x_last_name => params[:x_last_name],
      #          :x_company => params[:x_company],
      #          :x_address => params[:x_address],
      #          :x_city => params[:x_city],
      #          :x_state => params[:x_state],
      #          :x_zip => params[:x_zip],
      #          :x_country => params[:x_country],
      #          :x_phone => params[:x_phone],
      #          :x_fax => params[:x_fax],
      #          :x_invoice_num => params[:x_invoice_num],
      #          :x_description => params[:x_description],
      #          :x_type => params[:x_type],
      #          :x_cust_id => params[:x_cust_id],
      #          :x_ship_to_first_name => params[:x_ship_to_first_name],
      #          :x_ship_to_last_name => params[:x_ship_to_last_name],
      #          :x_ship_to_company => params[:x_ship_to_company],
      #          :x_ship_to_address => params[:x_ship_to_address],
      #          :x_ship_to_city => params[:x_ship_to_city],
      #          :x_ship_to_zip => params[:x_ship_to_zip],
      #          :x_ship_to_country => params[:x_ship_to_country],
      #          :x_amount => params[:x_amount],
      #          :x_tax => params[:x_tax],
      #          :x_duty => params[:x_duty],
      #          :x_freight => params[:x_freight],
      #          :x_tax_exempt => params[:x_tax_exempt],
      #          :x_po_num => params[:x_po_num],
      #          :x_cvv2_resp_code => params[:x_cvv2_resp_code],
      #          :x_MD5_hash => params[:x_MD5_hash],
      #          :x_cavv_response => params[:x_cavv_response],
      #          :x_method_available => params[:x_method_available],
      #        }
      #
      #      elsif @active_gateway == "Webpay"
      #        if File.exists?("#{Rails.root}/vendor/plugins/fedena_pay/config/online_payment_url.yml")
      #          response_urls = YAML.load_file(File.join(Rails.root, "vendor/plugins/fedena_pay/config/", "online_payment_url.yml"))
      #        end
      #        txnref = params[:txnref] || params[:txnRef] || ""
      #        response_url = response_urls.nil? ? nil : eval(response_urls["webpay_response_url"].to_s)
      #        response_url ||= "https://stageserv.interswitchng.com/test_paydirect/api/v1/gettransaction.json"
      #        response_url += "?productid=#{@product_id.strip}"
      #        response_url += "&transactionreference=#{txnref}"
      #        response_url += "&amount=#{(('%.02f' % @amount).to_f * 100).to_i}"
      #        uri = URI.parse(response_url)
      #        http = Net::HTTP.new(uri.host, uri.port)
      #        http.use_ssl = true
      #        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      #        request = Net::HTTP::Get.new(uri.request_uri, {'Hash' => Digest::SHA512.hexdigest(@product_id.strip + txnref.to_s + @merchant_id.strip)})
      #        response = http.request(request)
      #        response_str = JSON.parse response.body
      #        gateway_response = {
      #          :split_accounts => response_str["SplitAccounts"],
      #          :merchant_reference => response_str["MerchantReference"],
      #          :response_code => response_str["ResponseCode"],
      #          :lead_bank_name => response_str["LeadBankName"],
      #          :lead_bank_cbn_code => response_str["LeadBankCbnCode"],
      #          :amount => response_str["Amount"].to_f/ 100,
      #          :card_number => response_str["CardNumber"],
      #          :response_description => response_str["ResponseDescription"],
      #          :transaction_date => response_str["TransactionDate"],
      #          :retrieval_reference_number => response_str["RetrievalReferenceNumber"],
      #          :payment_reference => response_str["PaymentReference"]
      #        }
      #
      #      end
      if @custom_gateway.present?
        @custom_gateway.gateway_parameters[:response_parameters].each_pair do|k,v|
          unless k.to_s == "success_code"
            gateway_response[k.to_sym] = params[v.to_sym]
          end
        end
      end
      @gateway_status = false
      #      if @active_gateway == "Paypal"
      #        @gateway_status = true if params[:st] == "Completed"
      #      elsif @active_gateway == "Authorize.net"
      #        @gateway_status = true if gateway_response[:x_response_reason_code] == "1"
      #      elsif @active_gateway == "Webpay"
      #        @gateway_status = true if gateway_response[:response_code] == "00"
      #      end
      if @custom_gateway.present?
        success_code = @custom_gateway.gateway_parameters[:response_parameters][:success_code]
        @gateway_status = true if gateway_response[:transaction_status] == success_code
      end
      payment = Payment.new(:payee => @student, :payment => @fee, :gateway_response => gateway_response, :status => @gateway_status, :amount => gateway_response[:amount].to_f, :gateway => @active_gateway)
      payment.fee_collection = @fee_collection
      if payment.save and @fee.transaction_id.nil?
        #        amount_from_gateway = 0
        #        if @active_gateway == "Paypal"
        #          amount_from_gateway = params[:amt]
        #        elsif @active_gateway == "Authorize.net"
        #          amount_from_gateway = params[:x_amount]
        #        elsif @active_gateway == "Webpay"
        #          amount_from_gateway = gateway_response[:amount]
        #        end
        amount_from_gateway = gateway_response[:amount]
        if amount_from_gateway.to_f > 0.0 and payment.status
          transaction = FinanceTransaction.new
          transaction.title = @fee.transport_fee_collection.name
          transaction.category_id = FinanceTransactionCategory.find_by_name('Transport').id
          transaction.finance = @fee
          transaction.amount = amount_from_gateway.to_f
          transaction.transaction_date = Date.today
          transaction.payment_mode = "Online payment"
          transaction.payee = @fee.receiver

          if transaction.save
          	transaction.update_attributes(:balance => transaction.amount)
#            @fee.update_attributes(:transaction_id => transaction.id)
            payment.update_attributes(:finance_transaction_id => transaction.id)
            #            online_transaction_id = payment.gateway_response[:transaction_id]
            #            online_transaction_id ||= payment.gateway_response[:x_trans_id]
            #            online_transaction_id ||= payment.gateway_response[:payment_reference]
            online_transaction_id = payment.gateway_response[:transaction_reference]
            @paid_fees=@fee.finance_transaction unless @fee.transaction_id.blank?
          end
          if @gateway_status
            status = Payment.payment_status_mapping[:success]
            payment.update_attributes(:status_description => status)
            flash[:notice] = "#{t('payment_success')} <br>  #{t('payment_reference')} : #{online_transaction_id}"
            if current_user.parent?
              user = current_user
            else
              user = @student.user
            end
            if @student.is_email_enabled && user.email.present?
              begin
                Delayed::Job.enqueue(OnlinePayment::PaymentMail.new(payment.fee_collection.name, user.email, user.full_name, @custom_gateway.name, payment.gateway_response[:amount], online_transaction_id, payment.gateway_response, user.school_details, hostname))
              rescue Exception => e
                return
              end
            end

          else
            status = Payment.payment_status_mapping[:failed]
            payment.update_attributes(:status_description => status)
            flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{payment.gateway_response[:transaction_reference] || 'N/A'}"
          end

        else
          status = Payment.payment_status_mapping[:failed]
          payment.update_attributes(:status_description => status)
          flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{payment.gateway_response[:transaction_reference] || 'N/A'}"
        end

      else
        flash[:notice] = "#{t('flash_payed')}"
      end
      redirect_to :controller => 'transport_fee', :action => 'student_profile_fee_details', :id => params[:id], :id2 => params[:id2]
    end
  end

  end
end