class YuvaParivarthanBanksController < ApplicationController
	before_filter :login_required
  	filter_access_to :all
	def index
		#@bank=YuvaparivarthanBank.all
		@bank =YuvaparivarthanBank.paginate(:page=>params[:page],:per_page=>10,:order=>"bank_name ASC")
	end

	def show
		@bank = YuvaparivarthanBank.find(params[:id])
	end

	def new_bank
		@bank = YuvaparivarthanBank.new(params[:yuvaparivarthan_bank])
		if request.post? and @bank.save
			flash[:notice] = "#{t('Bank_Details_Added_Sucessfully')}"
			redirect_to :action => "index" 
		end
	end
	
	def edit_bank
		@bank = YuvaparivarthanBank.find(params[:id])
		if request.post? and @bank.update_attributes(params[:yuvaparivarthan_bank])
			flash[:notice] = "#{t('Bank_Details_Updated_Sucessfully')}"
			redirect_to :action => "index"
		end
	end

	def destroy_bank   
		@bank = YuvaparivarthanBank.find(params[:id])
		if @bank.bank_deposit_yuva_banks.empty?
			@bank.destroy
			flash[:notice]="#{t('bank_details_deleted_successfully')}"
			redirect_to :action => "index"
		else
			flash[:notice]="#{t('bank_already_has_the_transactions')}"
			redirect_to :action => "index"
		end
	end
	
end
