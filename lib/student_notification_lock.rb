class StudentNotificationLock  < Struct.new(:batch_id,:school_id)
	def perform
		MultiSchool.current_school = School.find(school_id)
		@batch = Batch.find(batch_id)
		@students = @batch.students
		@students.each do |s|
			@amount = FinanceTransaction.find_all_by_payee_id(s.id).map{|a| a.amount}
			@paid = @amount.inject(0){|sum,x| sum + x }.to_i
			if @paid >= @batch.course.fee_structure.to_i
		 	else
		 		@delayed_student_lock_message = Delayed::Job.enqueue(DelayedReminderJob.new(:sender_id  => User.find_by_username("admin").id,
        :recipient_ids => s.user.id,
        :subject=>"Fee Due",
        :body=>"You haven't paid your total fee, Please pay due amount before this day #{(@batch.start_date + 35.days + 15.days).to_date.strftime('%d/%m/%Y')}, otherwise your details will be locked." ))
        #@delayed_student_lock_message.update_attibutes(:student_id => s.id)
			end
		end
	end
end