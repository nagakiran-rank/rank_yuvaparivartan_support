namespace :add_fee_structure_and_status do
  desc "Automatically update columns for old courses and master course"
  task :install => :environment  do
  	@school = School.all
  	@master_course = MasterCourse.all
  	@master_course.each do |master|
  		master.update_attributes(:fee_structure => 2000, :status => true)
  		puts"MasterCourse updated successfully--------------"
	  	@school.each do |s|
		  	MultiSchool.current_school = School.find s.id
		  	@courses = Course.find_all_by_master_course_id_and_school_id(master.id,s.id)
	  		@courses.each do |course|
		  		course.update_attribute(:fee_structure,master.fee_structure)
		  		course.update_attribute(:status,master.status)
	  			puts "course updated successfully=====#{course.inspect}====="
	  		end
			end
		end
  end
end