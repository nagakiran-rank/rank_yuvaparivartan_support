module RankYuvaparivartanSupport
  module RankSubjectsController

  	def self.included (base)
      base.instance_eval do
      	alias_method_chain :new, :tmpl
      	alias_method_chain :edit, :tmpl
      	alias_method_chain :destroy, :tmpl
      	alias_method_chain :destroy_elective_group, :tmpl
        alias_method_chain :show, :tmpl
      end
    end

  def new_with_tmpl
    @subject = Subject.new
    @batch = Batch.find params[:id] if request.xhr? and params[:id]
    @elective_group = ElectiveGroup.find params[:id2] unless params[:id2].nil?
    respond_to do |format|
      format.js { render :template => 'rank_subjects/new_with_tmpl' }
    end
  end

  def edit_with_tmpl
    @subject = Subject.find params[:id]
    @batch = @subject.batch
    @elective_group = ElectiveGroup.find params[:id2] unless params[:id2].nil?
    respond_to do |format|
      format.html { }
      format.js { render :template => 'rank_subjects/edit_with_tmpl' }
    end
  end

  def destroy_with_tmpl
    @subject = Subject.find params[:id]
    @subject_exams= Exam.find_by_subject_id(@subject.id)
    unless @subject.is_not_eligible_for_delete
      @subject.inactivate
    else
      @error_text = "#{t('cannot_delete_subjects')}"
    end
    flash[:notice] = "Subject Deleted successfully!"
    render :template => 'rank_subjects/destroy_with_tmpl'
  end

  def destroy_elective_group_with_tmpl
    @batch=Batch.find(params[:id])
    @elective_group=ElectiveGroup.find(params[:id2])
    @elective_group.inactivate
     flash[:notice] = "Group Deleted successfully!"
     render :template => 'rank_subjects/destroy_elective_group_with_tmpl'
  end

  def show_with_tmpl
    if params[:batch_id] == ''
      @subjects = []
      @elective_groups = []
    else
      @batch = Batch.find params[:batch_id]
      @subjects = @batch.normal_batch_subject
      @elective_groups = ElectiveGroup.find_all_by_batch_id(params[:batch_id], :conditions =>{:is_deleted=>false}, :include => :subjects)
    end
    respond_to do |format|
      format.js { render :template => 'rank_subjects/show_with_tmpl' }
    end
  end

  end
end