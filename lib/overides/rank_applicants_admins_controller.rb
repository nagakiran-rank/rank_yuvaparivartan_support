module RankYuvaparivartanSupport
  module RankApplicantsAdminsController

def self.included (base)
      base.instance_eval do
        alias_method_chain :applicants, :tmpl
      end
    end

    def applicants_with_tmpl
    search_by =""
    if params[:search].present?
      search_by=params[:search]
    end
    @sort_order=""
    if params[:sort_order].present?
      @sort_order=params[:sort_order]
    end
    @results=Applicant.search_by_order(params[:id], @sort_order, search_by)
    if @sort_order==""
      @results = @results.sort_by { |u1| [u1.status,u1.created_at.to_date] }.reverse if @results.present?
    end
    @applicants = @results.paginate :per_page=>10,:page => params[:page]
    @registration_course = RegistrationCourse.find(params[:id])
    render :template => 'rank_applicants_admins/applicants_with_tmpl'
  end

  end
end