class StudentLock  < Struct.new(:batch_id,:school_id)
	def perform
		MultiSchool.current_school = School.find(school_id)
		@batch = Batch.find(batch_id)
		@students = @batch.students
		@students.each do |s|
			 @amount = FinanceTransaction.find_all_by_payee_id(s.id)
			 @paid = @amount.map{|e| e.amount}.inject(0){|sum,x| sum + x }
			if @paid >= @batch.course.fee_structure
			else
				s.update_attribute(:student_lock , true)
			end
		end
	end
end