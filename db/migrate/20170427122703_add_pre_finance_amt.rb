class AddPreFinanceAmt < ActiveRecord::Migration
  def self.up
  	add_column :finance_transactions, :pre_finance_amt, :boolean ,:default => false
  end

  def self.down
  end
end
