menu_link_present = MenuLink rescue false
unless menu_link_present == false
  administration_category = MenuLinkCategory.find_by_name("administration")
  MenuLink.create(:name=>'bank',:target_controller=>'yuva_parivarthan_banks',:target_action=>'index',:higher_link_id=>nil,:icon_class=>'google-docs-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>administration_category.id) unless MenuLink.exists?(:name=>'bank')
end

Privilege.find_or_create_by_name :name => "BankModule",:description => "bank_module_privilege"
if Privilege.column_names.include?("privilege_tag_id")
  Privilege.find_by_name('BankModule').update_attributes(:privilege_tag_id=>PrivilegeTag.find_by_name_tag('finance_control').id, :priority=>330 )
end


Privilege.find_or_create_by_name :name => "PreStudent",:description => "pre_student_privilege"
if Privilege.column_names.include?("privilege_tag_id")
  Privilege.find_by_name('PreStudent').update_attributes(:privilege_tag_id=>PrivilegeTag.find_by_name_tag('student_management').id, :priority=>331 )
end


Privilege.find_or_create_by_name :name => "AdvanceFeeCollection",:description => "advance_fee_collection_privilege"
if Privilege.column_names.include?("privilege_tag_id")
  Privilege.find_by_name('AdvanceFeeCollection').update_attributes(:privilege_tag_id=>PrivilegeTag.find_by_name_tag('finance_control').id, :priority=>332 )
end



# menu_link_present = MenuLink rescue false
#  unless menu_link_present == false
 	
#  	MenuLinkCategory.create(:name=>"aau_registration",:allowed_roles=>[:student,:admin],:origin_name=>"fedena_app_frame") unless MenuLinkCategory.exists?(:name=>"aau_registration")
#  	registration = MenuLinkCategory.find_by_name("aau_registration")
#  	MenuLink.create(:name=>'student_registration',:target_controller=>'students_aau_section_courses',:target_action=>'students_subjects',:higher_link_id=>nil,:icon_class=>'student-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>registration.id) unless MenuLink.exists?(:name=>'student_registration')
#  	MenuLink.create(:name=>'registration_approval',:target_controller=>'students_aau_section_courses',:target_action=>'registration_details',:higher_link_id=>nil,:icon_class=>'settings-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>registration.id) unless MenuLink.exists?(:name=>'registration_approval')
#  	administration_category = MenuLinkCategory.find_by_name("administration")
#  	MenuLink.create(:name=>'aau_students',:target_controller=>'aau_student_configurations',:target_action=>'index',:higher_link_id=>nil,:icon_class=>'google-docs-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>administration_category.id) unless MenuLink.exists?(:name=>'aau_students')
#  	MenuLink.create(:name=>'theses_home',:target_controller=>'theses_home',:target_action=>'index',:higher_link_id=>nil,:icon_class=>'google-docs-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>administration_category.id) unless MenuLink.exists?(:name=>'theses_home')
#   	higher_link_students=MenuLink.find_by_name_and_higher_link_id('aau_students',nil)
#  	higher_link=MenuLink.find_by_name_and_higher_link_id('settings',nil)

#  	MenuLink.create(:name=>'aau_settings',:target_controller=>'aau_home',:target_action=>'index',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>administration_category.id) unless MenuLink.exists?(:name=>'aau_settings')
#  	MenuLink.create(:name=>'student_suspension_status',:target_controller=>'aau_student_suspensions',:target_action=>'index',:higher_link_id=>higher_link_students.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>administration_category.id) unless MenuLink.exists?(:name=>'student_suspension_status')
#  	MenuLink.create(:name=>'aau_student_status',:target_controller=>'aau_student_statuses',:target_action=>'index',:higher_link_id=>higher_link_students.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>administration_category.id) unless MenuLink.exists?(:name=>'aau_student_status')
 	


#  end