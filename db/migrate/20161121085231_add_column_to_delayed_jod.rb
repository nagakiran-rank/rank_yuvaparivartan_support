class AddColumnToDelayedJod < ActiveRecord::Migration
  def self.up
  	add_column :delayed_jobs, :batch_id, :integer
  end

  def self.down
  end
end
