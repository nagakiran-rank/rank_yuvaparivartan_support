class BankDepositYuvaBank < ActiveRecord::Base
	belongs_to :bank_deposit_finance_transactions, :foreign_key => 'bank_deposit_yuvaparivarthan_banks_id'
	belongs_to :yuvaparivarthan_bank, :foreign_key => 'bank_id'
	has_attached_file :upload_document,
	:url => "/system/:class/:attachment/:id/:style/:basename.:extension",
	:path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
	validates_attachment_presence :upload_document

	# VALID_TYPES = ['application/pdf', 'image/jpeg', 'image/jpg', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ]
	
	validates_attachment_content_type :upload_document, :content_type => ['image/jpeg','image/jpg', 'image/png']
	# :message=>'Image can only be pdf, jpeg, mp4',:if=> Proc.new { |d| !d.upload_document_file_name.blank? }
	
	validates_attachment_size :upload_document, :less_than => 2097152
	validates_presence_of :date_of_deposit
end