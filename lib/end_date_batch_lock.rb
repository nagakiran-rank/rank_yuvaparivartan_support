class EndDateBatchLock  < Struct.new(:id,:school_id)
	def perform
		MultiSchool.current_school = School.find(school_id)
		batch = Batch.find_by_id(id)
		batch.update_attribute(:end_date_lock , true)
	end
end