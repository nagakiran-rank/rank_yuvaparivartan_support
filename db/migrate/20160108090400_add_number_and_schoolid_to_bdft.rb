class AddNumberAndSchoolidToBdft < ActiveRecord::Migration
   def self.up
  	add_column :bank_deposit_finance_transactions, :bank_deposit_finance_transaction_number, :string
  	add_column :bank_deposit_finance_transactions, :school_id, :integer
  end

  def self.down
  	remove_column :bank_deposit_finance_transactions, :bank_deposit_finance_transaction_number
  	remove_column :bank_deposit_finance_transactions, :school_id
  end
end
