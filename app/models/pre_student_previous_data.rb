class PreStudentPreviousData < ActiveRecord::Base
	belongs_to :pre_student_registration
	validates_presence_of :last_attended_school
end
