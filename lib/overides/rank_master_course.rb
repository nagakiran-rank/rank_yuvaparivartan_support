module RankYuvaparivartanSupport
  module RankMasterCourse

    def self.included (base)
      base.instance_eval do
        alias_method_chain :update_course_table, :tmpl
      end
    end
		def update_course_table_with_tmpl
      sql = "update courses set course_name='#{self.course_name}', code='#{self.code}', section_name='#{self.section_name}', grading_type='#{self.grading_type}', status='#{self.status}', fee_structure='#{self.fee_structure}' where master_course_id = #{self.id}"
      ActiveRecord::Base.connection.execute(sql)
		end
	end
end