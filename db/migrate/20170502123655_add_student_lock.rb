class AddStudentLock < ActiveRecord::Migration
  def self.up
  	add_column :students, :student_lock, :boolean ,:default => false
  end

  def self.down
  end
end
