module RankYuvaparivartanSupport
  module RankAttendancesController
  	def self.included (base)
      base.instance_eval do
        alias_method_chain :index, :tmpl
        alias_method_chain :daily_register, :tmpl
      end
    end

  	def index_with_tmpl
	    @config = Configuration.find_by_config_key('StudentAttendanceType')
	    @date_today = @local_tzone_time.to_date
	    if current_user.admin?
	      @batches = Batch.active
	    elsif @current_user.privileges.map{|p| p.name}.include?('StudentAttendanceRegister')
	      @batches = Batch.active
	    elsif @current_user.employee?
	      if @config.config_value == 'Daily'
	        @batches = @current_user.employee_record.batches
	      else
	        @batches = @current_user.employee_record.subjects.collect{|b| b.batch}
	        @batches += TimetableSwap.find_all_by_employee_id(@current_user.employee_record.try(:id)).map(&:subject).flatten.compact.map(&:batch)
	        @batches = @batches.uniq unless @batches.empty?
	      end
	    end
	    render :template => 'rank_attendances/index_with_tmpl'
	end

	def daily_register_with_tmpl
	    @batch = Batch.find_by_id(params[:batch_id])
	    @timetable = TimetableEntry.find(:all, :conditions => {:batch_id => @batch.try(:id)})
	    if(@timetable.nil? or @batch.nil?)
	      render :update do |page|
	        page.replace_html "register", :partial => "no_timetable"
	        page.hide "loader"
	      end
	      return
	    end
	    @today = params[:next].present? ? params[:next].to_date : @local_tzone_time.to_date
	    @students = @batch.students.by_first_name.with_full_name_admission_no
	    @leaves = Hash.new
	    attendances = Attendance.by_month_and_batch(@today,params[:batch_id]).group_by(&:student_id)
	    @students.each do |student|
	      @leaves[student.id] = Hash.new(false)
	      unless attendances[student.id].nil?
	        attendances[student.id].each do |attendance|
	          @leaves[student.id]["#{attendance.month_date}"] = attendance.id
	        end
	      end
	    end
	    #    @dates=((@batch.end_date.to_date > @today.end_of_month) ? (@today.beginning_of_month..@today.end_of_month) : (@today.beginning_of_month..@batch.end_date.to_date))
	    @dates=@batch.total_days(@today)
	    @end_date_lock = @batch.end_date_lock
	    @admin_user = @current_user.admin?
	    @working_dates=@batch.working_days(@today)
	    @holidays = []
	    @translated=Hash.new
	    @translated['name']=t('name')
	    @translated['student']=t('student_text')
	    @translated['rapid_attendance']=t('rapid_attendance')
	    @translated['daily_quick_attendance_explanation']=t('daily_quick_attendance_explanation')
	    @translated['attendance_before_the_date_of_admission_is_invalid']=t('attendance_before_the_date_of_admission_is_invalid')
	    (0..6).each do |i|
	      @translated[Date::ABBR_DAYNAMES[i].to_s]=t(Date::ABBR_DAYNAMES[i].downcase)
	    end
	    (1..12).each do |i|
	      @translated[Date::MONTHNAMES[i].to_s]=t(Date::MONTHNAMES[i].downcase)
	    end
	    respond_to do |fmt|
	      fmt.json {render :json=>{'leaves'=>@leaves,'students'=>@students,'dates'=>@dates,'holidays'=>@holidays,'batch'=>@batch,'today'=>@today, 'translated'=>@translated,'working_dates'=>@working_dates , 'end_date_lock' => @end_date_lock,'admin_user' => @admin_user}}
	      #      format.js { render :action => 'show' }
	    end
	end

  end
end