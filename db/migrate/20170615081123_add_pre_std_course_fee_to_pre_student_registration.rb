class AddPreStdCourseFeeToPreStudentRegistration < ActiveRecord::Migration
  def self.up
  	add_column :pre_student_registrations, :pre_std_course_fee, :integer
  end

  def self.down
  end
end
