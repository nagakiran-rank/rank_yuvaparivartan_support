class AddDocumentUploadColumne < ActiveRecord::Migration
  def self.up
  	add_column :writeoff_details, :document_upload_file_name, :string
  	add_column :writeoff_details, :document_upload_content_type, :string
  	add_column :writeoff_details,	:document_upload_file_size, :integer
  end

  def self.down
  end
end
