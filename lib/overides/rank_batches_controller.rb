module RankYuvaparivartanSupport
	module RankBatchesController

		def self.included (base)
	      base.instance_eval do
	        alias_method_chain :edit, :tmpl
	        alias_method_chain :assign_tutor, :tmpl	
	        alias_method_chain :update_employees, :tmpl	
	        alias_method_chain :destroy, :tmpl	
	        # alias_method_chain :update_employees, :tmpl	
	        alias_method_chain :new, :tmpl
	        alias_method_chain :assign_employee, :tmpl
	        alias_method_chain :create, :tmpl
	        alias_method_chain :show, :tmpl
	        alias_method_chain :update, :tmpl
	      end
	    end

	    def new_with_tmpl
		  @batch = @course.batches.build
		  @verticals = YuvaVertical.all
		  render :template => 'rank_batches/new_with_tmpl'
		end

		def create_with_tmpl
			puts "------create_with_tmpl------"
			@start_time = params[:start]["time(1i)"] + "-"+params[:start]["time(2i)"] + "-"+params[:start]["time(3i)"] + " " +params[:start]["time(4i)"] + ":"+params[:start]["time(5i)"]
			@end_time = params[:end]["time(1i)"] + "-"+params[:end]["time(2i)"] + "-"+params[:end]["time(3i)"] + " " +params[:end]["time(4i)"] + ":"+params[:end]["time(5i)"]
			params[:batch][:name] = params[:batch][:name].upcase
			@all_batches = Course.find(params[:course_id]).batches.map{|p| p.name}
		    @batch = @course.batches.build(params[:batch])
		    @batch.start_time = @start_time
		    @batch.end_time=@end_time
		    @verticals = YuvaVertical.all
		    if @batch.name.count(" ") > 0
		    	@error = true
		    	@batch.errors.add_to_base("Batch name should not contains any spaces")
		    	#render :action => 'new_with_tmpl' ,:course_id=>params[:course_id]
		    	render :template => 'rank_batches/new_with_tmpl'
		    elsif @all_batches.include? @batch.name
		    	@error = true
		    	@batch.errors.add_to_base("Batch name already exists!")
		    	#render :action => 'new_with_tmpl' ,:course_id=>params[:course_id]
		    	render :template => 'rank_batches/new_with_tmpl'
		    elsif @batch.save
		    	logger.debug "id: #{@batch.inspect}---create--"
		      flash[:notice] = "#{t('flash1')}"
		      id = @batch.id
		      # recipient_ids = []
		       student_lock_notification = @batch.start_date + 35.days
					 std_lock = @batch.start_date + 50.days
					 recipient_ids = [current_user.id] + User.find_all_by_admin(true).map{|p| p.id}
					 start_lock_date = @batch.start_date + 25.days
					 start_lock_message = @batch.start_date + 23.days
					 end_lock_date = @batch.end_date + 15.days
					 end_lock_message = @batch.end_date

					 @delayed_student_lock_notification = Delayed::Job.enqueue(StudentNotificationLock.new(@batch.id,@batch.school_id) , 0, student_lock_notification)
		      @delayed_student_lock_notification.update_attributes(:batch_id => @batch.id)
		      logger.debug "#{@delayed_student_lock_notification.inspect}----create-BATCH--C"
	# @delayed_student_lock = Delayed::Job.enqueue(StudentLock.new(@batch.id,@batch.school_id) , 0, std_lock)
	# 	      @delayed_student_lock.update_attributes(:batch_id => @batch.id)
		      #logger.debug "#{@delayed_student_lock.inspect}--create--@delayed_student_lock-----"

		      @delayed_start_date = Delayed::Job.enqueue(StartDateBatchLock.new(@batch.id,@batch.school_id) , 0, start_lock_date)
		      @delayed_start_date.update_attributes(:batch_id => @batch.id)
		      @delayed_start_message = Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
		        :recipient_ids => recipient_ids,
		        :subject=>"#{@batch.full_name} will be locked in 25 days",
		        :body=>"The batch #{@batch.full_name} will get locked in 25 days . Please do the necessary changes" ),0, start_lock_message)
		      @delayed_start_message.update_attributes(:batch_id => @batch.id)
		      @delayed_end_date = Delayed::Job.enqueue(EndDateBatchLock.new(@batch.id,@batch.school_id) , 0,end_lock_date)
		      @delayed_end_date.update_attributes(:batch_id => @batch.id)
		      @delayed_end_message = Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
		        :recipient_ids => recipient_ids,
		        :subject=>"#{@batch.full_name} will be completely locked in 15 days",
		        :body=>"The batch #{@batch.full_name} will get completely locked in 15 days . Please do the necessary changes" ),0, end_lock_message)
		      @delayed_end_message.update_attributes(:batch_id => @batch.id)

		      @delayed_student_lock = Delayed::Job.enqueue(StudentLock.new(@batch.id,@batch.school_id) , 0, std_lock)
		      @delayed_student_lock.update_attributes(:batch_id => @batch.id)

		      # @delayed_student_lock_notification = Delayed::Job.enqueue(StudentNotificationLock.new(@batch.id,@batch.school_id) , 0, student_lock_notification)
		      # @delayed_student_lock_notification.update_attributes(:batch_id => @batch.id)
		      # puts "#{@delayed_student_lock_notification.inspect}----create-BATCH--C"

		      # @delayed_student_lock = Delayed::Job.enqueue(StudentLock.new(@batch.id,@batch.school_id) , 0,student_lock)
		      # @delayed_student_lock.update_attributes(:batch_id => @batch.id)
		      # puts "#{@delayed_student_lock.inspect}--create--@delayed_student_lock-----"

		   		# @students = @batch.students
					# @students.each do |s|
					# 	 @amount = FinanceTransaction.find_all_by_payee_id(s.id)
					# 	 @paid = @amount.map{|e| e.amount}.inject(0){|sum,x| sum + x }
					# 	unless @paid >= @batch.course.fee_structure
					# 		@delayed_student_lock.update_attribute(:student_id,s.id)
					# 		puts "AFTER---UPDATE"
					# 	end
					# end

		      unless params[:import_subjects].nil?
		        msg = []
		        msg << "<ol>"
		        course_id = @batch.course_id
		        @previous_batch = Batch.find(:first,:order=>'id desc', :conditions=>"batches.id < '#{@batch.id }' AND batches.is_deleted = 0 AND course_id = ' #{course_id }'",:joins=>"INNER JOIN subjects ON subjects.batch_id = batches.id  AND subjects.is_deleted = 0")
		        unless @previous_batch.blank?
		          subjects = Subject.find_all_by_batch_id(@previous_batch.id,:conditions=>'is_deleted=false')
		          subjects.each do |subject|
		            if subject.elective_group_id.nil?
		              Subject.create(:name=>subject.name,:code=>subject.code,:batch_id=>@batch.id,:no_exams=>subject.no_exams,
		                :max_weekly_classes=>subject.max_weekly_classes,:elective_group_id=>subject.elective_group_id,:credit_hours=>subject.credit_hours,:is_deleted=>false)
		            else
		              elect_group_exists = ElectiveGroup.find_by_name_and_batch_id(ElectiveGroup.find(subject.elective_group_id).name,@batch.id)
		              if elect_group_exists.nil?
		                elect_group = ElectiveGroup.create(:name=>ElectiveGroup.find(subject.elective_group_id).name,
		                  :batch_id=>@batch.id,:is_deleted=>false)
		                Subject.create(:name=>subject.name,:code=>subject.code,:batch_id=>@batch.id,:no_exams=>subject.no_exams,
		                  :max_weekly_classes=>subject.max_weekly_classes,:elective_group_id=>elect_group.id,:credit_hours=>subject.credit_hours,:is_deleted=>false)
		              else
		                Subject.create(:name=>subject.name,:code=>subject.code,:batch_id=>@batch.id,:no_exams=>subject.no_exams,
		                  :max_weekly_classes=>subject.max_weekly_classes,:elective_group_id=>elect_group_exists.id,:credit_hours=>subject.credit_hours,:is_deleted=>false)
		              end
		            end
		            msg << "<li>#{subject.name}</li>"
		          end
		          msg << "</ol>"
		        else
		          msg = nil
		          flash[:no_subject_error] = "#{t('flash7')}"
		        end
		      end
		      flash[:subject_import] = msg unless msg.nil?
		      err = ""
		      err1 = "<p style = 'font-size:16px'>#{t('following_pblm_occured_while_saving_the_batch')}</p>"
		      err1 += "<ul>"
		      unless params[:import_fees].nil?
		        fee_msg = []
		        course_id = @batch.course_id
		        @previous_batch = Batch.find(:first,:order=>'id desc', :conditions=>"batches.id < '#{@batch.id }' AND batches.is_deleted = 0 AND course_id = ' #{course_id }'",:joins=>"INNER JOIN category_batches ON category_batches.batch_id = batches.id  INNER JOIN finance_fee_categories on finance_fee_categories.id=category_batches.finance_fee_category_id AND finance_fee_categories.is_deleted = 0 AND is_master= 1")
		        unless @previous_batch.blank?
		          fee_msg << "<ol>"
		          categories = CategoryBatch.find_all_by_batch_id(@previous_batch.id)
		          categories.each do |c|
		            
		            particulars = FinanceFeeParticular.find(:all,:conditions=>"(receiver_type='Batch' or receiver_type='StudentCategory') and (batch_id=#{@previous_batch.id}) and (finance_fee_category_id=#{c.finance_fee_category_id})")
		            #particulars = c.finance_fee_category.fee_particulars.all(:conditions=>"receiver_type='Batch' or receiver_type='StudentCategory'")
		            particulars.reject!{|pt|pt.deleted_category}
		            fee_discounts = FeeDiscount.find(:all,:conditions=>"(receiver_type='Batch' or receiver_type='StudentCategory') and (batch_id=#{@previous_batch.id}) and (finance_fee_category_id=#{c.finance_fee_category_id})")
		            #category_discounts = StudentCategoryFeeDiscount.find_all_by_finance_fee_category_id(c.id)
		            unless particulars.blank? and fee_discounts.blank?
		              new_category = CategoryBatch.new(:batch_id=>@batch.id,:finance_fee_category_id=>c.finance_fee_category_id)
		              if new_category.save
		                fee_msg << "<li>#{c.finance_fee_category.name}</li>"
		                particulars.each do |p|
		                  receiver_id=p.receiver_type=='Batch' ? @batch.id : p.receiver_id
		                  new_particular = FinanceFeeParticular.new(:name=>p.name,:description=>p.description,:amount=>p.amount,\
		                      :batch_id=>@batch.id,:receiver_id=>receiver_id,:receiver_type=>p.receiver_type)
		                  new_particular.finance_fee_category_id = new_category.finance_fee_category_id
		                  unless new_particular.save
		                    err += "<li> #{t('particular')} #{p.name} #{t('import_failed')}.</li>"
		                  end
		                end
		                fee_discounts.each do |disc|
		                  discount_attributes = disc.attributes
		                  discount_attributes.delete "type"
		                  discount_attributes.delete "finance_fee_category_id"
		                  discount_attributes.delete "batch_id"
		                  discount_attributes['receiver_id']=@batch.id if disc.receiver_type=='Batch'
		                  discount_attributes["batch_id"]= @batch.id
		                  discount_attributes["finance_fee_category_id"]= new_category.finance_fee_category_id
		                  unless FeeDiscount.create(discount_attributes)
		                    err += "<li> #{t('discount ')} #{disc.name} #{t('import_failed')}.</li>"
		                  end
		                end
		                #                category_discounts.each do |disc|
		                #                  discount_attributes = disc.attributes
		                #                  discount_attributes.delete "type"
		                #                  discount_attributes.delete "finance_fee_category_id"
		                #                  discount_attributes["finance_fee_category_id"]= new_category.id
		                #                  unless StudentCategoryFeeDiscount.create(discount_attributes)
		                #                    err += "<li>  #{t(' discount ')} #{disc.name} #{t(' import_failed')}.</li><br/>"
		                #                  end
		                #                end
		              else

		                err += "<li>  #{t('category')} #{c.finance_fee_category.name}1 #{t('import_failed')}.</li>"
		              end
		            else

		              err += "<li>  #{t('category')} #{c.finance_fee_category.name}2 #{t('import_failed')}.</li>"

		            end
		          end
		          fee_msg << "</ol>"
		          @fee_import_error = false
		          flash[:fees_import_error] =nil
		        else
		          flash[:fees_import_error] =t('no_fee_import_message')
		          @fee_import_error = true
		        end
		      end
		      err2 = "</ul>"
		      flash[:warn_notice] =  err1 + err + err2 unless err.empty?
		      flash[:fees_import] =  fee_msg unless fee_msg.nil?
		      
		      redirect_to [@course, @batch]
		    else
		      @grade_types=[]
		      gpa = Configuration.find_by_config_key("GPA").config_value
		      if gpa == "1"
		        @grade_types << "GPA"
		      end
		      cwa = Configuration.find_by_config_key("CWA").config_value
		      if cwa == "1"
		        @grade_types << "CWA"
		      end
		      render :template => 'rank_batches/new_with_tmpl'
		    end
		end

		def edit_with_tmpl
			@verticals = YuvaVertical.all
			render :template => 'rank_batches/edit_with_tmpl'
	  end

	  def update_with_tmpl
	  	@start_time = params[:start]["time(1i)"] + "-"+params[:start]["time(2i)"] + "-"+params[:start]["time(3i)"] + " " +params[:start]["time(4i)"] + ":"+params[:start]["time(5i)"]
			@end_time = params[:end]["time(1i)"] + "-"+params[:end]["time(2i)"] + "-"+params[:end]["time(3i)"] + " " +params[:end]["time(4i)"] + ":"+params[:end]["time(5i)"]
			params[:batch][:name] = params[:batch][:name].upcase
			batch_name = params[:batch][:name]
			@all_batches = Course.find(params[:course_id]).batches.map{|p| p.name}
			@all_batches.delete(@batch.name)
			@verticals = YuvaVertical.all
			@batch.start_time = @start_time
	    @batch.end_time=@end_time
			if batch_name.count(" ") > 0
	    	@error = true
	    	@batch.errors.add_to_base("Batch name should not contains any spaces")
	    	render :template => 'rank_batches/edit_with_tmpl'
	    elsif @all_batches.include? @batch.name
	    	@error = true
	    	@batch.errors.add_to_base("Batch name already exists!")
	    	# render :action => 'new' ,:course_id=>params[:course_id]
	    	render :template => 'rank_batches/edit_with_tmpl'
	    else
	    	if @batch.start_date_lock == true || @batch.end_date_lock == true
		    	if params[:batch][:end_date].to_date == @batch.end_date
		    		start_lock_date = Date.today + 1.days
		    		end_lock_date = @batch.end_date + 1.days
		    		Delayed::Job.enqueue(StartDateBatchLock.new(@batch.id,@batch.school_id) , 0, start_lock_date)
		    		Delayed::Job.enqueue(EndDateBatchLock.new(@batch.id,@batch.school_id) , 0,end_lock_date)
		    		flash[:notice] = "the batch will be closed tomorrow"
		    		render :template => 'rank_batches/edit_with_tmpl'
		    	else
				    if @batch.update_attributes(params[:batch])
				      flash[:notice] = "#{t('flash2')}"
				      id = @batch.id
				      # recipient_ids = []
				      recipient_ids = [current_user.id] + User.find_all_by_admin(true).map{|p| p.id}
				      start_lock_date = Date.today + 1.days
				      start_lock_message = Date.today
				      end_lock_date = @batch.end_date + 15.days
				      end_lock_message = @batch.end_date
				      Delayed::Job.enqueue(StartDateBatchLock.new(@batch.id,@batch.school_id) , 0, start_lock_date)
				      # Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
				      #   :recipient_ids => recipient_ids,
				      #   :subject=>"#{@batch.full_name} will be locked in #{start_lock_date}",
				      #   :body=>"The batch #{@batch.full_name} will get locked in #{start_lock_date} . Please do the necessary changes" ),0, start_lock_message)
				      Delayed::Job.enqueue(EndDateBatchLock.new(@batch.id,@batch.school_id) , 0,end_lock_date)
				      # Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
				      #   :recipient_ids => recipient_ids,
				      #   :subject=>"#{@batch.full_name} will be completely locked on #{end_lock_date}",
				      #   :body=>"The batch #{@batch.full_name} will get completely locked in #{end_lock_date}. Please do the necessary changes" ),0, end_lock_message)
				      redirect_to [@course, @batch]
				    else
				      # render 'edit'
				      #flash[:notice] ="#{t('flash3')}"
				      #redirect_to  edit_course_batch_path(@course, @batch)
				      # render 'edit'
				      #flash[:notice] ="#{t('flash3')}"
				      #redirect_to  edit_course_batch_path(@course, @batch)
				      render :template => 'rank_batches/edit_with_tmpl'
				    end
				end
			else
				@delayed_job = Delayed::Job.find_all_by_batch_id(@batch.id)
				@delayed_job.each do |p|
					p.destroy
				end
				if @batch.update_attributes(params[:batch])
				    flash[:notice] = "#{t('flash2')}"
					id = @batch.id
					logger.debug "id: #{id.inspect}----update"
			      # recipient_ids = []
			      recipient_ids = [current_user.id] + User.find_all_by_admin(true).map{|p| p.id}
			      start_lock_date = @batch.start_date + 25.days
			      student_lock_notification = @batch.start_date + 35.days
					 	std_lock = @batch.start_date + 50.days
			      start_lock_message = @batch.start_date + 23.days
			      end_lock_date = @batch.end_date + 15.days
			      end_lock_message = @batch.end_date

			      @delayed_student_lock_notification = Delayed::Job.enqueue(StudentNotificationLock.new(@batch.id,@batch.school_id) , 0, student_lock_notification)
		      @delayed_student_lock_notification.update_attributes(:batch_id => @batch.id)
		      logger.debug "#{@delayed_student_lock_notification.inspect}----update-BATCH--C"

	# @delayed_student_lock = Delayed::Job.enqueue(StudentLock.new(@batch.id,@batch.school_id) , 0, std_lock)
	# 	      @delayed_student_lock.update_attributes(:batch_id => @batch.id)
	# 	      logger.debug "#{@delayed_student_lock.inspect}--update--@delayed_student_lock-----"

			      @delayed_start_date = Delayed::Job.enqueue(StartDateBatchLock.new(@batch.id,@batch.school_id) , 0, start_lock_date)
			      @delayed_start_date.update_attributes(:batch_id => @batch.id)
			      @delayed_start_message = Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
			        :recipient_ids => recipient_ids,
			        :subject=>"#{@batch.full_name} will be locked in 25 days",
			        :body=>"The batch #{@batch.full_name} will get locked in 25 days . Please do the necessary changes" ),0, start_lock_message)
			      @delayed_start_message.update_attributes(:batch_id => @batch.id)
			      @delayed_end_date = Delayed::Job.enqueue(EndDateBatchLock.new(@batch.id,@batch.school_id) , 0,end_lock_date)
			      @delayed_end_date.update_attributes(:batch_id => @batch.id)
			      @delayed_end_message = Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
			        :recipient_ids => recipient_ids,
			        :subject=>"#{@batch.full_name} will be completely locked in 15 days",
			        :body=>"The batch #{@batch.full_name} will get completely locked in 15 days . Please do the necessary changes" ),0, end_lock_message)
			      @delayed_end_message.update_attributes(:batch_id => @batch.id)

			      @delayed_student_lock = Delayed::Job.enqueue(StudentLock.new(@batch.id,@batch.school_id) , 0, std_lock)
		      	@delayed_student_lock.update_attributes(:batch_id => @batch.id)

			      # @delayed_student_lock_notification = Delayed::Job.enqueue(StudentNotificationLock.new(@batch.id,@batch.school_id) , 0, student_lock_notification)
			      # puts "#{@delayed_student_lock_notification.inspect}--update---BATCH--C"
			      # @delayed_student_lock_notification.update_attributes(:batch_id => @batch.id)

		      	# @delayed_student_lock = Delayed::Job.enqueue(StudentLock.new(@batch.id,@batch.school_id) , 0,student_lock)
		      	# puts "#{@delayed_student_lock.inspect}--update--@delayed_student_lock-----"
		      	# @delayed_student_lock.update_attributes(:batch_id => @batch.id)

					   		# @students = @batch.students
								# @students.each do |s|
								# 	 @amount = FinanceTransaction.find_all_by_payee_id(s.id)
								# 	 @paid = @amount.map{|e| e.amount}.inject(0){|sum,x| sum + x }
								# 	unless @paid >= @batch.course.fee_structure
								# 		@delayed_student_lock.update_attribute(:student_id,s.id)
								# 		puts "AFTER---UPDATE"
								# 	end
								# end
			      redirect_to [@course, @batch]
			  else
			  	render :template => 'rank_batches/edit_with_tmpl'
			  end
			end
			end
		end

	  def destroy_with_tmpl
	    if @batch.students.empty? and @batch.subjects.empty?
	      @batch.inactivate
	      flash[:notice] = "#{t('flash4')}"
	      redirect_to @course
	    else
	      flash[:warn_notice] = "<p>#{t('batches.flash5')}</p>" unless @batch.students.empty?
	      flash[:warn_notice] = "<p>#{t('batches.flash6')}</p>" unless @batch.subjects.empty?
	      redirect_to [@course, @batch]
	    end
		end

		def assign_tutor_with_tmpl
	    @batch = Batch.find_by_id(params[:id])
	    if @batch.nil?
	      page_not_found
	    else
	      @assigned_employee=@batch.employees
	      @departments = EmployeeDepartment.ordered
	    end
	    render :template => 'rank_batches/assign_tutor_with_tmpl'
		end

		def update_employees_with_tmpl
		    @employees = Employee.find_all_by_employee_department_id(params[:department_id]).sort_by{|e| e.full_name.downcase}
		    @batch = Batch.find_by_id(params[:batch_id])
		    @assigned_employee=@batch.employees
		    render :update do |page|
		      page.replace_html 'employee-list', :partial => 'rank_batches/employee_list_with_tmpl'
		    end
		end

		def assign_employee_with_tmpl
	    @batch = Batch.find_by_id(params[:batch_id])
	    @employees = Employee.find_all_by_employee_department_id(params[:department_id]).sort_by{|e| e.full_name.downcase}
	    if @batch.employees.present? && (@batch.employees.count == 1)
	    	@assigned_employee=@batch.employees
	    	render :update do |page|
	    		page.replace_html 'employee-list', :partial => 'rank_batches/employee_list_with_tmpl'
		      page.replace_html 'flash', :text=>"<p class='flash-msg'>#{"A tutor is already assigned to this batch . Please remove the current tutor to assign another tutor"}</p>"
		    end
	    else
	    	@batch.employee_ids=@batch.employee_ids << params[:id]
	   		@assigned_employee=@batch.employees
		    render :update do |page|
		      page.replace_html 'employee-list', :partial => 'rank_batches/employee_list_with_tmpl'
		      page.replace_html 'tutor-list', :partial => 'assigned_tutor_list'
		      page.replace_html 'flash', :text=>"<p class='flash-msg'>#{t('tutor_assigned_successfully')}</p>"
		    end
		  end
	  end

	  def show_with_tmpl
	    @students = @batch.students
	    render :template => 'rank_batches/show_with_tmpl'
	  end

	  def show_locked_students
      @students = Student.find_all_by_batch_id_and_student_lock(params[:id],true)
      render :template => 'rank_batches/show_locked_students'
    end

    def locked_students
    	@students = Student.find_all_by_batch_id_and_student_lock(params[:id],true)
    	render :template => 'rank_batches/locked_students'
    end

	end
end