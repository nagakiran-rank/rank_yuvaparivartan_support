module RankYuvaparivartanSupport
  module RankHostelFeeController

  	def self.included (base)
      base.instance_eval do
        alias_method_chain :pay_defaulters_fees, :tmpl
        alias_method_chain :student_profile_fee_details, :tmpl
        alias_method_chain :hostel_fee_collection_pay, :tmpl
      end
    end

    def pay_defaulters_fees_with_tmpl
    category_id = FinanceTransactionCategory.find_by_name("Hostel").id
    @pay = HostelFee.find params[:id]
    transaction = FinanceTransaction.new
    transaction.title = @pay.hostel_fee_collection.name
    transaction.category_id = category_id
    transaction.finance = @pay
    transaction.amount = @pay.rent
    transaction.payee = @pay.student
    transaction.transaction_date = Date.today.to_date
    transaction.balance = transaction.amount
#    if transaction.save
#      @pay.update_attribute(:finance_transaction_id, transaction.id)
#    end
    @hostel_fee = HostelFee.find_all_by_hostel_fee_collection_id(@pay.hostel_fee_collection_id, :conditions => ["finance_transaction_id is null"])
    @hostel_fee.reject! { |x| x.student.nil? }
    render :update do |page|
      page.replace_html "hostel_fee_collection_details", :partial => 'hostel_fee_collection_defaulters_details'
      page.replace_html "pay_msg", :text => "<p class='flash-msg'> #{t('fees_paid')} </p>"
    end
  end

  def hostel_fee_collection_pay_with_tmpl
    @transaction = HostelFee.find(params[:fees][:hostel_fee_id])
    @date=@transaction.hostel_fee_collection
    @student = Student.find(params[:student])
    @batch=@student.batch
    @students=Student.find(:all,:joins=>:hostel_fees, :conditions => "hostel_fees.hostel_fee_collection_id='#{@transaction.hostel_fee_collection_id}' and hostel_fees.is_active=1 and students.batch_id='#{@student.batch_id}'")
    @prev_student=@students.select{|student| student.id<@student.id}.last||@students.last
    @next_student=@students.select{|student| student.id>@student.id}.first||@students.first
    unless params[:fees][:payment_mode].blank?
      transaction = FinanceTransaction.new
      transaction.title = @transaction.hostel_fee_collection.name
      transaction.category_id = FinanceTransactionCategory.find_by_name('Hostel').id
      transaction.finance = @transaction
      transaction.amount = @transaction.rent
      transaction.amount += params[:fees][:fine].to_f unless params[:fees][:fine].blank?
      transaction.fine_amount = params[:fees][:fine].to_f unless params[:fees][:fine].blank?
      transaction.fine_included = true unless params[:fees][:fine].blank?
      transaction.transaction_date = Date.today
      transaction.payment_mode = params[:fees][:payment_mode]
      transaction.payment_note = params[:fees][:payment_note]
      transaction.payee = @transaction.student
      transaction.balance = transaction.amount
      if transaction.save
      	 #transaction.update_attributes(:balance => transaction.amount)
#        @transaction.update_attributes(:finance_transaction_id => transaction.id)
        flash[:notice]="#{t('fee_paid')}"
        flash[:warn_notice]=nil
      end
      @finance_transaction = @transaction.finance_transaction     
      @fine = params[:fees][:fine] if params[:fees].present?
    else
      flash[:notice]=nil
      flash[:warn_notice]="#{t('select_one_payment_mode')}"
    end
    render :update do |page|
      page.replace_html 'fees_details', :partial => 'fees_details'
    end
  end

  def student_profile_fee_details_with_tmpl
    @student=Student.find(params[:id])
    @fee= HostelFee.find_by_hostel_fee_collection_id_and_student_id(params[:id2], params[:id])
    @amount = @fee.rent
    @paid_fees=@fee.finance_transaction unless @fee.finance_transaction_id.blank?
    @fee_collection = HostelFeeCollection.find(params[:id2])
    if FedenaPlugin.can_access_plugin?("fedena_pay")
      if ((PaymentConfiguration.config_value("enabled_fees").present? and PaymentConfiguration.config_value("enabled_fees").include? "Hostel Fee"))
        @active_gateway = PaymentConfiguration.config_value("fedena_gateway")
        #        if @active_gateway == "Paypal"
        #          @merchant_id = PaymentConfiguration.config_value("paypal_id")
        #          @merchant ||= String.new
        #          @certificate = PaymentConfiguration.config_value("paypal_certificate")
        #          @certificate ||= String.new
        #        elsif @active_gateway == "Authorize.net"
        #          @merchant_id = PaymentConfiguration.config_value("authorize_net_merchant_id")
        #          @merchant_id ||= String.new
        #          @certificate = PaymentConfiguration.config_value("authorize_net_transaction_password")
        #          @certificate ||= String.new
        #        elsif @active_gateway == "Webpay"
        #          @merchant_id = PaymentConfiguration.config_value("webpay_merchant_id")
        #          @merchant_id ||= String.new
        #          @product_id = PaymentConfiguration.config_value("webpay_product_id")
        #          @product_id ||= String.new
        #          @item_id = PaymentConfiguration.config_value("webpay_item_id")
        #          @item_id ||= String.new
        @custom_gateway = CustomGateway.find(@active_gateway)
#        @custom_gateway.gateway_parameters[:config_fields].each_pair do|k,v|
#          instance_variable_set("@"+k,v)
#        end
      end
      hostname = "#{request.protocol}#{request.host_with_port}"
      if params[:create_transaction].present?
        gateway_response = Hash.new
        #        if @active_gateway == "Paypal"
        #          gateway_response = {
        #            :amount => params[:amt],
        #            :status => params[:st],
        #            :transaction_id => params[:tx]
        #          }
        #        elsif @active_gateway == "Authorize.net"
        #          gateway_response = {
        #            :x_response_code => params[:x_response_code],
        #            :x_response_reason_code => params[:x_response_reason_code],
        #            :x_response_reason_text => params[:x_response_reason_text],
        #            :x_avs_code => params[:x_avs_code],
        #            :x_auth_code => params[:x_auth_code],
        #            :x_trans_id => params[:x_trans_id],
        #            :x_method => params[:x_method],
        #            :x_card_type => params[:x_card_type],
        #            :x_account_number => params[:x_account_number],
        #            :x_first_name => params[:x_first_name],
        #            :x_last_name => params[:x_last_name],
        #            :x_company => params[:x_company],
        #            :x_address => params[:x_address],
        #            :x_city => params[:x_city],
        #            :x_state => params[:x_state],
        #            :x_zip => params[:x_zip],
        #            :x_country => params[:x_country],
        #            :x_phone => params[:x_phone],
        #            :x_fax => params[:x_fax],
        #            :x_invoice_num => params[:x_invoice_num],
        #            :x_description => params[:x_description],
        #            :x_type => params[:x_type],
        #            :x_cust_id => params[:x_cust_id],
        #            :x_ship_to_first_name => params[:x_ship_to_first_name],
        #            :x_ship_to_last_name => params[:x_ship_to_last_name],
        #            :x_ship_to_company => params[:x_ship_to_company],
        #            :x_ship_to_address => params[:x_ship_to_address],
        #            :x_ship_to_city => params[:x_ship_to_city],
        #            :x_ship_to_zip => params[:x_ship_to_zip],
        #            :x_ship_to_country => params[:x_ship_to_country],
        #            :x_amount => params[:x_amount],
        #            :x_tax => params[:x_tax],
        #            :x_duty => params[:x_duty],
        #            :x_freight => params[:x_freight],
        #            :x_tax_exempt => params[:x_tax_exempt],
        #            :x_po_num => params[:x_po_num],
        #            :x_cvv2_resp_code => params[:x_cvv2_resp_code],
        #            :x_MD5_hash => params[:x_MD5_hash],
        #            :x_cavv_response => params[:x_cavv_response],
        #            :x_method_available => params[:x_method_available],
        #          }
        #        elsif @active_gateway == "Webpay"
        #          # url = "https://stageserv.interswitchng.com/test_paydirect/api/v1/gettransaction.json"
        #          # url += "?productid=#{@product_id}"
        #          # url += "&transactionreference=#{params[:txnref]}"
        #          # url += "&amount=#{(('%.02f' % @amount).to_f * 100).to_i}"
        #          if File.exists?("#{Rails.root}/vendor/plugins/fedena_pay/config/online_payment_url.yml")
        #            response_urls = YAML.load_file(File.join(Rails.root, "vendor/plugins/fedena_pay/config/", "online_payment_url.yml"))
        #          end
        #          txnref = params[:txnref] || params[:txnRef] || ""
        #          response_url = response_urls.nil? ? nil : eval(response_urls["webpay_response_url"].to_s)
        #          response_url ||= "https://stageserv.interswitchng.com/test_paydirect/api/v1/gettransaction.json"
        #          response_url += "?productid=#{@product_id.strip}"
        #          response_url += "&transactionreference=#{txnref}"
        #          response_url += "&amount=#{(('%.02f' % @amount).to_f * 100).to_i}"
        #          uri = URI.parse(response_url)
        #          http = Net::HTTP.new(uri.host, uri.port)
        #          http.use_ssl = true
        #          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        #          request = Net::HTTP::Get.new(uri.request_uri, {'Hash' => Digest::SHA512.hexdigest(@product_id.strip + txnref.to_s + @merchant_id.strip)})
        #          response = http.request(request)
        #          response_str = JSON.parse response.body
        #          gateway_response = {
        #            :split_accounts => response_str["SplitAccounts"],
        #            :merchant_reference => response_str["MerchantReference"],
        #            :response_code => response_str["ResponseCode"],
        #            :lead_bank_name => response_str["LeadBankName"],
        #            :lead_bank_cbn_code => response_str["LeadBankCbnCode"],
        #            :amount => response_str["Amount"].to_f/ 100,
        #            :card_number => response_str["CardNumber"],
        #            :response_description => response_str["ResponseDescription"],
        #            :transaction_date => response_str["TransactionDate"],
        #            :retrieval_reference_number => response_str["RetrievalReferenceNumber"],
        #            :payment_reference => response_str["PaymentReference"]
        #          }
        #        end
        if @custom_gateway.present?
          @custom_gateway.gateway_parameters[:response_parameters].each_pair do|k,v|
            unless k.to_s == "success_code"
              gateway_response[k.to_sym] = params[v.to_sym]
            end
          end
        end
        @gateway_status = false
        #        if @active_gateway == "Paypal"
        #          @gateway_status = true if params[:st] == "Completed"
        #        elsif @active_gateway == "Authorize.net"
        #          @gateway_status = true if gateway_response[:x_response_reason_code] == "1"
        #        elsif @active_gateway == "Webpay"
        #          @gateway_status = true if gateway_response[:response_code] == "00"
        #        end
        if @custom_gateway.present?
          success_code = @custom_gateway.gateway_parameters[:response_parameters][:success_code]
          @gateway_status = true if gateway_response[:transaction_status] == success_code
        end
        payment = Payment.new(:payee => @student, :payment => @fee, :gateway_response => gateway_response, :status => @gateway_status, :amount => gateway_response[:amount].to_f, :gateway => @active_gateway)
        payment.fee_collection = @fee_collection

        if payment.save and @fee.finance_transaction_id.nil?
          #amount_from_gateway = 0
          #          if @active_gateway == "Paypal"
          #            amount_from_gateway = params[:amt]
          #          elsif @active_gateway == "Authorize.net"
          #            amount_from_gateway = params[:x_amount]
          #          elsif @active_gateway == "Webpay"
          #            amount_from_gateway = gateway_response[:amount]
          #          end
          amount_from_gateway = gateway_response[:amount]
          if amount_from_gateway.to_f > 0.0 and payment.status
            transaction = FinanceTransaction.new
            transaction.title = @fee.hostel_fee_collection.name
            transaction.category_id = FinanceTransactionCategory.find_by_name('Hostel').id
            transaction.finance = @fee
            transaction.amount = amount_from_gateway.to_f
            transaction.transaction_date = Date.today
            transaction.payment_mode = "Online payment"
            transaction.payee = @fee.student
            transaction.balance = transaction.amount
            if transaction.save
              #transaction.update_attributes(:balance => transaction.amount)
#              @fee.update_attributes(:finance_transaction_id => transaction.id)
              payment.update_attributes(:finance_transaction_id => transaction.id)
              #              online_transaction_id = payment.gateway_response[:transaction_id]
              #              online_transaction_id ||= payment.gateway_response[:x_trans_id]
              #              online_transaction_id ||= payment.gateway_response[:payment_reference]
              online_transaction_id = payment.gateway_response[:transaction_reference]
              @paid_fees=@fee.finance_transaction unless @fee.finance_transaction_id.blank?
            end
            if @gateway_status
              status = Payment.payment_status_mapping[:success]
              payment.update_attributes(:status_description => status)
              flash[:notice] = "#{t('payment_success')} <br>  #{t('payment_reference')} : #{online_transaction_id}"
              if current_user.parent?
                user = current_user
              else
                user = @student.user
              end
              if @student.is_email_enabled && user.email.present?
                begin
                  Delayed::Job.enqueue(OnlinePayment::PaymentMail.new(payment.fee_collection.name, user.email, user.full_name, @custom_gateway.name, payment.amount, online_transaction_id, payment.gateway_response, user.school_details, hostname))
                rescue Exception => e
                  return
                end
              end
            else
              status = Payment.payment_status_mapping[:failed]
              payment.update_attributes(:status_description => status)
              flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{payment.gateway_response[:transaction_reference] || 'N/A'}"
            end
          else
            status = Payment.payment_status_mapping[:failed]
            payment.update_attributes(:status_description => status)
            flash[:notice] = "#{t('payment_failed')} <br> #{t('reason')} : #{payment.gateway_response[:reason_code] || 'N/A'} <br> #{t('transaction_id')} : #{payment.gateway_response[:transaction_reference] || 'N/A'}"
          end
        else
          flash[:notice] = "#{t('already_paid')}"
        end
        redirect_to :controller => 'hostel_fee', :action => 'student_profile_fee_details', :id => params[:id], :id2 => params[:id2]
      end
    end
  end
  end
end