class YuvaState < ActiveRecord::Base
	belongs_to :country
	has_many :yuva_cities, :foreign_key => 'state_id'
end
