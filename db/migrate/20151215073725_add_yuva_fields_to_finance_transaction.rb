class AddYuvaFieldsToFinanceTransaction < ActiveRecord::Migration
  def self.up
    add_column :finance_transactions, :fee_deposit, :boolean ,:default => false
    add_column :finance_transactions, :balance , :decimal ,:precision => 15, :scale => 2
    add_column :finance_transactions, :deposited_date , :date
  end

  def self.down
    remove_column :finance_transactions, :fee_deposit
  end
end
