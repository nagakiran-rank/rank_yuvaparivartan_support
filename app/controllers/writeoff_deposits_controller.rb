class WriteoffDepositsController < ApplicationController
	require 'will_paginate/collection'

	def writeoff_index
		last_transaction = WriteoffDeposit.last
    last_bdft_no = last_transaction.writeoff_deposit_number unless last_transaction.nil?
    unless last_bdft_no.nil?
     bdft_number = last_bdft_no.next
    else
      bdft_number = "1"
    end
		@write_off_details = WriteoffDetail.new
		@write_off_details.writeoff_amount = params[:write_off_amount]
		@write_off_details.reason_for_writeoff = params[:reason_for_write_off]
		@write_off_details.document_upload = params[:document_upload]
		@write_off_details.vertical_id = params[:type_of_vertical]
		@write_off_details.user = params[:writeoff_deposited_by]
		@write_off_details.writeoff_date = params[:writeoff_date]
		if @write_off_details.save
			if params[:finance_check_box].present?
				params[:finance_check_box].each do |p|
					@writeoff_deposit = WriteoffDeposit.new
					# @writeoff_deposit.bank_deposit_finance_transaction_id = BankDepositFinanceTransaction.find(p.to_i).bank_deposit_finance_transaction_number.to_i
					@writeoff_deposit.writeoff_details_id =@write_off_details.id
					@writeoff_deposit.student_id = p.to_i
					params[:students].each do |k|
						if p.to_i == k[0].to_i
							@writeoff_deposit.amount = k[1].to_d
						end
					end	
					@writeoff_deposit.save!
					@writeoff_deposit.update_attribute(:writeoff_deposit_number,bdft_number)
					flash[:notice] = "#{t('write_off_created_successfully.')}"
				end
			end
    end
		redirect_to :controller => 'bank_deposits' ,:action=>"write_off"
	end

	def deposits
		# if current_user.admin?
			@writeoff_details = WriteoffDetail.all.reverse
			if params[:page].present?
		        page_deposit = params[:page]
		    else
		      page_deposit = 1
		    end
		    if params[:per_page].present?
		      per_page_deposit = params[:per_page]
		    else
		      per_page_deposit = 10
		    end
		    # b=BankDepositFinanceTransaction.all.map {|p| p.bank_deposit_finance_transaction_number}.uniq
		    # a = b.reverse
		    # @bdf=[]
		    # unless a.empty?
		    #  a.map{|p| @bdf<<BankDepositFinanceTransaction.find_by_bank_deposit_finance_transaction_number(p)}
		      @wod =  WillPaginate::Collection.create( page_deposit, per_page_deposit, @writeoff_details.length) do |pager|
		        pager.replace @writeoff_details[pager.offset, pager.per_page]
		      end    
		    # end
		# else
  #     flash[:notice] = "#{t('flash_msg4')}"
  #     redirect_to :controller => "user",:action => "dashboard"
  #   end
	end

	def writeoff_reports_details
		@verticals = YuvaVertical.all
	end

	def show_writeoff_deposit_details
		@writeoff_details = WriteoffDetail.find(params[:id])
		@writeoff_deposit = WriteoffDeposit.find_all_by_writeoff_details_id(@writeoff_details.id)	
	end

	def edit_writeoff_deposit_details
		@writeoff_details = WriteoffDetail.find(params[:id])
		@writeoff_deposit = WriteoffDeposit.find_all_by_writeoff_details_id(@writeoff_details.id)
		if request.post?
			@writeoff_details.update_attribute(:reason_for_writeoff,params[:reason_for_write_off])
			if params[:document_upload].present?
				puts"ggdfgdfgfg"
				@writeoff_details.update_attribute(:document_upload,params[:document_upload])
			end
			if params[:writeoff_date].present?
				puts"fyrtyru"
				@writeoff_details.update_attribute(:writeoff_date,params[:writeoff_date])
			end
			flash[:notice] = "#{t('write_off_updated_successfully')}"
			redirect_to :controller => 'writeoff_deposits' ,:action=>"deposits"
		end	
	end

	def writeoff_report_details_yuva
		@writeoff_deposit = WriteoffDetail.find_all_by_vertical_id(params[:vertical_id])
		render :update do |page|
      page.replace_html 'report_table', :partial=>'writeoff_deposits/writeoff_report_details_yuva'
    end
	end

	def approve_writeoff_deposit_details
		@writeoff_details = WriteoffDetail.find(params[:id])
		@writeoff_deposit = WriteoffDeposit.find_all_by_writeoff_details_id(@writeoff_details.id)
		@writeoff_deposit.each do |e|
		  e.update_attribute(:approve,true)
		end
		 redirect_to :controller => "writeoff_deposits",:action => "deposits"
	end

	def reject_writeoff_deposit_details
		@writeoff_details = WriteoffDetail.find(params[:id])
		@writeoff_deposit = WriteoffDeposit.find_all_by_writeoff_details_id(@writeoff_details.id)
		@writeoff_deposit.each do |s|
        s.update_attribute(:reject,true)
    end
     redirect_to :controller => "writeoff_deposits",:action => "deposits"
  end

  def download_uploded_document
  	begin
      @doc = WriteoffDetail.find params[:id]
      unless @doc.nil?
        send_file  @doc.document_upload.path , :type=>@doc.document_upload.content_type
      end
    rescue Exception => e
      Rails.logger.info "Exception from rank_yuvaparivarthan_support pluggin in writeoff_deposit_detail_controller, download_uploded_document action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong while downloading a file. Please inform administration"
      redirect_to :controller => "writeoff_deposit_details", :action => :show_writeoff_deposit_details ,:id => @doc.id
    end
  end

  def change_document_upload
  	render :update do |page|
      page.replace_html 'document_upload', :partial => 'writeoff_deposits/change_document_upload'
      page.hide "document_hide"
    end
  end

end
